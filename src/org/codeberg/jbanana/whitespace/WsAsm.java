package org.codeberg.jbanana.whitespace;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.function.Supplier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;
import java.util.Set;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

/**
 * Turns Whitespace assembler into executable Whitespace.
 * <h2>Usage:</h2>
 * <pre>java WsAsm [options] myfile.wsa [more files...]</pre>
 * where options are
 * <dl>
 *	<dt>-s #!/path/program</dt><dd>Include the given shebang in the output.</dd>
 *	<dt>-x</dt><dd>Enable extensions.</dd>
 * </dl>
 * <p>See documentation for assembler reference and examples.</p>
 */
public class WsAsm {
	private static final Pattern STRING_PATTERN = Pattern.compile ( "\"(.*)\"" );
	private static final Pattern INSTRUCTION_PATTERN = Pattern.compile (
		"""
		(?x)              # comments and whitespace
		\\s*              # leading white space
		(
		  [^\\ \\#]+      # instruction (group 1)
		)?                # instruction is optional
		(?:
		  \\ +            # separator
		  (
		    [^\\#]        # one non-# char
		    |             # or
		    [^\\#]+[^\\s] # one non-# char and some non-sp chars
		  )               # parameter   (group 2)
		)??               # parameter is optional
		\\s*              # white space
		(?:
		  \\#.*           # comment
		)?                # comment is optional
		"""
	);

	/**
	 * Entry point.
	 *
	 * @param args
	 * <dl>
	 *	<dt>-s #!/path/program</dt><dd>Optional - includes the given shebang in the output.</dd>
	 *	<dt>-x</dt><dd>Optional - enables extensions.</dd>
	 *	<dt>&lt;somefile.wsa&gt;</dt><dd>Required - a file to assemble.</dd>
	 *	<dt>[more files]</dt><dd>Optional- you can assemble as many files as you like.</dd>
	 * </dl>
	 */
	public static void main ( final String... args ) {
		/*v*/ var failed     = false;
		/*v*/ var shebang    = ( String ) null;
		final var fileNames  = new ArrayList < String > ();
		/*v*/ var compatible = true;

		for ( var index = 0; index < args.length; index++ ) {
			final var arg = args [ index ];

			if ( fileNames.isEmpty () )
				switch ( arg ) {
					case "-s":
						shebang = args [ ++index ];
						break;
					case "-x":
						compatible = false;
						break;
					default:
						fileNames.add ( arg );
				}
			else
				fileNames.add ( arg );
		}

		for ( final var fileName : fileNames ) {
			try {
				new WsAsm (
					new File ( fileName ),
					shebang,
					compatible
				). assemble ();
			} catch ( final IOException x ) {
				failed = true;
				System.err.println ( "Assembly failed: " + x );
				x.printStackTrace ();
			}
		}

		if ( failed )
			System.exit ( 2 );
	}

	/**
	 * A LinkedHashSet that does not throw ConcurrentModificationException if
	 * more elements are added, but will carry on iterating. Iterating is safe
	 * when elememts are added, but if elements are removed then the operation
	 * of this class is not guaranteed.
	 */
	/*package*/ static class ExtendableSet < E >
			extends LinkedHashSet < E > {
		public Iterator < E > iterator () {
			return new Iterator < E > () {
				private /*v*/ int index;

				public boolean hasNext () {
					return index < size ();
				}

				public E next () {
					if ( index >= size () )
						throw new NoSuchElementException ();

					final var iter = getIter ();

					// This would be slow for very large sets, but we don't
					// expect that here.
					for ( int i = 0; i < index; i++ )
						iter.next ();
					index++;
					return iter.next ();
				}
			};
		}

		private Iterator < E > getIter () {
			return super.iterator ();
		}
	}

	private static class Barf
			extends RuntimeException {
		private Barf ( final String msg ) {
			super ( msg );
		}
	}

	private final File                   sourceFile;
	private final Set < String >         macros     = new ExtendableSet <> ();
	private final String                 shebang;
	private final boolean                compatible;
	private final File                   sourceDir;
	private /*v*/ File                   macroFile;
	private final Set < String >         userMacros = new ExtendableSet <> ();
	private /*v*/ int                    counter;
	private final Map < String, String > symbols    = new HashMap <> ();

	/*package*/ WsAsm ( final File sourceFile, final String shebang, final boolean compatible ) {
		this.sourceFile = sourceFile;
		this.shebang    = shebang;
		this.compatible = compatible;
		this.sourceDir  = sourceFile.getParentFile ();
	}

	/*package*/ Map < Integer, Integer > assemble ()
			throws IOException {
		final var lineMap    = new HashMap < Integer, Integer > ();
		final var result     = new StringWriter ();
		final var targetFile = getWsFile ( sourceFile );
		final var lines      = Files.lines ( sourceFile.toPath (), UTF_8 )
			.collect ( toList () );

		for ( int i = 0, c = lines.size (), j = 0; i < c; i++ ) {
			final String ws = lineToWhitespace ( lines.get ( i ), i );

			if ( ! ws.isEmpty () )
				lineMap.put ( counter - 1, i + 1 );

			result.write ( ws );
		}

		result.write ( getWhiteSpace ( "end", compatible ? null : "0" ) );
		for ( final String userMacro : userMacros )
			result.write ( makeMacroRoutine ( userMacro ) );
		for ( final String macro : macros ) {
			switch ( macro ) {
				case "$print"        : result.write ( getPrintRoutine        () ); break;
				case "$readctostack" : result.write ( getReadCToStackRoutine () ); break;
				case "$readntostack" : result.write ( getReadNToStackRoutine () ); break;
				case "$incr"         : result.write ( getIncrRoutine         () ); break;
				case "$incrn"        : result.write ( getIncrNRoutine        () ); break;
				case "$decr"         : result.write ( getDecrRoutine         () ); break;
				case "$decrn"        : result.write ( getDecrNRoutine        () ); break;
				case "$mult10n"      : result.write ( getMult10NRoutine      () ); break;
				case "$abs"          : result.write ( getAbsRoutine          () ); break;
				case "$truncdiv"     : result.write ( getTruncDivRoutine     () ); break;
				case "$truncmod"     : result.write ( getTruncModRoutine     () ); break;
				case "$mag"          : result.write ( getMagRoutine          () ); break;
				case "$32bcrop"      : result.write ( get32bCropRoutine      () ); break;
				case "$malloc"       : result.write ( getMallocRoutine       () ); break;
				case "$blocksz"      : result.write ( getBlockSzRoutine      () ); break;
				case "$free"         : result.write ( getFreeRoutine         () ); break;
				case "$freearr"      : result.write ( getFreeArrRoutine      () ); break;
				case "$lstnew"       : result.write ( getLstNewRoutine       () ); break;
				case "$lstsz"        : result.write ( getLstSzRoutine        () ); break;
				case "$lstadd"       : result.write ( getLstAddRoutine       () ); break;
				case "$lstget"       : result.write ( getLstGetRoutine       () ); break;
				case "$arr2lst"      : result.write ( getArr2LstRoutine      () ); break;
				case "$lst2arr"      : result.write ( getLst2ArrRoutine      () ); break;
				case "$lstdup"       : result.write ( getLstDupRoutine       () ); break;
				case "$lstrev"       : result.write ( getLstRevRoutine       () ); break;
				case "$freelst"      : result.write ( getFreeLstRoutine      () ); break;
				case "$freelstlite"  : result.write ( getFreeLstLiteRoutine  () ); break;
				case "$mapnew"       : result.write ( getMapNewRoutine       () ); break;
				case "$mapsz"        : result.write ( getMapSzRoutine        () ); break;
				case "$$hash2bucket" : result.write ( getHash2BucketRoutine  () ); break;
				case "$$mapbfind"    : result.write ( getMapBFindRoutine     () ); break;
				case "$mapput"       : result.write ( getMapPutRoutine       () ); break;
				case "$mapget"       : result.write ( getMapGetRoutine       () ); break;
				case "$readstr"      : result.write ( getReadStrRoutine      () ); break;
				case "$printstr"     : result.write ( getPrintStrRoutine     () ); break;
				// $printc routine not needed - done inline
				// $printn routine not needed - done inline
				case "$str2int"      : result.write ( getStr2IntRoutine      () ); break;
				case "$int2str"      : result.write ( getInt2StrRoutine      () ); break;
				case "$strlen"       : result.write ( getStrLenRoutine       () ); break;
				case "$strlens"      : result.write ( getStrLenSRoutine      () ); break;
				case "$storestr"     : result.write ( getStoreStrRoutine     () ); break;
				case "$strcmp"       : result.write ( getStrCmpRoutine       () ); break;
				case "$strcpy"       : result.write ( getStrCpyRoutine       () ); break;
				case "$strncpy"      : result.write ( getStrNCpyRoutine      () ); break;
				case "$strcat"       : result.write ( getStrCatRoutine       () ); break;
				case "$strtok"       : result.write ( getStrTokRoutine       () ); break;
				case "$startswith"   : result.write ( getStartsWithRoutine   () ); break;
				case "$strhash"      : result.write ( getStrHash             () ); break;
				case "$str2fl"       : result.write ( getStr2FlRoutine       () ); break;
				case "$fl2str"       : result.write ( getFl2StrRoutine       () ); break;
				case "$readfl"       : result.write ( getReadFlRoutine       () ); break;
				case "$storefl"      : result.write ( getStoreFlRoutine      () ); break;
				case "$addfl"        : result.write ( getAddFlRoutine        () ); break;
				case "$subfl"        : result.write ( getSubFlRoutine        () ); break;
				case "$multfl"       : result.write ( getMultFlRoutine       () ); break;
				case "$divfl"        : result.write ( getDivFlRoutine        () ); break;
				case "$modfl"        : result.write ( getModFlRoutine        () ); break;
				case "$truncmodfl"   : result.write ( getTruncModFlRoutine   () ); break;
				case "$floorfl"      : result.write ( getFloorFlRoutine      () ); break;
				case "$truncfl"      : result.write ( getTruncFlRoutine      () ); break;
				case "$sqrtfl"       : result.write ( getSqrtFlRoutine       () ); break;
				case "$expfl"        : result.write ( getExpFlRoutine        () ); break;
				case "$printfl"      : result.write ( getPrintFlRoutine      () ); break;
				case "$int2fl"       : result.write ( getInt2FlRoutine       () ); break;
				case "$fl2int"       : result.write ( getFl2IntRoutine       () ); break;

				// These require extenstions
				case "$x-args"       : maybeWrite ( result, this::getXArgsRoutine, macro ); break;

				default:
					throw new IllegalStateException ( "Unexpected macro name: " + macro );
			}
		}

		System.out.println ( "Writing to " + targetFile.getAbsolutePath () );
		try ( final var output = new PrintWriter ( targetFile, UTF_8 ) ) {
			if ( null != shebang )
				output.print ( shebang );
			if ( macros.contains ( "$malloc" ) ) output.print ( mallocSetup1 () );
			output.print ( result.toString () );
			if ( macros.contains ( "$malloc" ) ) output.print ( mallocSetup2 () );
		}

		return lineMap
			.entrySet ()
			.stream ()
			.map (
				e -> macros.contains ( "$malloc" )
					? Map.entry ( e.getKey () + 1, e.getValue () )
					: e
			)
			.collect ( toMap ( Map.Entry::getKey, Map.Entry::getValue ) );
	}

	private interface WsMaker {
		String make () throws IOException;
	}

	private void maybeWrite ( final StringWriter result, final WsMaker source, final String macroName )
			throws IOException {
		if ( compatible ) throw new Barf ( "Macro " + macroName + " requires extensions." );
		result.write ( source.make () );
	}

	private String lineToWhitespace ( final String line, final int index )
			throws IOException {
		final Matcher matcher = INSTRUCTION_PATTERN.matcher ( line );

		if ( ! matcher.matches () )
			throw new IllegalArgumentException (
				"Badly formatted instruction in "
					+ ( null == macroFile ? sourceFile : macroFile )
					+ " at line "
					+ ( index + 1 )
					+ ": "
					+ line
			);

		final var rawParam  = matcher.group ( 2 );
		final var parameter = null != rawParam && rawParam.startsWith ( "@" )
			? getSymbol ( rawParam )
			: rawParam;

		return getWhiteSpace (
			matcher.group ( 1 ),
			parameter,
			index
		);
	}

	private String getSymbol ( final String symbolName ) {
		if ( ! symbols.containsKey ( symbolName ) )
			throw new IllegalArgumentException ( "Undefined symbol name " + symbolName );

		return symbols.get ( symbolName );
	}

	/*package*/ static File getWsFile ( final File sourceFile ) {
		final var name = sourceFile.getName ();
		final var dir  = sourceFile.getParentFile ();

		return new File (
				dir, (
					name.toLowerCase ().endsWith ( ".wsa" )
						? name.substring ( 0, name.length () - 4 )
						: name
				) + ".ws"
			);
	}

	private String getWhiteSpace ( final String instructions )
			throws IOException {
		final var result = new StringBuilder ();

		for ( final var s = new Scanner ( instructions ); s.hasNextLine ();  )
			result.append ( lineToWhitespace ( s.nextLine (), -1 ) );

		return result.toString ();
	}

	private String getWhiteSpace ( final String instruction, final String param )
			throws IOException {
		return getWhiteSpace ( instruction, param, -1 );
	}

	private String getWhiteSpace ( final String instruction, final String rawParam, final int lineNo )
			throws IOException {
		//System.err.println ( lineNo + ": inst: " + instruction + " - rawParam: " + rawParam );

		if ( null == instruction ) return "";

		counter++;

		final var param = null != rawParam && rawParam.startsWith ( "@" )
			? symbols.get ( rawParam )
			: rawParam;

		try {
			switch ( instruction ) {
				// Stack manipulation
				case "push":  return "  "    + makeNum ( param );
				case "dup":   return " \n ";
				case "copyn": return " \t "  + makeNum ( param );
				case "swap":  return " \n\t";
				case "drop":  return " \n\n";
				case "slide": return " \t\n" + makeNum ( param );
				// unused: " \t\t"

				// Arithmetic
				case "add":   return "\t   ";
				case "sub":   return "\t  \t";
				case "mult":  return "\t  \n";
				case "div":   return "\t \t ";
				case "mod":   return "\t \t\t";
				// unused: "\t \t\n"
				// unused: "\t \n "
				// unused: "\t \n\t"
				// unused: "\t \n\n"

				// Heap access
				case "store":    return "\t\t ";
				case "retrieve": return "\t\t\t";
				case "x-dump":   if ( compatible ) throw new Barf ( "Invalid instruction x-dump - extensions not enabled." ); else return "\t\t\n";

				// Flow
				case "call":   return "\n \t"  + makeLabel ( param );
				case "jump":   return "\n \n"  + makeLabel ( param );
				case "jumpz":  return "\n\t "  + makeLabel ( param );
				case "jumpn":  return "\n\t\t" + makeLabel ( param );
				case "return": return "\n\t\n";
				case "end":    return "\n\n\n" + ( compatible ? "" : makeNum ( param ) );
				// unused: "\n  "
				// unused: "\n\n "
				// unused: "\n\n\t"

				// I/O
				case "printc":      return "\t\n  ";
				case "printn":      return "\t\n \t";
				case "readc":       return "\t\n\t ";
				case "readn":       return "\t\n\t\t";
				case "x-args":      if ( compatible ) throw new Barf ( "Invalid instruction x-args - extensions not enabled."      ); else return "\t\n \n";
				case "x-readfile":  if ( compatible ) throw new Barf ( "Invalid instruction x-readfile - extensions not enabled."  ); else return "\t\n\t\n ";
				case "x-writefile": if ( compatible ) throw new Barf ( "Invalid instruction x-writefile - extensions not enabled." ); else return "\t\n\t\n\n";
				case "x-closefile": if ( compatible ) throw new Barf ( "Invalid instruction x-closefile - extensions not enabled." ); else return "\t\n\t\n\t";
				// unused: "\t\n\n ";
				// unused: "\t\n\n\t";
				// unused: "\t\n\n\n"

				// built-in macros
				case "$print"       : counter--; return makePrint        ( param, lineNo );
				case "$readctostack": counter--; return makeReadCToStack ( param, lineNo );
				case "$readntostack": counter--; return makeReadNToStack ( param, lineNo );
				case "$incr"        : counter--; return makeIncr         ( param, lineNo );
				case "$incrn"       : counter--; return makeIncrN        ( param, lineNo );
				case "$decr"        : counter--; return makeDecr         ( param, lineNo );
				case "$decrn"       : counter--; return makeDecrN        ( param, lineNo );
				case "$mult10^n"    : counter--; return makeMult10N      ( param, lineNo );
				case "$abs"         : counter--; return makeAbs          ( param, lineNo );
				case "$truncdiv"    : counter--; return makeTruncDiv     ( param, lineNo );
				case "$truncmod"    : counter--; return makeTruncMod     ( param, lineNo );
				case "$mag"         : counter--; return makeMag          ( param, lineNo );
				case "$32bcrop"     : counter--; return make32bCrop      ( param, lineNo );
				case "$malloc"      : counter--; return makeMalloc       ( param, lineNo );
				case "$blocksz"     : counter--; return makeBlockSz      ( param, lineNo );
				case "$free"        : counter--; return makeFree         ( param, lineNo );
				case "$freearr"     : counter--; return makeFreeArr      ( param, lineNo );
				case "$lstnew"      : counter--; return makeLstNew       ( param, lineNo );
				case "$lstsz"       : counter--; return makeLstSz        ( param, lineNo );
				case "$lstadd"      : counter--; return makeLstAdd       ( param, lineNo );
				case "$lstget"      : counter--; return makeLstGet       ( param, lineNo );
				case "$arr2lst"     : counter--; return makeArr2Lst      ( param, lineNo );
				case "$lst2arr"     : counter--; return makeLst2Arr      ( param, lineNo );
				case "$lstdup"      : counter--; return makeLstDup       ( param, lineNo );
				case "$lstrev"      : counter--; return makeLstRev       ( param, lineNo );
				case "$freelst"     : counter--; return makeFreeLst      ( param, lineNo );
				case "$freelstlite" : counter--; return makeFreeLstLite  ( param, lineNo );
				case "$mapnew"      : counter--; return makeMapNew       ( param, lineNo );
				case "$mapsz"       : counter--; return makeMapSz        ( param, lineNo );
				case "$$hash2bucket": counter--; return makeHash2Bucket  ( param, lineNo );
				case "$$mapbfind"   : counter--; return makeMapBFind     ( param, lineNo );
				case "$mapput"      : counter--; return makeMapPut       ( param, lineNo );
				case "$mapget"      : counter--; return makeMapGet       ( param, lineNo );
				case "$readstr"     : counter--; return makeReadStr      ( param, lineNo );
				case "$printstr"    : counter--; return makePrintStr     ( param, lineNo );
				case "$printc"      : counter--; return makePrintC       ( param, lineNo );
				case "$printn"      : counter--; return makePrintN       ( param, lineNo );
				case "$str2int"     : counter--; return makeStr2Int      ( param, lineNo );
				case "$int2str"     : counter--; return makeInt2Str      ( param, lineNo );
				case "$strlen"      : counter--; return makeStrLen       ( param, lineNo );
				case "$strlens"     : counter--; return makeStrLenS      ( param, lineNo );
				case "$storestr"    : counter--; return makeStoreStr     ( param, lineNo );
				case "$strcmp"      : counter--; return makeStrCmp       ( param, lineNo );
				case "$strcpy"      : counter--; return makeStrCpy       ( param, lineNo );
				case "$strncpy"     : counter--; return makeStrNCpy      ( param, lineNo );
				case "$strcat"      : counter--; return makeStrCat       ( param, lineNo );
				case "$strtok"      : counter--; return makeStrTok       ( param, lineNo );
				case "$startswith"  : counter--; return makeStartsWith   ( param, lineNo );
				case "$strhash"     : counter--; return makeStrHash      ( param, lineNo );
				case "$str2fl"      : counter--; return makeStr2Fl       ( param, lineNo );
				case "$fl2str"      : counter--; return makeFl2Str       ( param, lineNo );
				case "$readfl"      : counter--; return makeReadFl       ( param, lineNo );
				case "$storefl"     : counter--; return makeStoreFl      ( param, lineNo );
				case "$addfl"       : counter--; return makeAddFl        ( param, lineNo );
				case "$subfl"       : counter--; return makeSubFl        ( param, lineNo );
				case "$multfl"      : counter--; return makeMultFl       ( param, lineNo );
				case "$divfl"       : counter--; return makeDivFl        ( param, lineNo );
				case "$modfl"       : counter--; return makeModFl        ( param, lineNo );
				case "$truncmodfl"  : counter--; return makeTruncModFl   ( param, lineNo );
				case "$floorfl"     : counter--; return makeFloorFl      ( param, lineNo );
				case "$truncfl"     : counter--; return makeTruncFl      ( param, lineNo );
				case "$sqrtfl"      : counter--; return makeSqrtFl       ( param, lineNo );
				case "$expfl"       : counter--; return makeExpFl        ( param, lineNo );
				case "$printfl"     : counter--; return makePrintFl      ( param, lineNo );
				case "$int2fl"      : counter--; return makeInt2Fl       ( param, lineNo );
				case "$fl2int"      : counter--; return makeFl2Int       ( param, lineNo );

				case "$x-args"      : counter--; return makeXArgs        ( param, lineNo );

				default:
					if ( instruction.isEmpty () ) { // It's a comment
						counter--;
						return "";
					}
					if ( instruction.startsWith ( ":" ) ) // It's a label
						return "\n  " + makeLabel ( instruction );
					if ( instruction.startsWith ( "%" ) ) { // It's a user-defined macro
						counter--;
						userMacros.add ( instruction );
						return makeMacroCall ( instruction, lineNo );
					}
					if ( instruction.startsWith ( "@" ) ) { // It's a symbol definition
						counter--;
						symbols.put ( instruction, param );
						return "";
					}
					throw new IllegalArgumentException (
						"Unrecognised instruction in "
							+ ( null == macroFile ? sourceFile : macroFile )
							+ " at line "
							+ ( lineNo + 1 )
							+ ": "
							+ instruction
					);
			}
		} catch ( final IllegalArgumentException x ) {
			throw new IllegalArgumentException ( "Could not get white space for instruction " + instruction + " at " + ( lineNo + 1 ), x );
		}
	}

	private String makeMacroCall ( final String instruction, final int lineNo )
			throws IOException {
		return lineToWhitespace ( "call :" + instruction, lineNo );
	}

	private String makeMacroRoutine ( final String instruction )
			throws IOException {
		final var result = new StringBuilder ();

		macroFile = new File ( sourceDir, instruction + ".wsam" );

		final var lines = Files.lines ( macroFile.toPath () ).collect ( toList () );

		result.append ( lineToWhitespace ( ':' + instruction, 0 ) );
		for ( int i = 0, c = lines.size (); i < c; i++ )
			result.append ( lineToWhitespace ( lines.get ( i ), i ) );
		macroFile = null;

		return result.toString ();
	}

	/*package*/ static String makeNum ( final String token ) {
		if ( null == token ) throw new IllegalArgumentException ( "Cannot make number from null token." );

		final String     cleanT = token.trim ();
		final BigInteger value;

		if ( cleanT.startsWith ( "'" ) && cleanT.endsWith ( "'" ) )
			switch ( cleanT ) {
				case "'\\n'": value = BigInteger.valueOf ( '\n' ); break;
				case "'\\r'": value = BigInteger.valueOf ( '\r' ); break;
				case "'\\t'": value = BigInteger.valueOf ( '\t' ); break;
				default:      value = BigInteger.valueOf ( cleanT.codePointAt ( 1 ) );
			}
		else
			value = new BigInteger ( cleanT );

		return ( value.signum () < 0 ? '\t' : ' ' ) + value.toString ( 2 ).replaceAll ( "-", "" ).replaceAll ( "0", " " ).replaceAll ( "1", "\t" ) + "\n";
	}

	/*package*/ static String makeLabel ( final String token ) {
		if ( null == token )
			throw new IllegalArgumentException ( "Null label token" );

		if ( token.isEmpty () )
			throw new IllegalArgumentException ( "Empty label token" );

		if ( ! token.startsWith ( ":" ) )
			throw new IllegalArgumentException ( "Empty label token" );

		if ( 1 == token.length () )
			throw new IllegalArgumentException ( "Label token contains : and nothing else" );

		final StringBuilder result = new StringBuilder ();

		for ( int i = 1, count = token.length (); i < count; i++ ) {
			final char ch = token.charAt ( i );

			if ( ch > 127 )
				throw new IllegalArgumentException ( "Non-ASCII character '" + ch + "' in label token" );

			if ( ch == ' ' )
				throw new IllegalArgumentException ( "Space in label token" );

			final String tmp = "0000000" + Integer.toBinaryString ( ch );

			result.append ( tmp.substring ( tmp.length () - 8 ) );
		}

		return result.toString ().replaceAll ( "0", " " ).replaceAll ( "1", "\t" ) + "\n";
	}

	private void need ( final String macroName ) {
		macros.add ( macroName );
	}

	private String makePrint ( final String param, final int line )
			throws IOException {
		final var result = new StringBuilder ();

		pushStr ( stringParam ( param, line ), line, result );
		result.append ( getWhiteSpace ( "drop", null,      line ) );
		result.append ( getWhiteSpace ( "call", ":$Print", line ) );

		need ( "$print" );

		return result.toString ();
	}

	private String stringParam ( final String param, final int line ) {
		if ( null == param )
			throw new IllegalArgumentException ( "Missing string parameter at line " + ( line + 1 ));

		final Matcher matcher = STRING_PATTERN.matcher ( param );

		if ( ! matcher.matches () )
			throw new IllegalArgumentException ( "Badly formatted string at line " + ( line + 1 ) + ": " + param );

		return matcher.group ( 1 )
			.replaceAll ( "\\\\\"", "\"" )
			.replaceAll ( "\\\\n",  "\n" )
			.replaceAll ( "\\\\r",  "\r" )
			.replaceAll ( "\\\\t",  "\t" )
		;
	}

	private void pushStr ( final String content, final int line, final StringBuilder result )
			throws IOException {
		result.append ( getWhiteSpace ( "push", "0", line ) );

		final var chars = content.codePoints ().toArray ();

		for ( int i = chars.length; i --> 0;  )
			result.append ( getWhiteSpace ( "push", Integer.toString ( chars [ i ] ), line ) );
		result.append ( getWhiteSpace ( "push", Integer.toString ( chars.length + 1 ) ) );
	}

	private String getPrintRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Print
			dup
			jumpz  :$PrintDone
			printc
			jump   :$Print
			:$PrintDone
			drop
			return
			"""
		);
	}

	private String makeReadCToStack ( final String param, final int line )
			throws IOException {
		need ( "$readctostack" );

		return getWhiteSpace ( "call", ":$ReadCToStack", line );
	}

	private String getReadCToStackRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$ReadCToStack
			push 1   # [1]
			$malloc  # [a]
			dup      # [a,a]
			dup      # [a,a,a]
			readc    # [a,a]
			retrieve # ['g',a]
			swap     # [a,'g']
			$free    # ['g']
			return
			"""
		);
	}

	private String makeReadNToStack ( final String param, final int line )
			throws IOException {
		need ( "$readntostack" );

		return getWhiteSpace ( "call", ":$ReadNToStack", line );
	}

	private String getReadNToStackRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$ReadNToStack
			push 1     # [1]
			$malloc    # [a]
			dup        # [a,a]
			dup        # [a,a,a]
			readn      # [a,a]
			retrieve   # [6,a]
			swap       # [a,6]
			$free      # [6]
			return
			"""
		);
	}

	private String makeIncr ( final String param, final int line )
			throws IOException {
		need ( "$incr" );

		return getWhiteSpace ( "call", ":$Incr", line );
	}

	private String getIncrRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Incr     # [p]
			dup        # [p,p]
			retrieve   # [n,p]
			push 1     # [1,n,p]
			add        # [n+1,p]
			store      # []
			return
			"""
		);
	}

	private String makeIncrN ( final String param, final int line )
			throws IOException {
		need ( "$incrn" );

		return getWhiteSpace ( "call", ":$IncrN", line );
	}

	private String getIncrNRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$IncrN   # [4,p]
			swap      # [p,4]
			dup       # [p,p,4]
			retrieve  # [n,p,4]
			copyn 2   # [4,n,p,4]
			add       # [n+4,p,4]
			store     # [4]
			drop      # []
			return
			"""
		);
	}

	private String makeDecr ( final String param, final int line )
			throws IOException {
		need ( "$decr" );

		return getWhiteSpace ( "call", ":$Decr", line );
	}

	private String getDecrRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Decr     # [p]
			dup        # [p,p]
			retrieve   # [n,p]
			push 1     # [1,n,p]
			sub        # [n-1,p]
			store      # []
			return
			"""
		);
	}

	private String makeDecrN ( final String param, final int line )
			throws IOException {
		need ( "$decrn" );

		return getWhiteSpace ( "call", ":$DecrN", line );
	}

	private String getDecrNRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$DecrN    # [4,p]
			swap       # [p,4]
			dup        # [p,p,4]
			retrieve   # [n,p,4]
			copyn 2    # [4,n,p,4]
			sub        # [n-4,p,4]
			store      # [4]
			drop       # []
			return
			"""
		);
	}

	private String makeMult10N ( final String param, final int line )
			throws IOException {
		need ( "$mult10n" );

		return getWhiteSpace ( "call", ":$Mult10^N", line );
	}

	private String getMult10NRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Mult10^N            # [n,a]
			dup                   # [n,n,a]
			jumpn :$Mult10^NDown  # [n,a]
			:$Mult10^NUp          # [n,a]
			dup                   # [n,n,a]
			jumpz :$Mult10^NDone  # [n,a]
			copyn 1               # [a,n,a]
			retrieve              # [v,n,a]
			push 10               # [10,v,n,a]
			mult                  # [10v,n,a]
			copyn 2               # [a,10v,n,a]
			swap                  # [10v,a,n,a]
			store                 # [n,a]
			push 1                # [1,n,a]
			sub                   # [n-1,a]
			jump :$Mult10^NUp     # [n-1,a]

			:$Mult10^NDown        # [n,a]
			dup                   # [n,n,a]
			jumpz :$Mult10^NDone  # [n,a]
			copyn 1               # [a,n,a]
			retrieve              # [v,n,a]
			push 10               # [10,v,n,a]
			$truncdiv             # [v/10,n,a]
			copyn 2               # [a,v/10,n,a]
			swap                  # [v/10,a,n,a]
			store                 # [n,a]
			push 1                # [1,n,a]
			add                   # [n-1,a]
			jump :$Mult10^NDown   # [n-1,a]

			:$Mult10^NDone        # [n,a]
			drop                  # [a]
			drop                  # []
			return                # []
			"""
		);
	}

	private String makeAbs ( final String param, final int line )
			throws IOException {
		need ( "$abs" );

		return getWhiteSpace ( "call", ":$Abs", line );
	}

	private String getAbsRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Abs
			dup
			jumpn :$AbsNeg
			return
			:$AbsNeg
			push -1
			mult
			return
			"""
		);
	}

	private String makeTruncDiv ( final String param, final int line )
			throws IOException {
		need ( "$truncdiv" );

		return getWhiteSpace ( "call", ":$TruncDiv", line );
	}

	private String getTruncDivRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$TruncDiv            # [numerator,denominator]
			dup                   # [n,n,d]
			jumpn :$TruncDivNNeg  # [n,d]
			copyn 1               # [d,n,d]
			jumpn :$TruncDivMix   # [n,d]
			:$TruncDivMatch       # [n,d]
			div                   # [r]
			return                # [r]

			:$TruncDivMix         # [n,d]
			push -1               # [-1,n,d]
			mult                  # [-n,d]
			div                   # [-r]
			push -1               # [-1,-r]
			mult                  # [r]
			return

			:$TruncDivNNeg        # [n,d]
			copyn 1               # [d,n,d]
			jumpn :$TruncDivMatch # [n,d]
			jump :$TruncDivMix    # [n,d]
			"""
		);
	}

	private String makeTruncMod ( final String param, final int line )
			throws IOException {
		need ( "$truncmod" );

		return getWhiteSpace ( "call", ":$TruncMod", line );
	}

	private String getTruncModRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$TruncMod            # [denominator,numerator]

			# remainder = n - ( d * truncDiv( n, d ) )
			# rpn:        n d n d truncDiv * -
			# Oops, I've been writing stacks the other way round 8~)

			copyn 1               # [n,d,n]
			copyn 1               # [d,n,d,n]
			$truncdiv             # [q,d,n]
			mult                  # [d*q,n]
			sub                   # [n-d*q]
			return                # [n-d*q]
			"""
		);
	}

	private String makeMag ( final String param, final int line )
			throws IOException {
		need ( "$mag" );

		return getWhiteSpace ( "call", ":$Mag", line );
	}

	private String getMagRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Mag            # [a]
			push 0           # [m(starts at 0),a]
			swap             # [a,m]
			retrieve         # [n,m]
			dup              # [n,n,m]
			jumpn :$MagFlip  # [n,m]
			jump :$MagLoop   # [n,m]
			:$MagFlip        # [-n,m]
			push -1          # [-1,-n,m]
			mult             # [n,m]
			:$MagLoop
			dup              # [n,n,m]
			jumpz :$MagDone  # [n,m]
			swap             # [m,n]
			push 1           # [1,m,n]
			add              # [m+1,n]
			swap             # [n,m+1]
			push 10          # [10,n,m+1]
			div              # [n/10,m+1]
			jump :$MagLoop

			:$MagDone        # [0,m]
			drop             # [m]
			return           # [m]
			"""
		);
	}

	private String make32bCrop ( final String param, final int line )
			throws IOException {
		need ( "$32bcrop" );

		return getWhiteSpace ( "call", ":$32bCrop", line );
	}

	private String get32bCropRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$32bCrop           # [n]
			# some helpful constants
			@$Max32bInt    2147483647   # 2^31-1
			@$Max32bInt-1  2147483646   # 2^31-2
			@$Min32bInt    -2147483648  # -(2^31)
			@$32bIntRange  4294967296   # 2^32

			dup                 # [n,n]
			dup                 # [n,n,n]
			jumpn :$32bCrop:n   # [n,n]
			push @$Max32bInt-1  # [@$Max32bInt-1,n,n]
			add                 # [startPoint,n]
			jump :$32bCrop:cont # [startPoint,n]

			:$32bCrop:n         # [n,n]
			push @$Max32bInt    # [@$Max32bInt,n,n]
			sub                 # [startPoint,n]

			:$32bCrop:cont      # [startPoint,n]
			push @$32bIntRange  # [@$32bIntRange,startPoint,n]
			$truncdiv           # [choppedStartPoint,n]
			push @$32bIntRange  # [@$32bIntRange,choppedStartPoint,n]
			mult                # [adjustment,n]
			sub                 # [result]
			return              # [result]
			"""
		);
	}

	private String makeMalloc ( final String param, final int line )
			throws IOException {
		need ( "$malloc" );

		return getWhiteSpace ( "call", ":$Malloc", line );
	}

	private String getMallocRoutine ()
			throws IOException {
		return getWhiteSpace (
			// Note: the heap addresses in the comments are off by 3
			// because the start of malloc'd heap moved
			"""
			:$Malloc
			# check for zero alloction
			dup                     # [3,3]
			jumpz       :$MallocBad # [3]

			# check for negative alloction
			dup                     # [3,3]
			jumpn       :$MallocBad # [3]

			# figure out the address
			call        :$MAddress  # [0,261,3]

			# extra stuff for new addresses?
			jumpz       :$MOldInit  # [261,3]
			jump        :$MNewInit  # [261,3]

			:$MOldInit              # [261,3]
			dup                     # [261,261,3]
			copyn       2           # [3,261,261,3]
			add                     # [264,261,3]
			jump        :$MInitNext # [264,261,3]

			# figure largest address to use
			:$MNewInit              # [261,3]
			dup                     # [261,261,3]
			copyn       2           # [3,261,261,3]
			add                     # [264,261,3]

			# point this list item to the next one
			dup                     # [264,264,261,3]
			copyn       2           # [261,264,264,261,3]
			push        2           # [2,261,264,264,261,3]
			sub                     # [259,264,264,261,3]
			swap                    # [264,259,264,261,3]
			store                   # [264,261,3]

			# set next list item
			dup                     # [264,264,261,3]
			push        0           # [0,264,264,261,3]
			store                   # [264,261,3]
			dup                     # [264,264,261,3]
			push        1           # [1,264,264,261,3]
			add                     # [265,264,261,3]
			push        0           # [0,265,264,261,3]
			store                   # [264,261,3]

			# init new addresses to 0
			:$MInitNext             # [264,261,3]
			push        1           # [1,264,261,3]
			sub                     # [263,261,3]

			# write zero there
			dup                     # [263,263,261,3]
			push        0           # [0,263,263,261,3]
			store                   # [263,261,3]

			# are we done?
			dup                     # [263,263,261,3]
			copyn       2           # [261,263,263,261,3]
			sub                     # [2,263,261,3]
			jumpz       :$MDone     # [263,261,3]
			jump        :$MInitNext # [263,261,3]

			:$MDone                 # [263,261,3]
			# get rid of the address we used for initialising to 0
			drop                    # [261,3]

			# get rid of the allocation size
			slide       1           # [261]

			# set the in-use flag
			dup                     # [261,261]
			push        1           # [1,261,261]
			sub                     # [260,261]
			push        1           # [1,260,261]
			store                   # [261]

			return                  # [261]


			:$MAddress              # [3]
			# start with first block
			push        256         # [259,3]
			:$MTryBlock             # [259,3]
			dup                     # [259,259,3]
			retrieve                # [264,259,3]
			dup                     # [264,264,259,3]
			# if it's a zero, add a new block
			jumpz       :$MNewBlck  # [264,259,3]
			# 264 is the next block, need to check if 259 is the right size
			dup                     # [264,264,259,3]
			copyn       2           # [259,264,264,259,3]
			sub                     # [5,264,259,3]
			push        2           # [2,5,264,259,3]
			sub                     # [3,264,259,3]
			copyn       3           # [3,3,264,259,3]
			sub                     # [0,264,259,3]
			jumpz       :$MChkFree  # [264,259,3]
			# it's the wrong size, so get ready to try next block
			:$MPreTry               # [264,259,3]
			swap                    # [259,264,3]
			drop                    # [264,3]
			jump        :$MTryBlock # [264,3]

			# 259 is the right size, but is it free?
			:$MChkFree              # [264,259,3]
			copyn       1           # [259,264,259,3]
			push        1           # [1,259,264,259,3]
			add                     # [260,264,259,3]
			retrieve                # [0,264,259,3]
			jumpz       :$MReUse    # [264,259,3]

			# 259 isn't free, so go on to the next block
			jump        :$MPreTry   # [264,259,3]

			# 259 is free, so we can use it
			:$MReUse                # [264,259,3]
			drop                    # [259,3]
			push        2           # [2,259,3]
			add                     # [261,3]
			# tell the caller there's less setup to do
			push        0           # [1,261,3]
			return                  # [1,261,3]

			:$MNewBlck              # [264,259,3]
			drop                    # [259,3]
			push        2           # [2,259,3]
			add                     # [261,3]
			# tell the caller there's more setup to do
			push        1           # [1,261,3]
			return                  # [1,261,3]

			:$MallocBad             # [3]
			drop                    # []
			push        -1          # [-1]
			return                  # [-1]
			"""
		);
	}

	private String mallocSetup1 ()
			throws IOException {
		return getWhiteSpace ( "call", ":$MSetup" );
	}

	private String mallocSetup2 ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$MSetup
			push   256
			push   0
			store
			push   260
			push   0
			store
			return
			"""
		);
	}

	private String makeBlockSz ( final String param, final int line )
			throws IOException {
		need ( "$blocksz" );

		return getWhiteSpace ( "call", ":$BlockSz", line );
	}

	private String getBlockSzRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$BlockSz
			dup
			push 2
			sub
			retrieve
			swap
			sub
			return
			"""
		);
	}

	private String makeFree ( final String param, final int line )
			throws IOException {
		need ( "$free" );

		return getWhiteSpace ( "call", ":$Free", line );
	}

	private String getFreeRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Free
			push   1
			sub
			push   0
			store
			return
			"""
		);
	}

	private String makeFreeArr ( final String param, final int line )
			throws IOException {
		need ( "$freearr" );

		return getWhiteSpace ( "call", ":$FreeArr", line );
	}

	private String getFreeArrRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$FreeArr            # [b]
			dup                  # [b,b]
			:$FreeArrNext
			dup                  # [b,b,b]
			retrieve             # [b1,b,b]
			dup                  # [b1,b1,b,b]
			jumpz :$FreeArrDone  # [b1,b,b]
			$free                # [b,b]
			push 1               # [1,b,b]
			add                  # [b+1,b]
			jump :$FreeArrNext   # [b+1,b]

			:$FreeArrDone        # [b1,b,b]
			drop                 # [b,b]
			drop                 # [b]
			$free                # []
			return               # []
			"""
		);
	}

	private String makeLstNew ( final String param, final int line )
			throws IOException {
		need ( "$lstnew" );

		return getWhiteSpace ( "call", ":$LstNew", line );
	}

	private String getLstNewRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$LstNew # []
			push 2   # [2]
			$malloc  # [pLst]
			dup      # [pLst,pLst]
			push 8   # [8,pLst,pLst]
			$malloc  # [pLstArr,pLst,pLst]
			store    # [pLst]
			return   # [pLst]
			"""
		);
	}

	private String makeLstSz ( final String param, final int line )
			throws IOException {
		need ( "$lstsz" );

		return getWhiteSpace ( "call", ":$LstSz", line );
	}

	private String getLstSzRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$LstSz  # [pLst]
			push 1   # [1,pLst]
			add      # [pLstSz]
			retrieve # [lstSz]
			return   # [lstSz]
			"""
		);
	}

	private String makeLstAdd ( final String param, final int line )
			throws IOException {
		need ( "$lstadd" );

		return getWhiteSpace ( "call", ":$LstAdd", line );
	}

	private String getLstAddRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$LstAdd           # [nyu,pLst]
			copyn 1            # [pLst,nyu,pLst]
			$lstsz             # [lstSz,nyu,pLst]
			dup                # [lstSz,lstSz,nyu,pLst]
			copyn 3            # [pLst,lstSz,lstSz,nyu,pLst]
			retrieve           # [pLstArr,lstSz,lstSz,nyu,pLst]
			$blocksz           # [lstArrSpace+!,lstSz,lstSz,nyu,pLst]
			push 1             # [1,lstArrSpace+!,lstSz,lstSz,nyu,pLst]
			sub                # [lstArrSpace,lstSz,lstSz,nyu,pLst]
			sub                # [lstSz-lstArrSpace,lstSz,nyu,pLst]
			jumpz :$LstAddGrow # [lstSz,nyu,pLst]
			jump :$LstAddAdd   # [lstSz,nyu,pLst]

			:$LstAddGrow       # [lstSz,nyu,pLst]
			dup                # [lstSz,lstSz,nyu,pLst]
			push 2             # [2,lstSz,lstSz,nyu,pLst]
			mult               # [lstSz*2,lstSz,nyu,pLst]
			$malloc            # [pNyuArr,lstSz,nyu,pLst]
			copyn 3            # [pLst,pNyuArr,lstSz,nyu,pLst]
			retrieve           # [pLstArr,pNyuArr,lstSz,nyu,pLst]
			copyn 2            # [lstSz,pLstArr,pNyuArr,lstSz,nyu,pLst]
			push 1             # [1,lstSz,pLstArr,pNyuArr,lstSz,nyu,pLst]
			sub                # [i,pLstArr,pNyuArr,lstSz,nyu,pLst]

			:$LstAddPop        # [i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			copyn 1            # [pLstArr,i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			copyn 1            # [i,pLstArr,i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			add                # [pLstArr+i,i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			retrieve           # [item,i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			copyn 3            # [pNyuArr,item,i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			copyn 2            # [i,pNyuArr,item,i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			add                # [pNyuArr+i,item,i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			swap               # [item,pNyuArr+i,i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			store              # [i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			push 1             # [1,i,pLstArr,pNyuArr,lstSz,nyu,pLst]
			sub                # [i-1,pLstArr,pNyuArr,lstSz,nyu,pLst]
			dup                # [i-1,i-1,pLstArr,pNyuArr,lstSz,nyu,pLst]
			jumpn :$LstAddPopN # [i-1,pLstArr,pNyuArr,lstSz,nyu,pLst]
			jump :$LstAddPop   # [i-1,pLstArr,pNyuArr,lstSz,nyu,pLst]

			:$LstAddPopN       # [i-1,pLstArr,pNyuArr,lstSz,nyu,pLst]
			drop               # [pLstArr,pNyuArr,lstSz,nyu,pLst]
			drop               # [pNyuArr,lstSz,nyu,pLst]
			slide 1            # [pNyuArr,nyu,pLst]
			copyn 2            # [pLst,pNyuArr,nyu,pLst]
			retrieve           # [pLstArr,pNyuArr,nyu,pLst]
			$free              # [pNyuArr,nyu,pLst]
			copyn 2            # [pLst,pNyuArr,nyu,pLst]
			swap               # [pNyuArr,pLst,nyu,pLst]
			store              # [nyu,pLst]
			copyn 1            # [pLst,nyu,pLst]
			$lstsz             # [lstSz,nyu,pLst]
			:$LstAddAdd        # [lstSz,nyu,pLst]
			copyn 2            # [pLst,lstSz,nyu,pLst]
			retrieve           # [pLstArr,lstSz,nyu,pLst]
			add                # [pLstArr+lstSz,nyu,pLst]
			swap               # [nyu,pLstArr+lstSz,pLst]
			store              # [pLst]
			dup                # [pLst,pLst]
			push 1             # [1,pLst,pLst]
			add                # [pLstSz,pLst]
			dup                # [pLstSz,pLstSz,pLst]
			retrieve           # [oldLstSz,pLstSz,pLst]
			push 1             # [1,oldLstSz,pLstSz,pLst]
			add                # [newLstSz,pLstSz,pLst]
			store              # [pLst]
			drop               # []
			return             # []
			"""
		);
	}

	private String makeLstGet ( final String param, final int line )
			throws IOException {
		need ( "$lstget" );

		return getWhiteSpace ( "call", ":$LstGet", line );
	}

	private String getLstGetRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$LstGet          # [i,pLst]
			dup               # [i,i,pLst]
			jumpn :$LstGetBad # [i,pLst]
			dup               # [i,i,pLst]
			copyn 2           # [pLst,i,i,pLst]
			push 1            # [1,pLst,i,i,pLst]
			add               # [pLstSz,i,i,pLst]
			retrieve          # [lstSz,i,i,pLst]
			sub               # [1-lstSz,i,pLst]
			jumpn :$LstGetOk  # [i,pLst]
			jump :$LstGetBad  # [i,pLst]
			:$LstGetOk        # [i,pLst]
			swap              # [pLst,i]
			retrieve          # [pLstArr,i]
			add               # [pElement]
			retrieve          # [element]
			push 0            # [0,element]
			return            # [0,element]

			:$LstGetBad       # [i,pLst]
			push -1           # [-1,i,pLst]
			slide 2           # [-1]
			return            # [-1]
			"""
		);
	}

	private String makeArr2Lst ( final String param, final int line )
			throws IOException {
		need ( "$arr2lst" );

		return getWhiteSpace ( "call", ":$Arr2Lst", line );
	}

	private String getArr2LstRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Arr2Lst           # [pArr]
			# Make a list to put the array contents into
			$lstnew             # [pLst,pArr]

			# Loop setup: "(for int i=0,c=10;..."
			copyn 1             # [pArr,pLst,pArr]
			$strlen             # [c,pLst,pArr]
			push 0              # [i,c,pLst,pArr]

			# Loop entry condition: "...;i<c;...)"
			:$Arr2LstLoop       # [i,c,pLst,pArr]
			dup                 # [i,i,c,pLst,pArr]
			copyn 2             # [c,i,i,c,pLst,pArr]
			sub                 # [i-c,i,c,pLst,pArr]
			jumpn :$Arr2LstBody # [i,c,pLst,pArr]
			jump :$Arr2LstDone  # [i,c,pLst,pArr]

			# Loop body
			:$Arr2LstBody       # [i,c,pLst,pArr]
			copyn 2             # [pLst,i,c,pLst,pArr]
			copyn 4             # [pArr,pLst,i,c,pLst,pArr]
			copyn 2             # [i,pArr,pLst,i,c,pLst,pArr]
			add                 # [arr[i],pLst,i,c,pLst,pArr]
			retrieve            # [elem,pLst,i,c,pLst,pArr]
			$lstadd             # [i,c,pLst,pArr]

			# Loop tail increment: "...;i++)"
			push 1              # [1,i,c,pLst,pArr]
			add                 # [i+1,c,pLst,pArr]
			jump :$Arr2LstLoop  # [i+1,c,pLst,pArr]

			:$Arr2LstDone       # [i,c,pLst,pArr]
			drop                # [c,pLst,pArr]
			drop                # [pLst,pArr]
			slide 1             # [pLst]
			return              # [pLst]
			"""
		);
	}

	private String makeLst2Arr ( final String param, final int line )
			throws IOException {
		need ( "$lst2arr" );

		return getWhiteSpace ( "call", ":$Lst2Arr", line );
	}

	private String getLst2ArrRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Lst2Arr # [pLst]
			dup       # [pLst,pLst]
			$lstsz    # [lstSz,pLst]
			dup       # [lstSz,lstSz,pLst]
			push 1    # [1,lstSz,lstSz,pLst]
			add       # [lstSz+1,lstSz,pLst]
			$malloc   # [pArr,lstSz,pLst]
			copyn 2   # [pLst,pArr,lstSz,pLst]
			retrieve  # [pLstArr,pArr,lstSz,pLst]
			copyn 2   # [lstSz,pLstArr,pArr,lstSz,pLst]
			$strncpy  # [pArr,lstSz,pLst]
			slide 2   # [pArr]
			return    # [pArr]
			"""
		);
	}

	private String makeLstDup ( final String param, final int line )
			throws IOException {
		need ( "$lstdup" );

		return getWhiteSpace ( "call", ":$LstDup", line );
	}

	private String getLstDupRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$LstDup #[pSource]
			retrieve #[pSourceArr]
			$arr2lst #[pResult]
			return   #[pResult]
			"""
		);
	}

	private String makeLstRev ( final String param, final int line )
			throws IOException {
		need ( "$lstrev" );

		return getWhiteSpace ( "call", ":$LstRev", line );
	}

	private String getLstRevRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$LstRev           # [pLst]
			dup                # [pLst,pLst]
			push 1             # [1,pLst,pLst]
			add                # [pSz,pLst]
			retrieve           # [sz,pLst]
			swap               # [pLst,sz]
			retrieve           # [pArr,sz]
			copyn 1            # [sz,pArr,sz]
			push 2             # [2,sz,pArr,sz]
			div                # [count,pArr,sz]
			push 0             # [i,count,pArr,sz]
			:$LstRev:loop      # [i,count,pArr,sz]
			# swap?
			copyn 1            # [count,i,count,pArr,sz]
			copyn 1            # [i,count,i,count,pArr,sz]
			sub                # [count-i,i,count,pArr,sz]
			jumpz :$LstRev:end # [i,count,pArr,sz]
			# swap!
			copyn 2            # [pArr,i,count,pArr,sz]
			copyn 1            # [i,pArr,i,count,pArr,sz]
			add                # [pLo,i,count,pArr,sz]
			dup                # [pLo,pLo,i,count,pArr,sz]
			retrieve           # [elemLo,pLo,i,count,pArr,sz]
			copyn 5            # [sz,elemLo,pLo,i,count,pArr,sz]
			copyn 3            # [i,sz,elemLo,pLo,i,count,pArr,sz]
			sub                # [sz-i,elemLo,pLo,i,count,pArr,sz]
			push 1             # [1,sz-i,elemLo,pLo,i,count,pArr,sz]
			sub                # [iHi,elemLo,pLo,i,count,pArr,sz]
			copyn 5            # [pArr,iHi,elemLo,pLo,i,count,pArr,sz]
			add                # [pHi,elemLo,pLo,i,count,pArr,sz]
			dup                # [pHi,pHi,elemLo,pLo,i,count,pArr,sz]
			retrieve           # [elemHi,pHi,elemLo,pLo,i,count,pArr,sz]
			swap               # [pHi,elemHi,elemLo,pLo,i,count,pArr,sz]
			copyn 2            # [elemLo,pHi,elemHi,elemLo,pLo,i,count,pArr,sz]
			store              # [elemHi,elemLo,pLo,i,count,pArr,sz]
			copyn 2            # [pLo,elemHi,elemLo,pLo,i,count,pArr,sz]
			swap               # [elemHi,pLo,elemLo,pLo,i,count,pArr,sz]
			store              # [elemLo,pLo,i,count,pArr,sz]
			drop               # [pLo,i,count,pArr,sz]
			drop               # [i,count,pArr,sz]
			# increment index and go again
			push 1             # [1,i,count,pArr,sz]
			add                # [i+1,count,pArr,sz]
			jump :$LstRev:loop # [i+1,count,pArr,sz]

			:$LstRev:end       # [i,count,pArr,sz]
			drop               # [count,pArr,sz]
			drop               # [pArr,sz]
			drop               # [sz]
			drop               # []
			return             # []
			"""
		);
	}

	private String makeFreeLst ( final String param, final int line )
			throws IOException {
		need ( "$freelst" );

		return getWhiteSpace ( "call", ":$FreeLst", line );
	}

	private String getFreeLstRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$FreeLst # [pLst]
			dup       # [pLst,pLst]
			retrieve  # [pLstArr,pLst]
			$freearr  # [pLst]
			$free     # []
			return    # []
			"""
		);
	}

	private String makeFreeLstLite ( final String param, final int line )
			throws IOException {
		need ( "$freelstlite" );

		return getWhiteSpace ( "call", ":$FreeLstLite", line );
	}

	private String getFreeLstLiteRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$FreeLstLite # [pList]
			dup           # [pList,pList]
			retrieve      # [pListArr,pList]
			$free         # [pList]
			$free         # []
			return        # []
			"""
		);
	}

	private String makeMapNew ( final String param, final int line )
			throws IOException {
		need ( "$mapnew" );

		return getWhiteSpace ( "call", ":$MapNew", line );
	}

	private String getMapNewRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$MapNew              # []
			@$MapNew:buckets 7

			push 2                # [2]
			$malloc               # [pMap]
			dup                   # [pMap,pMap]
			push @$MapNew:buckets # [@$MapNew:buckets,pMap,pMap]
			$malloc               # [pBuckets,pMap,pMap]

			push @$MapNew:buckets # [i,pBuckets,pMap,pMap]

			:$MapNew:loop         # [i,pBuckets,pMap,pMap]
			push 1                # [1,i,pBuckets,pMap,pMap]
			sub                   # [i,pBuckets,pMap,pMap]
			dup                   # [i,i,pBuckets,pMap,pMap]
			jumpn :$MapNew:xloop  # [i,pBuckets,pMap,pMap]
			copyn 1               # [pBuckets,i,pBuckets,pMap,pMap]
			copyn 1               # [i,pBuckets,i,pBuckets,pMap,pMap]
			add                   # [ppBucket,i,pBuckets,pMap,pMap]
			$lstnew               # [pBucket,ppBucket,i,pBuckets,pMap,pMap]
			store                 # [i,pBuckets,pMap,pMap]
			jump :$MapNew:loop    # [i,pBuckets,pMap,pMap]

			:$MapNew:xloop        # [i,pBuckets,pMap,pMap]
			drop                  # [pBuckets,pMap,pMap]
			store                 # [pMap]
			return                # [pMap]
			"""
		);
	}

	private String makeMapSz ( final String param, final int line )
			throws IOException {
		need ( "$mapsz" );

		return getWhiteSpace ( "call", ":$MapSz", line );
	}

	private String getMapSzRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$MapSz  # [pMap]
			push 1   # [1,pMap]
			add      # [pMapSz]
			retrieve # [sz]
			return   # [sz]
			"""
		);
	}

	private String makeHash2Bucket ( final String param, final int line )
			throws IOException {
		need ( "$$hash2bucket" );

		return getWhiteSpace ( "call", ":$$Hash2Bucket", line );
	}

	private String getHash2BucketRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$$Hash2Bucket # [pMap,hash]
			retrieve       # [pBuckets,hash]
			$blocksz       # [bucketCount,hash]
			mod            # [bucketIndex]
			return         # [bucketIndex]
			"""
		);
	}

	private String makeMapBFind ( final String param, final int line )
			throws IOException {
		need ( "$$mapbfind" );

		return getWhiteSpace ( "call", ":$$MapBFind", line );
	}

	private String getMapBFindRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$$MapBFind           # [pBucket,pKey]
			# check each bucket entry for key equality
			# return entry if found
			# return -1 if not found
			dup                   # [pBucket,pBucket,pKey]
			$lstsz                # [size,pBucket,pKey]
			:$$MapBFind:loop      # [i,pBucket,pKey]
			push 1                # [1,i,pBucket,pKey]
			sub                   # [i,pBucket,pKey]
			dup                   # [i,i,pBucket,pKey]
			jumpn :$$MapBFind:bad # [i,pBucket,pKey]
			copyn 1               # [pBucket,i,pBucket,pKey]
			copyn 1               # [i,pBucket,i,pBucket,pKey]
			$lstget               # [0,pEntry,i,pBucket,pKey]
			jumpz :$$MapBFind:cnt # [pEntry,i,pBucket,pKey]
			$print "Bad bucket in map\\n"
			end 1
			:$$MapBFind:cnt       # [pEntry,i,pBucket,pKey]
			dup                   # [pEntry,pEntry,i,pBucket,pKey]
			retrieve              # [pEntryKey,pEntry,i,pBucket,pKey]
			copyn 4               # [pKey,pEntryKey,pEntry,i,pBucket,pKey]
			$strcmp               # [0/n,pEntry,i,pBucket,pKey]
			jumpz :$$MapBFind:ok  # [pEntry,i,pBucket,pKey]
			drop                  # [i,pBucket,pKey]
			jump :$$MapBFind:loop # [i,pBucket,pKey]

			:$$MapBFind:bad       # [i,pBucket,pKey]
			push -1               # [-1,i,pBucket,pKey]
			slide 3               # [-1]
			return                # [-1]

			:$$MapBFind:ok        # [pEntry,i,pBucket,pKey]
			slide 3               # [pEntry]
			return                # [pEntry]
			"""
		);
	}

	private String makeMapPut ( final String param, final int line )
			throws IOException {
		need ( "$mapput" );

		return getWhiteSpace ( "call", ":$MapPut", line );
	}

	private String getMapPutRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$MapPut            # [pValue,pKey,pMap]
			##===========================
			## Consider rehashing here??
			##===========================
			# calculate a hash of the key
			copyn 1             # [pKey,pValue,pKey,pMap]
			$strhash            # [hash,pValue,pKey,pMap]

			# map the hash to a bucket
			copyn 3             # [pMap,hash,pValue,pKey,pMap]
			$$hash2bucket       # [bucketIndex,pValue,pKey,pMap]
			copyn 3             # [pMap,bucketIndex,pValue,pKey,pMap]
			retrieve            # [pBuckets,bucketIndex,pValue,pKey,pMap]
			add                 # [ppBucket,pValue,pKey,pMap]
			retrieve            # [pBucket,pValue,pKey,pMap]

			# is the key already mapped?
			copyn 2             # [pKey,pBucket,pValue,pKey,pMap]
			copyn 1             # [pBucket,pKey,pBucket,pValue,pKey,pMap]
			$$mapbfind          # [pOldEntry,pBucket,pValue,pKey,pMap]
			dup                 # [pOldEntry,pOldEntry,pBucket,pValue,pKey,pMap]
			jumpn :$MapPut:new  # [pOldEntry,pBucket,pValue,pKey,pMap]

			# replace revious value
			push 1              # [1,pOldEntry,pBucket,pValue,pKey,pMap]
			add                 # [pOldEntryValue,pBucket,pValue,pKey,pMap]
			copyn 2             # [pValue,pOldEntryValue,pBucket,pValue,pKey,pMap]
			store               # [pBucket,pValue,pKey,pMap]
			slide 3             # [pBucket]
			drop                # []
			return              # []

			:$MapPut:new        # [-1,pBucket,pValue,pKey,pMap]
			drop                # [pBucket,pValue,pKey,pMap]
			# make a map entry
			push 2              # [2,pBucket,pValue,pKey,pMap]
			$malloc             # [pEntry,pBucket,pValue,pKey,pMap]
			dup                 # [pEntry,pEntry,pBucket,pValue,pKey,pMap]
			copyn 4             # [pKey,pEntry,pEntry,pBucket,pValue,pKey,pMap]
			store               # [pEntry,pBucket,pValue,pKey,pMap]
			dup                 # [pEntry,pEntry,pBucket,pValue,pKey,pMap]
			push 1              # [1,pEntry,pEntry,pBucket,pValue,pKey,pMap]
			add                 # [pEntryValue,pEntry,pBucket,pValue,pKey,pMap]
			copyn 3             # [pValue,pEntryValue,pEntry,pBucket,pValue,pKey,pMap]
			store               # [pEntry,pBucket,pValue,pKey,pMap]

			# add the entry to the bucket
			$lstadd             # [pValue,pKey,pMap]
			drop                # [pKey,pMap]
			drop                # [pMap]
			push 1              # [1,pMap]
			add                 # [pMapSz]
			$incr               # []
			return              # []
			"""
		);
	}

	private String makeMapGet ( final String param, final int line )
			throws IOException {
		need ( "$mapget" );

		return getWhiteSpace ( "call", ":$MapGet", line );
	}

	private String getMapGetRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$MapGet            # [pKey,pMap]
			# calculate a hash of the key
			dup                 # [pKey,pKey,pMap]
			$strhash            # [hash,pKey,pMap]

			# map the hash to a bucket
			copyn 2             # [pMap,hash,pKey,pMap]
			$$hash2bucket       # [bucketIndex,pKey,pMap]
			copyn 2             # [pMap,bucketIndex,pKey,pMap]
			retrieve            # [pBucketArray,bucketIndex,pKey,pMap]
			add                 # [ppBucket,pKey,pMap]
			retrieve            # [pBucket,pKey,pMap]

			# look for key in bucket
			$$mapbfind          # [pEntry,pMap]
			dup                 # [pEntry,pEntry,pMap]
			jumpn :$MapGet:bad  # [pEntry,pMap]
			push 1              # [1,pEntry,pMap]
			add                 # [pEntryValue,pMap]
			retrieve            # [pValue,pMap]
			slide 1             # [pValue]
			return              # [pValue]

			:$MapGet:bad        # [-1,pEntry,pMap]
			slide 2             # [-1]
			return              # [-1]
			"""
		);
	}

	private String makeReadStr ( final String param, final int line )
			throws IOException {
		need ( "$readstr" );

		return getWhiteSpace ( "call", ":$ReadStr", line );
	}

	private String getReadStrRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$ReadStr              # []
			# List to store chars
			$lstnew                # [pLst]

			# Address to read char into
			push 1                 # [1,pLst]
			$malloc                # [pChar,pLst]

			:$ReadStr:char         # [pChar,pLst]
			# Read a char
			dup                    # [pChar,pChar,pLst]
			readc                  # [pChar,pLst]
			dup                    # [pChar,pChar,pLst]
			retrieve               # [char,pChar,pLst]

			# Is it EOF?
			dup                    # [char,char,pChar,pLst]
			jumpn :$ReadStr:EOF    # [char,pChar,pLst]

			# Is it CR?
			dup                    # [char,char,pChar,pLst]
			push '\\r'             # ['\\r',char,char,pChar,pLst]
			sub                    # [diff,char,pChar,pLst]
			jumpz :$ReadStr:winCR  # [char,pChar,pLst]

			# Is it LF?
			dup                    # [char,char,pChar,pLst]
			push '\\n'             # ['\\n',char,char,pChar,pLst]
			sub                    # [diff,char,pChar,pLst]
			jumpz :$ReadStr:gotStr # [char,pChar,pLst]

			# Add to list and get next
			copyn 2                # [pLst,char,pChar,pLst]
			swap                   # [char,pLst,pChar,pLst]
			$lstadd                # [pChar,pLst]
			jump :$ReadStr:char    # [pChar,pLst]

			:$ReadStr:winCR        # [char,pChar,pLst]
			# We got a CR, so we assume that the next char is LF and throw it away
			copyn 1                # [pChar,char,pChar,pLst]
			readc                  # [char,pChar,pLst]
			# fallthrough

			:$ReadStr:gotStr       # [char,pChar,pLst]
			drop                   # [pChar,pLst]
			$free                  # [pLst]

			# turn list into string
			:$ReadStr:2str         # [pLst]
			dup                    # [pLst,pLst]
			$lst2arr               # [pArr,pLst]
			swap                   # [pLst,pArr]
			$freelstlite           # [pArr]
			return                 # [pArr]

			:$ReadStr:EOF          # [-1,pChar,pLst]
			drop                   # [pChar,pLst]
			$free                  # [pLst]
			dup                    # [pLst,pLst]
			$lstsz                 # [sz,pLst]
			jumpz :$ReadStr:EOFEmp # [pLst]
			jump :$ReadStr:2str    # [pLst]
			:$ReadStr:EOFEmp       # [pLst]
			$freelstlite           # []
			push -1                # [-1]
			return                 # [-1]
			"""
		);
	}

	private String makePrintStr ( final String param, final int line )
			throws IOException {
		need ( "$printstr" );

		return getWhiteSpace ( "call", ":$PrintStr", line );
	}

	private String getPrintStrRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$PrintStr
			dup
			retrieve
			dup
			jumpz    :$PrintStrRet
			printc
			push     1
			add
			jump     :$PrintStr

			:$PrintStrRet
			drop
			drop
			return
			"""
		);
	}

	private String makePrintC ( final String param, final int line )
			throws IOException {
		return getWhiteSpace (
			"""
			dup
			printc
			push   10
			printc
			"""
		);
	}

	private String makePrintN ( final String param, final int line )
			throws IOException {
		return getWhiteSpace (
			"""
			dup
			printn
			push   10
			printc
			"""
		);
	}

	private String makeStr2Int ( final String param, final int line )
			throws IOException {
		need ( "$str2int" );

		return getWhiteSpace ( "call", ":$Str2Int", line );
	}

	private String getStr2IntRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Str2Int
			# initialise
			push 1                   # [1,pStr]
			$malloc                  # [ppStr,pStr]
			dup                      # [ppStr,ppStr,pStr]
			copyn 2                  # [pStr,ppStr,ppStr,pStr]
			store                    # [ppStr,pStr]
			slide 1                  # [ppStr]
			push 1                   # [1,ppStr]
			$malloc                  # [pTotal,ppStr]
			push 1                   # [1,pTotal,ppStr]
			$malloc                  # [pDigits,pTotal,ppStr]
			dup                      # [pDigits,pDigits,pTotal,ppStr]
			push -1                  # [-1,pDigits,pDigits,pTotal,ppStr]
			store                    # [pDigits,pTotal,ppStr]

			# Check for '+' or '-'
			copyn 2                  # [ppStr,pDigits,pTotal,ppStr]
			retrieve                 # [pStr,pDigits,pTotal,ppStr]
			retrieve                 # [char,pDigits,pTotal,ppStr]
			push '+'                 # ['+',char,pDigits,pTotal,ppStr]
			sub                      # [diff,pDigits,pTotal,ppStr]
			jumpz :$Str2Int:plus     # [pDigits,pTotal,ppStr]
			copyn 2                  # [ppStr,pDigits,pTotal,ppStr]
			retrieve                 # [pStr,pDigits,pTotal,ppStr]
			retrieve                 # [char,pDigits,pTotal,ppStr]
			push '-'                 # ['-',char,pDigits,pTotal,ppStr]
			sub                      # [diff,pDigits,pTotal,ppStr]
			jumpz :$Str2Int:minus    # [pDigits,pTotal,ppStr]
			jump :$Str2Int:nosign    # [pDigits,pTotal,ppStr]

			:$Str2Int:minus          # [pDigits,pTotal,ppStr]
			push -1                  # [-1,pDigits,pTotal,ppStr]
			copyn 3                  # [ppStr,-1,pDigits,pTotal,ppStr]
			$incr                    # [-1,pDigits,pTotal,ppStr]
			jump :$Str2Int:start     # [-1,pDigits,pTotal,ppStr]

			:$Str2Int:plus           # [pDigits,pTotal,ppStr]
			push 1                   # [1,pDigits,pTotal,ppStr]
			copyn 3                  # [ppStr,1,pDigits,pTotal,ppStr]
			$incr                    # [1,pDigits,pTotal,ppStr]
			jump :$Str2Int:start     # [1,pDigits,pTotal,ppStr]

			:$Str2Int:nosign         # [pDigits,pTotal,ppStr]
			push 1                   # [1,pDigits,pTotal,ppStr]
			jump :$Str2Int:start     # [1,pDigits,pTotal,ppStr]

			:$Str2Int:start          # [1|-1,pDigits,pTotal,ppStr]
			# Get character (which we expect to be a digit)
			copyn 3                  # [ppStr,1|-1,pDigits,pTotal,ppStr]
			retrieve                 # [pStr,1|-1,pDigits,pTotal,ppStr]
			retrieve                 # [char,1|-1,pDigits,pTotal,ppStr]

			# Check for null terminator
			dup                      # [char,char,1|-1,pDigits,pTotal,ppStr]
			jumpz :$Str2Int:done     # [char,1|-1,pDigits,pTotal,ppStr]

			# Check if too high to be a digit
			dup                      # [char,char,1|-1,pDigits,pTotal,ppStr]
			push '9'                 # ['9',char,char,1|-1,pDigits,pTotal,ppStr]
			swap                     # [char,'9',char,1|-1,pDigits,pTotal,ppStr]
			sub                      # [diff,char,1|-1,pDigits,pTotal,ppStr]
			jumpn :$Str2Int:digitBad # [char,1|-1,pDigits,pTotal,ppStr]
			# Check if too low to be a digit
			dup                      # [char,char,1|-1,pDigits,pTotal,ppStr]
			push '0'                 # ['0',char,char,1|-1,pDigits,pTotal,ppStr]
			sub                      # [diff,char,1|-1,pDigits,pTotal,ppStr]
			jumpn :$Str2Int:digitBad # [char,1|-1,pDigits,pTotal,ppStr]
			jump :$Str2Int:digitOk   # [char,1|-1,pDigits,pTotal,ppStr]

			:$Str2Int:digitBad       # [char,1|-1,pDigits,pTotal,ppStr]
			drop                     # [1|-1,pDigits,pTotal,ppStr]
			drop                     # [pDigits,pTotal,ppStr]
			$free                    # [pTotal,ppStr]
			$free                    # [ppStr]
			drop                     # []
			push -99                 # [-99]
			return

			:$Str2Int:digitOk        # [char,1|-1,pDigits,pTotal,ppStr]
			# mark digits received
			copyn 2                  # [pDigits,char,1|-1,pDigits,pTotal,ppStr]
			push 0                   # [0,pDigits,char,1|-1,pDigits,pTotal,ppStr]
			store                    # [char,1|-1,pDigits,pTotal,ppStr]
			# Include digit in running total
			push '0'                 # ['0',char,1|-1,pDigits,pTotal,ppStr]
			sub                      # [digit,1|-1,pDigits,pTotal,ppStr]
			copyn 3                  # [pTotal,digit,1|-1,pDigits,pTotal,ppStr]
			retrieve                 # [total,digit,1|-1,pDigits,pTotal,ppStr]
			push 10                  # [10,total,digit,1|-1,pDigits,pTotal,ppStr]
			mult                     # [total*10,digit,1|-1,pDigits,pTotal,ppStr]
			add                      # [total*10+digit,1|-1,pDigits,pTotal,ppStr]
			copyn 3                  # [pTotal,total*10+digit,1|-1,pDigits,pTotal,ppStr]
			swap                     # [total*10+digit,pTotal,1|-1,pDigits,pTotal,ppStr]
			store                    # [1|-1,pDigits,pTotal,ppStr]

			# Increment counter to get next digit
			copyn 3                  # [ppStr,1|-1,pDigits,pTotal,ppStr]
			$incr                    # [1|-1,pDigits,pTotal,ppStr]
			jump :$Str2Int:start     # [1|-1,pDigits,pTotal,ppStr]

			:$Str2Int:done           # [char,1|-1,pDigits,pTotal,ppStr]
			# All done. Get result.
			drop                     # [1|-1,pDigits,pTotal,ppStr]
			swap                     # [pDigits,1|-1,pTotal,ppStr]
			dup                      # [pDigits,pDigits,1|-1,pTotal,ppStr]
			retrieve                 # [0/-99,pDigits,1|-1,pTotal,ppStr]
			jumpz :$Str2Int:ok       # [pDigits,1|-1,pTotal,ppStr]
			$free                    # [1|-1,pTotal,ppStr]
			drop                     # [pTotal,ppStr]
			$free                    # [ppStr]
			drop                     # []
			push -99                 # [-99]
			return                   # [-99]

			:$Str2Int:ok             # [pDigits,1|-1,pTotal,ppStr]
			$free                    # [1|-1,pTotal,ppStr]
			swap                     # [pTotal,1|-1,ppStr]
			dup                      # [pTotal,pTotal,1|-1,ppStr]
			retrieve                 # [total,pTotal,1|-1,ppStr]
			swap                     # [pTotal,total,1|-1,ppStr]
			$free                    # [total,1|-1,ppStr]
			mult                     # [-total|total,ppStr]
			swap                     # [ppStr,-total|total]
			$free                    # [-total|total]
			push 0                   # [0,-total|total]
			return                   # [0,-total|total]
			"""
		);
	}

	private String makeInt2Str ( final String param, final int line )
			throws IOException {
		need ( "$int2str" );

		return getWhiteSpace ( "call", ":$Int2Str", line );
	}

	private String getInt2StrRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Int2Str           # [-57]
			dup                 # [-57,-57]
			jumpz :$Int2Str:nil # [-57]
			dup                 # [-57,-57]
			# if it's -ve, make +ve and add sign later
			jumpn :$Int2Str:neg # [-57]
			push 1              # [1,-57]
			jump :$Int2Str:bgn  # [1,-57]
			:$Int2Str:neg       # [-57]
			push -1             # [-1,-57]
			mult                # [57]
			push -1             # [-1,57]
			:$Int2Str:bgn       # [1/-1,57]
			swap                # [57,1/-1]
			# Digits go in a list in reverse order...
			$lstnew             # [pLst,57,1/-1]
			# ...starting with null terminator
			dup                 # [pLst,pLst,57,1/-1]
			push 0              # [0,pLst,pLst,57,1/-1]
			$lstadd             # [pLst,57,1/-1]
			swap                # [57,pLst,1/-1]
			:$Int2Str:loop      # [57,pLst,1/-1]
			# Get least signif digit
			dup                 # [57,57,pLst,1/-1]
			push 10             # [10,57,57,pLst,1/-1]
			mod                 # [7,57,pLst,1/-1]
			push '0'            # ['0',7,57,pLst,1/-1]
			add                 # ['7',57,pLst,1/-1]
			copyn 2             # [pLst,'7',57,pLst,1/-1]
			swap                # ['7',pLst,57,pLst,1/-1]
			$lstadd             # [57,pLst,1/-1]
			# Get what's left
			push 10             # [10,57,pLst,1/-1]
			div                 # [5,pLst,1/-1]
			dup                 # [5,5,pLst,1/-1]
			jumpz :$Int2Str:end # [5,5,pLst,1/-1]
			jump :$Int2Str:loop # [5,pLst,1/-1]

			:$Int2Str:end       # [0,pLst,1/-1]
			drop                # [pLst,1/-1]
			swap                # [1/-1,pLst]
			jumpn :$Int2Str:n2  # [pLst]
			jump :$Int2Str:e2   # [pLst]
			:$Int2Str:n2        # [pLst]
			dup                 # [pLst,pLst]
			push '-'            # ['-',pLst,pLst]
			$lstadd             # [pLst]
			:$Int2Str:e2        # [pLst]
			dup                 # [pLst,pLst]
			$lstrev             # [pLst]
			dup                 # [pLst,pLst]
			$lst2arr            # [pStr,pLst]
			swap                # [pLst,pStr]
			$freelstlite        # [pStr]
			return              # [pStr]

			:$Int2Str:nil       # [0]
			$storestr "0"       # [pStr,0]
			slide 1             # [pStr]
			return              # [pStr]
			"""
		);
	}

	private String makeStrLen ( final String param, final int line )
			throws IOException {
		need ( "$strlen" );

		return getWhiteSpace ( "call", ":$StrLen", line );
	}

	private String getStrLenRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StrLen   # [p]
			push     0 # [0,p]
			$strlens   # [n]
			return
			"""
		);
	}

	private String makeStrLenS ( final String param, final int line )
			throws IOException {
		need ( "$strlens" );

		return getWhiteSpace ( "call", ":$StrLenS", line );
	}

	private String getStrLenSRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StrLenS             # ['.',a]

			# get character
			copyn    1            # [a,'.',a]
			retrieve              # ['x','.',a]

			# have we dropped off the string without finding the separator?
			dup                   # ['x','x','.',a]
			jumpz    :$StrLenSNul # ['x','.',a]

			# is it the separator?
			copyn    1            # ['.','x','.',a]
			sub                   # [n,'.',a]
			jumpz    :$StrLenSEnd # ['.',a]

			# get length from next character
			copyn    1            # [a,'.',a]
			push     1            # [1,a,'.',a]
			add                   # [a+1,'.',a]
			copyn    1            # ['.',a+1,'.',a]
			call     :$StrLenS    # [n,'.',a]

			# add 1 to that length and return
			push     1            # [1,n,'.',a]
			add                   # [n+1,'.',a]
			slide    2            # [n+1]
			return

			:$StrLenSNul          # ['x','.',a]
			drop                  # ['.',a]
			:$StrLenSEnd          # ['.',a]
			drop                  # [a]
			drop                  # []
			push     0            # [0]
			return
			"""
		);
	}

	private String makeStoreStr ( final String param, final int line )
			throws IOException {
		final var result  = new StringBuilder ();
		final var content = stringParam ( param, line );

		result.append ( getWhiteSpace ( "push", Integer.toString ( content.codePointCount ( 0, content.length () ) + 1 ), line ) );
		pushStr ( content, line, result );
		result.append ( getWhiteSpace ( "call", ":$StoreStr", line ) );
		need ( "$storestr" );

		return result.toString ();
	}

	private String getStoreStrRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StoreStr                    # [len]
			$malloc                       # [a,'x','y','z',0,4]
			:$StoreStrNext
			dup                           # [a,a,'x','y','z',0,4]
			copyn          2              # ['x',a,a,'x','y','z',0,4]
			dup                           # ['x','x',a,a,'x','y','z',0,4]
			jumpz          :$StoreStrDone # ['x',a,a,'x','y','z',0,4]
			store                         # [a,'x','y','z',0,4]
			swap                          # ['x',a,'y','z',0,4]
			drop                          # [a,'y','z',0,4]
			push           1              # [1,a,'y','z',0,4]
			add                           # [a+1,'y','z',0,4]
			jump           :$StoreStrNext
			:$StoreStrDone                # [0,a+n,a+n,0,4]
			drop                          # [a+n,a+n,0,4]
			drop                          # [a+n,0,4]
			swap                          # [0,a+n,4]
			drop                          # [a+n,4]
			swap                          # [4,a+n]
			push           1              # [1,4,a+n]
			sub                           # [3,a+n]
			sub                           # [a]
			return
			"""
		);
	}

	private String makeStrCmp ( final String param, final int line )
			throws IOException {
		need ( "$strcmp" );
		return getWhiteSpace ( "call", ":$StrCmp", line );
	}

	private String getStrCmpRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StrCmp                # [a,b]
			# are chars same?
			call        :$StrCmpGet # ['x','x',a,b]
			sub                     # [0,a,b]
			jumpz       :$StrCmpChk # [a,b]

			:$StrCmpBad             # [a,b]
			# found a difference
			drop                    # [b]
			drop                    # []
			push        -1          # [-1]
			return

			:$StrCmpChk             # [a,b]
			call        :$StrCmpGet # ['x','x',a,b]
			jumpz       :$StrCmp1Z  # ['x',a,b]
			jumpz       :$StrCmpBad # [a,b]
			push        1           # [1,a,b]
			add                     # [a+1,b]
			swap                    # [b,a+1]
			push        1           # [1,b,a+1]
			add                     # [b+1,a+1]
			jump        :$StrCmp

			:$StrCmp1Z              # ['x',a,b]
			jumpz       :$StrCmpYay # [a,b]
			jump        :$StrCmpBad

			:$StrCmpYay             # [a,b]
			# found a match
			drop                    # [b]
			drop                    # []
			push        0           # [0]
			return

			:$StrCmpGet
			dup                     # [a,a,b]
			retrieve                # ['x',a,b]
			copyn       2           # [b,'x',a,b]
			retrieve                # ['x','x',a,b]
			return
			"""
		);
	}

	private String makeStrCpy ( final String param, final int line )
			throws IOException {
		need ( "$strcpy" );
		return getWhiteSpace ( "call", ":$StrCpy", line );
	}

	private String getStrCpyRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StrCpy           # [s,t]
			dup                # [s,s,t]
			retrieve           # ['a',s,t]
			dup                # ['a','a',s,t]
			jumpz :$StrCpyEnd  # ['a',s,t]
			copyn 2            # [t,'a',s,t]
			swap               # ['a',t,s,t]
			store              # [s,t]
			push 1             # [1,s,t]
			add                # [s+1,t]
			copyn 1            # [t,s+1,t]
			push 1             # [1,t,s+1,t]
			add                # [t+1,s+1,t]
			swap               # [s+1,t+1,t]
			# recursion?!?
			$strcpy            # [t+1,t]
			drop               # [t]
			return

			:$StrCpyEnd        # [0,s,t]
			drop               # [s,t]
			drop               # [t]
			return
			"""
		);
	}

	private String makeStrNCpy ( final String param, final int line )
			throws IOException {
		need ( "$strncpy" );

		return getWhiteSpace ( "call", ":$StrNCpy", line );
	}

	private String getStrNCpyRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StrNCpy            # [2,s,d]                   [5,s,d]

			# Check for copying nothing
			dup                  # [2,2,s,d]                 [5,5,s,d]
			jumpz :$StrNCpyN     # [2,s,d]                   [5,s,d]

			# find the last destination address to be written because we'll work backwards
			copyn 2              # [d,2,s,d]                 [d,5,s,d]
			copyn 1              # [2,d,2,s,d]               [5,d,5,s,d]
			add                  # [d+2,2,s,d]               [d+5,5,s,d]
			push 1               # [1,d+2,2,s,d]             [1,d+5,5,s,d]
			sub                  # [d+1,2,s,d]               [d+4,5,s,d]

			# is the source shorter than the requested number of characters?
			copyn 2              # [s,d+1,2,s,d]             [s,d+4,5,s,d]
			$strlen              # [3,d+1,2,s,d]             [3,d+4,5,s,d]
			dup                  # [3,3,d+1,2,s,d]           [3,3,d+4,5,s,d]
			copyn 3              # [2,3,3,d+1,2,s,d]         [5,3,3,d+4,5,s,d]
			sub                  # [1,3,d+1,2,s,d]           [-2,3,d+4,5,s,d]
			jumpn :$StrNCpy0     # [3,d+1,2,s,d]             [3,d+4,5,s,d]
			drop                 # [d+1,2,s,d]
			copyn 1              # [2,d+1,2,s,d]
			swap                 # [d+1,2,2,s,d]
			jump :$StrNCpyC      # [d+1,2,2,s,d]

			# yes, so copy some zeros
			:$StrNCpy0           #                           [3,d+4,5,s,d]
			swap                 #                           [d+4,3,5,s,d]
			copyn 1              #                           [3,d+4,3,5,s,d]
			copyn 3              #                           [5,3,d+4,3,5,s,d]
			swap                 #                           [3,5,d+4,3,5,s,d]
			sub                  #                           [2,d+4,3,5,s,d]
			:$StrNCpy0Loop
			dup                  #                           [2,2,d+4,3,5,s,d]
			jumpz :$StrNCpy0Done #                           [2,d+4,3,5,s,d]
			copyn 1              #                           [d+4,2,d+4,3,5,s,d]
			push 0               #                           [0,d+4,2,d+4,3,5,s,d]
			store                #                           [2,d+4,3,5,s,d]
			push 1               #                           [1,2,d+4,3,5,s,d]
			sub                  #                           [1,d+4,3,5,s,d]
			swap                 #                           [d+4,1,3,5,s,d]
			push 1               #                           [1,d+4,1,3,5,s,d]
			sub                  #                           [d+3,1,3,5,s,d]
			swap                 #                           [1,d+3,3,5,s,d]
			jump :$StrNCpy0Loop  #                           [1,d+3,3,5,s,d]

			:$StrNCpy0Done       #                           [0,d+2,3,5,s,d]
			# can we give up here?
			copyn 4              #                           [s,0,d+2,3,5,s,d]
			$strlen              #                           [n,0,d+2,3,5,s,d]
			jumpz :$StrNCpyCDone #                           [0,d+2,3,5,s,d]
			# no, things to copy
			drop                 #                           [d+2,3,5,s,d]

			:$StrNCpyC           # [d+1,2,2,s,d]             [d+2,3,5,s,d]
			copyn 1              # [2,d+1,2,2,s,d]           [3,d+2,3,5,s,d]
			push 1               # [1,2,d+1,2,2,s,d]         [1,3,d+2,3,5,s,d]
			sub                  # [1,d+1,2,2,s,d]           [2,d+2,3,5,s,d]
			:$StrNCpyCLoop       # [1,d+1,2,2,s,d]           [2,d+2,3,5,s,d]
			dup                  # [1,1,d+1,2,2,s,d]         [2,2,d+2,3,5,s,d]
			copyn 5              # [s,1,1,d+1,2,2,s,d]       [s,2,2,d+2,3,5,s,d]
			add                  # [s+1,1,d+1,2,2,s,d]       [s+2,2,d+2,3,5,s,d]
			retrieve             # ['y',1,d+1,2,2,s,d]       ['z',2,d+2,3,5,s,d]
			copyn 2              # [d+1,'y',1,d+1,2,2,s,d]   [d+2,'z',2,d+2,3,5,s,d]
			swap                 # ['y',d+1,1,d+1,2,2,s,d]   ['z',d+2,2,d+2,3,5,s,d]
			store                # [1,d+1,2,2,s,d]           [2,d+2,3,5,s,d]
			dup                  # [1,1,d+1,2,2,s,d]         [2,2,d+2,3,5,s,d]
			jumpz :$StrNCpyCDone # [1,d+1,2,2,s,d]           [2,d+2,3,5,s,d]
			push 1               # [1,1,d+1,2,2,s,d]         [1,2,d+2,3,5,s,d]
			sub                  # [0,d+1,2,2,s,d]           [1,d+2,3,5,s,d]
			swap                 # [d+1,0,2,2,s,d]           [d+2,1,3,5,s,d]
			push 1               # [1,d+1,0,2,2,s,d]         [1,d+2,1,3,5,s,d]
			sub                  # [d,0,2,2,s,d]             [d+1,1,3,5,s,d]
			swap                 # [0,d,2,2,s,d]             [1,d+1,3,5,s,d]
			jump :$StrNCpyCLoop  # [0,d,2,2,s,d]             [1,d+1,3,5,s,d]

			:$StrNCpyCDone       # [0,d,2,2,s,d]             [0,d,3,5,s,d]
			drop                 # [d,2,2,s,d]               [d,3,5,s,d]
			drop                 # [2,2,s,d]                 [3,5,s,d]
			drop                 # [2,s,d]                   [5,s,d]
			drop                 # [s,d]                     [s,d]
			drop                 # [d]                       [d]
			return               # [d]                       [d]

			:$StrNCpyN           # [2,s,d]                   [5,s,d]
			drop                 # [s,d]                     [s,d]
			drop                 # [d]                       [d]
			return               # [d]                       [d]
			"""
		);
	}

	private String makeStrCat ( final String param, final int line )
			throws IOException {
		need ( "$strcat" );

		return getWhiteSpace ( "call", ":$StrCat", line );
	}

	private String getStrCatRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StrCat  # [b,a]
			swap      # [a,b]
			dup       # [a,a,b]
			$strlen   # [La,a,b]
			dup       # [La,La,a,b]
			copyn 3   # [b,La,La,a,b]
			$strlen   # [Lb,La,La,a,b]
			add       # [Lb+La,La,a,b]
			push 1    # [1,Lb+La,La,a,b]
			add       # [Lb+La+1,La,a,b]
			$malloc   # [c,La,a,b]
			dup       # [c,c,La,a,b]
			copyn 3   # [a,c,c,La,a,b]
			$strcpy   # [c,c,La,a,b]
			copyn 2   # [La,c,c,La,a,b]
			add       # [c+La,c,La,a,b]
			copyn 4   # [b,c+La,c,La,a,b]
			$strcpy   # [c+La,c,La,a,b]
			drop      # [c,La,a,b]
			slide 3   # [c]
			return
			"""
		);
	}

	private String makeStrTok ( final String param, final int line )
			throws IOException {
		need ( "$strtok" );

		return getWhiteSpace ( "call", ":$StrTok", line );
	}

	private String getStrTokRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StrTok               # ['.',a]
			# figure out how many parts
			copyn 1                # [a,'.',a]
			copyn 1                # ['.',a,'.',a]
			call :$StrTokCount     # [3,'.',a]

			# allocate space for array of addresses (+1)
			push 1                 # [1,3,'.',a]
			add                    # [4,'.',a]
			$malloc                # [B,'.',a]
			dup                    # [b,b,'.',a]

			# allocate space for pointer into source
			push 1                 # [1,b,b,'.',a]
			$malloc                # [p,b,b,'.',a]

			# set pointer to source address
			dup                    # [p,p,b,b,'.',a]
			copyn 5                # [a,p,p,b,b,'.',a]
			store                  # [p,b,b,'.',a]

			:$StrTokLoop
			# allocate space of a part
			dup                    # [p,p,b,b,'.',a]
			dup                    # [p,p,p,b,b,'.',a]
			retrieve               # [a+n,p,p,b,b,'.',a]
			copyn 5                # ['.',a+n,p,p,b,b,'.',a]
			$strlens               # [3,p,p,b,b,'.',a]
			dup                    # [3,3,p,p,b,b,'.',a]
			push 1                 # [1,3,3,p,p,b,b,'.',a]
			add                    # [4,3,p,p,b,b,'.',a]
			$malloc                # [c1,3,p,p,b,b,'.',a]

			# copy that part there
			copyn 2                # [p,c1,3,p,p,b,b,'.',a]
			retrieve               # [a+n,c1,3,p,p,b,b,'.',a]
			copyn 2                # [3,a+n,c1,3,p,p,b,b,'.',a]
			$strncpy               # [c1,3,p,p,b,b,'.',a]

			# put that address in the result array
			copyn 4                # [b,c1,3,p,p,b,b,'.',a]
			swap                   # [c1,b,3,p,p,b,b,'.',a]
			store                  # [3,p,p,b,b,'.',a]

			# increment the pointer
			$incrn                 # [p,b,b,'.',a]

			# have we reached the end of the input?
			dup                    # [p,p,b,b,'.',a]
			retrieve               # [a+n,p,b,b,'.',a]
			retrieve               # ['x',p,b,b,'.',a]
			jumpz :$StrTokEnd      # [p,b,b,'.',a]
			dup                    # [p,p,b,b,'.',a]
			$incr                  # [p,b,b,'.',a]

			# increment the pointer into the result
			swap                   # [b,p,b,'.',a]
			push 1                 # [1,b,p,b,'.',a]
			add                    # [b+1,p,b,'.',a]
			swap                   # [p,b+1,b,'.',a]
			jump :$StrTokLoop

			:$StrTokEnd            # [p,b,b,'.',a]
			# tidy up
			$free                  # [b,b,'.',a]

			# return the address of the array
			drop                   # [b,'.',a]
			slide 2                # [b]
			return

			#----------
			:$StrTokCount          # ['.',a]

			# allocate a counter (default zero is a good value)
			push 1                 # [1,'.',a]
			$malloc                # [c,'.',a]

			# allocate a pointer
			push 1                 # [1,c,'.',a]
			$malloc                # [p,c,'.',a]

			# set pointer to string address
			dup                    # [p,p,c,'.',a]
			copyn 4                # [a,p,p,c,'.',a]
			store                  # [p,c,'.',a]

			:$StrTokCountLoop
			# get a character
			dup                    # [p,p,c,'.',a]
			retrieve               # [a+n,p,c,'.',a]
			retrieve               # ['x',p,c,'.',a]

			# end of input?
			dup                    # ['x','x',p,c,'.',a]
			jumpz :$StrTokCountEnd # ['x',p,c,'.',a]

			# separator?
			copyn 3                # ['.','x',p,c,'.',a]
			sub                    # [n,p,c,'.',a]
			jumpz :$StrTokCount+1  # [p,c,'.',a]
			:$StrTokCounted        # [p,c,'.',a]
			dup                    # [p,p,c,'.',a]
			$incr                  # [p,c,'.',a]
			jump :$StrTokCountLoop

			:$StrTokCountEnd       # [0,p,c,'.',a]
			drop                   # [p,c,'.',a]
			$free                  # [c,'.',a]
			dup                    # [c,c,'.',a]
			retrieve               # [98,c,'.',a]
			copyn 1                # [c,98,c,'.',a]
			$free                  # [98,c,'.',a]
			slide 3                # [98]
			push 1                 # [1,98]
			add                    # [99]
			return

			:$StrTokCount+1        # [p,c,'.',a]
			copyn 1                # [c,p,c,'.',a]
			$incr                  # [p,c,'.',a]
			jump :$StrTokCounted
			"""
		);
	}

	private String makeStartsWith ( final String param, final int line )
			throws IOException {
		need ( "$startswith" );

		return getWhiteSpace ( "call", ":$StartsWith", line );
	}

	private String getStartsWithRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StartsWith            # [pPrefix,pStr]
			push -1                 # [i,pPrefix,pStr]
			:$StartsWith:loop       # [i,pPrefix,pStr]
			# did we get to the end of the prefix?
			push 1                  # [1,i,pPrefix,pStr]
			add                     # [i,pPrefix,pStr]
			copyn 1                 # [pPrefix,i,pPrefix,pStr]
			copyn 1                 # [i,pPrefix,i,pPrefix,pStr]
			add                     # [pPrefix+i,i,pPrefix,pStr]
			retrieve                # [prefixChar,i,pPrefix,pStr]
			dup                     # [prefixChar,prefixChar,i,pPrefix,pStr]
			jumpz :$StartsWith:ok   # [prefixChar,i,pPrefix,pStr]
			# did we get to the end of the string?
			copyn 3                 # [pStr,prefixChar,i,pPrefix,pStr]
			copyn 2                 # [i,pStr,prefixChar,i,pPrefix,pStr]
			add                     # [pStr+i,prefixChar,i,pPrefix,pStr]
			retrieve                # [strChar,prefixChar,i,pPrefix,pStr]
			dup                     # [strChar,strChar,prefixChar,i,pPrefix,pStr]
			jumpz :$StartsWith:nok  # [strChar,prefixChar,i,pPrefix,pStr]
			# are the chars the same?
			sub                     # [diff,i,pPrefix,pStr]
			jumpz :$StartsWith:loop # [i,pPrefix,pStr]
			# didn't match
			push 0                  # [0,i,pPrefix,pStr]
			slide 3                 # [0]
			return                  # [0]

			:$StartsWith:ok         # [prefixChar,i,pPrefix,pStr]
			push 1                  # [1,prefixChar,i,pPrefix,pStr]
			slide 4                 # [1]
			return                  # [1]

			:$StartsWith:nok        # [strChar,prefixChar,i,pPrefix,pStr]
			push 0                  # [0,strChar,prefixChar,i,pPrefix,pStr]
			slide 5                 # [0]
			return                  # [0]
			"""
		);
	}

	private String makeStrHash ( final String param, final int line )
			throws IOException {
		need ( "$strhash" );

		return getWhiteSpace ( "call", ":$StrHash", line );
	}

	private String getStrHash ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StrHash           # [pStr]
			@HashMagic 31
			push 0              # [hash,pStr]

			:$StrHash:loop      # [hash,pStr]
			# get char
			copyn 1             # [pStr,hash,pStr]
			retrieve            # [char,hash,pStr]
			# reached end?
			dup                 # [char,char,hash,pStr]
			jumpz :$StrHash:end # [char,hash,pStr]

			# calculate new hash value
			swap                # [hash,char,pStr]
			push @HashMagic     # [@HashMagic,hash,char,pStr]
			mult                # [hash,char,pStr]
			add                 # [hash,pStr]

			# next char
			swap                # [pStr,hash]
			push 1              # [1,pStr,hash]
			add                 # [pStr,hash]
			swap                # [hash,pStr]
			jump :$StrHash:loop # [hash,pStr]

			:$StrHash:end       # [0,hash,pStr]
			drop                # [hash,pStr]
			slide 1             # [hash]

			# simulate 32 bit roll over
			$32bcrop            # [hash,pStr]
			return              # [hash]
			"""
		);
	}

	private String makeStr2Fl ( final String param, final int line )
			throws IOException {
		need ( "$str2fl" );

		return getWhiteSpace ( "call", ":$Str2Fl", line );
	}

	private String getStr2FlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Str2Fl         # [s]
			# make space for the float
			push 2           # [2,s]
			$malloc          # [f,s]
			# tokenise the input on the decimal point
			swap             # [s,f]
			push '.'         # ['.',s,f]
			$strtok          # [b,f]
			# check for two parts
			dup              # [b,b,f]
			$strlen          # [2,b,f]
			push 2           # [2,2,b,f]
			sub              # [0,b,f]
			jumpz :$Str2Fl_2 # [b,f]
			# check for one part (integer to float is ok)
			dup              # [b,b,f]
			$strlen          # [1,b,f]
			push 1           # [1,1,b,f]
			sub              # [0,b,f]
			jumpz :$Str2Fl_1 # [b,f]
			# too many decimal points?
			$freearr         # [f]
			$free            # []
			push 1           # [1]
			return

			:$Str2Fl_1       # [b,f]
			# treat the one element as if it were 2 concatenated
			dup              # [b,b,f]
			retrieve         # [b1,b,f]
			$strlen          # [3,b,f]
			push 1           # [1,3,b,f]
			add              # [4,b,f]
			$malloc          # [c,b,f]
			copyn 1          # [b,c,b,f]
			retrieve         # [b1,c,b,f]
			$strcpy          # [c,b,f]
			jump :$Str2Fl_3  # [c,b,f]

			:$Str2Fl_2       # [b,f]
			# concatenate the parts (no decimal)
			dup              # [b,b,f]
			retrieve         # [b1,b,f]
			copyn 1          # [b,b1,b,f]
			push 1           # [1,b,b1,b,f]
			add              # [b+1,b1,b,f]
			retrieve         # [b2,b1,b,f]
			$strcat          # [c,b,f]
			jump :$Str2Fl_3  # [c,b,f]

			:$Str2Fl_3
			# turn it into an int and put in first part of float
			copyn 2          # [f,c,b,f]
			copyn 1          # [c,f,c,b,f]
			$str2int         # [(0,314),f,c,b,f]
			jumpz :$Str2Fl_4 # [314,f,c,b,f]
			# wrong number format
			drop             # [c,b,f]
			$free            # [b,f]
			$freearr         # [f]
			$free            # []
			push 2           # [2]
			return

			:$Str2Fl_4       # [314,f,c,b,f]
			store            # [c,b,f]
			$free            # [b,f]
			# Get the scale value and put in 2nd part of float
			dup              # [b,b,f]
			$strlen          # [2,b,f]
			push 2           # [2,2,b,f]
			sub              # [0,b,f]
			jumpz :$Str2Fl_5 # [b,f]
			dup              # [b,b,f]
			$strlen          # [1,b,f]
			push 1           # [1,1,b,f]
			sub              # [0,b,f]
			jumpz :$Str2Fl_6 # [b,f]
			# can't decide scale
			$freearr         # [f]
			$free            # []
			push 3           # [3]
			return

			:$Str2Fl_6       # [b,f]
			copyn 1          # [f,b,f]
			push 1           # [1,f,b,f]
			add              # [f+1,b,f]
			push 0           # [0,f+1,b,f]
			store            # [b,f]
			jump :$Str2FlTidy

			:$Str2Fl_5       # [b,f]
			dup              # [b,b,f]
			push 1           # [1,b,b,f]
			add              # [b+1,b,f]
			retrieve         # [b2,b,f]
			$strlen          # [2,b,f]
			push -1          # [-1,2,b,f]
			mult             # [-2,b,f]
			copyn 2          # [f,-2,b,f]
			push 1           # [1,f,-2,b,f]
			add              # [f+1,-2,b,f]
			swap             # [-2,f+1,b,f]
			store            # [b,f]
			jump :$Str2FlTidy

			:$Str2FlTidy     # [b,f]
			# tidy up
			$freearr         # [f]
			push 0           # [0,f]
			return
			"""
		);
	}

	private String makeFl2Str ( final String param, final int line )
			throws IOException {
		need ( "$fl2str" );

		return getWhiteSpace ( "call", ":$Fl2Str", line );
	}

	private String getFl2StrRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Fl2Str              # [pFl]
			# first, get the unscaled value as a string
			dup                   # [pFl,pFl]
			retrieve              # [flValue,pFl]
			$int2str              # [pValStr,pFl]
			# get the scale - decides how to handle value
			swap                  # [pFl,pValStr]
			push 1                # [1,pFl,pValStr]
			add                   # [pScale,pValStr]
			retrieve              # [scale,pValStr]
			dup                   # [scale,scale,pValStr]
			jumpz :$Fl2Str:scaleZ # [scale,pValStr]
			dup                   # [scale,scale,pValStr]
			jumpn :$Fl2Str:scNeg  # [scale,pValStr]

			# scale is positive - make a bigger string
			dup                   # [scale,scale,pValStr]
			copyn 2               # [pValStr,scale,scale,pValStr]
			$strlen               # [sz,scale,scale,pValStr]
			add                   # [scale+sz,scale,pValStr]
			push 1                # [1,scale+sz,scale,pValStr]
			add                   # [scale+sz+1,scale,pValStr]
			$malloc               # [pResult,scale,pValStr]
			# copy value to bigger string
			copyn 2               # [pValStr,pResult,scale,pValStr]
			$strcpy               # [pResult,scale,pValStr]
			# add some zeros on the end
			swap                  # [scale,pResult,pValStr]
			copyn 1               # [pResult,scale,pResult,pValStr]
			$strlen               # [len,scale,pResult,pValStr]
			copyn 2               # [pResult,len,scale,pResult,pValStr]
			add                   # [pZeros,scale,pResult,pValStr]
			swap                  # [scale,pZeros,pResult,pValStr]
			:$Fl2Str:sPLoop       # [scale,pZeros,pResult,pValStr]
			dup                   # [scale,scale,pZeros,pResult,pValStr]
			jumpz :$Fl2Str:sPEnd  # [scale,pZeros,pResult,pValStr]
			copyn 1               # [pZeros,scale,pZeros,pResult,pValStr]
			push '0'              # ['0',pZeros,scale,pZeros,pResult,pValStr]
			store                 # [scale,pZeros,pResult,pValStr]
			swap                  # [pZeros,scale,pResult,pValStr]
			push 1                # [1,pZeros,scale,pResult,pValStr]
			add                   # [pZeros+1,scale,pResult,pValStr]
			swap                  # [scale,pZeros+1,pResult,pValStr]
			push 1                # [1,scale,pZeros+1,pResult,pValStr]
			sub                   # [scale-1,pZeros+1,pResult,pValStr]
			jump :$Fl2Str:sPLoop  # [scale-1,pZeros+1,pResult,pValStr]

			:$Fl2Str:sPEnd        # [scale,pZeros,pResult,pValStr]
			drop                  # [pZeros,pResult,pValStr]
			drop                  # [pResult,pValStr]
			swap                  # [pResult,pResult]
			$free                 # [pResult]
			return                # [pResult]

			:$Fl2Str:scNeg        # [scale,pValStr]
			# scale is negative
			# make a copy of the string with one char extra for decimal point
			copyn 1               # [pValStr,scale,pValStr]
			$strlen               # [sz,scale,pValStr]
			dup                   # [sz,sz,scale,pValStr]
			push 2                # [2,sz,sz,scale,pValStr]
			add                   # [sz+2,sz,scale,pValStr]
			$malloc               # [pResult,sz,scale,pValStr]
			copyn 3               # [pValStr,pResult,sz,scale,pValStr]
			$strcpy               # [pResult,sz,scale,pValStr]
			# shuffle some digits down
			swap                  # [sz,pResult,scale,pValStr]
			copyn 2               # [scale,sz,pResult,scale,pValStr]
			:$Fl2Str:scNegLp      # [scale,sz,pResult,scale,pValStr]
			copyn 2               # [pResult,scale,sz,pResult,scale,pValStr]
			copyn 2               # [sz,pResult,scale,sz,pResult,scale,pValStr]
			add                   # [pResult+sz,scale,sz,pResult,scale,pValStr]
			dup                   # [pResult+sz,pResult+sz,scale,sz,pResult,scale,pValStr]
			push 1                # [1,pResult+sz,pResult+sz,scale,sz,pResult,scale,pValStr]
			sub                   # [pResult+sz-1,pResult+sz,scale,sz,pResult,scale,pValStr]
			retrieve              # [char,pResult+sz,scale,sz,pResult,scale,pValStr]
			store                 # [scale,sz,pResult,scale,pValStr]
			swap                  # [sz,scale,pResult,scale,pValStr]
			push 1                # [1,sz,scale,pResult,scale,pValStr]
			sub                   # [sz-1,scale,pResult,scale,pValStr]
			swap                  # [scale,sz-1,pResult,scale,pValStr]
			push 1                # [1,scale,sz-1,pResult,scale,pValStr]
			add                   # [scale+1,sz-1,pResult,scale,pValStr]
			dup                   # [scale+1,scale+1,sz-1,pResult,scale,pValStr]
			jumpz :$Fl2Str:scNegX # [scale+1,sz-1,pResult,scale,pValStr]
			jump :$Fl2Str:scNegLp # [scale+1,sz-1,pResult,scale,pValStr]

			:$Fl2Str:scNegX       # [0,sz,pResult,scale,pValStr]
			# write dp
			drop                  # [sz,pResult,scale,pValStr]
			copyn 1               # [pResult,sz,pResult,scale,pValStr]
			add                   # [pResult+sz,pResult,scale,pValStr]
			push '.'              # ['.',pResult+sz,pResult,scale,pValStr]
			store                 # [pResult,scale,pValStr]
			#tidy up
			swap                  # [scale,pResult,pValStr]
			drop                  # [pResult,pValStr]
			swap                  # [pValStr,pResult]
			$free                 # [pResult]
			return                # [pResult]

			:$Fl2Str:scaleZ       # [scale,pResult]
			# scale is zero
			drop                  # [pResult]
			return                # [pResult]
			"""
		);
	}

	private String makeReadFl ( final String param, final int line )
			throws IOException {
		need ( "$readfl" );

		return getWhiteSpace ( "call", ":$ReadFl", line );
	}

	private String getReadFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$ReadFl
			$readstr         # [s]
			dup              # [s,s]
			$str2fl          # [0,f,s]
			jumpz :$ReadFlOk # [f,s]
			# no f here!     # [s]
			drop             # []
			push 1           # [1]
			return

			:$ReadFlOk
			swap             # [s,f]
			$free            # [f]
			push 0           # [0,f]
			return
			"""
		);
	}

	private String makeStoreFl ( final String param, final int line )
			throws IOException {
		final var result  = new StringBuilder ();
		final var content = stringParam ( param, line );

		result.append ( getWhiteSpace ( "push", Integer.toString ( content.length () + 1 ), line ) );
		pushStr ( content, line, result );
		result.append ( getWhiteSpace ( "call", ":$StoreFl", line ) );
		need ( "$storefl" );
		need ( "$storestr" );

		return result.toString ();
	}

	private String getStoreFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$StoreFl          # [len]
			call :$StoreStr    # [s]
			dup                # [s,s]
			$str2fl            # [0,f,s]
			jumpz :$StoreFlOk  # [f,s]
			$print "Bad number: "
			$printstr
			push 10
			printc
			end
			:$StoreFlOk        # [f,s]
			swap               # [s,f]
			$free              # [f]
			return             # [f]
			"""
		);
	}

	private String makeAddFl ( final String param, final int line )
			throws IOException {
		need ( "$addfl" );

		return getWhiteSpace ( "call", ":$AddFl", line );
	}

	private String getAddFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$AddFl              # [f2,f1]
			# duplicate one
			push 2               # [2,f2,f1]
			$malloc              # [L1,f2,f1]
			copyn 2              # [f1,L1,f2,f1]
			push 2               # [2,f1,L1,f2,f1]
			$strncpy             # [L1,f2,f1]
			# duplicate the other
			push 2               # [2,L1,f2,f1]
			$malloc              # [L2,L1,f2,f1]
			copyn 2              # [f2,L2,L1,f2,f1]
			push 2               # [2,f2,L2,L1,f2,f1]
			$strncpy             # [L2,L1,f2,f1]
			# get scale 2
			dup                  # [L2,L2,L1,f2,f1]
			push 1               # [1,L2,L2,L1,f2,f1]
			add                  # [L2+1,L2,L1,f2,f1]
			retrieve             # [L2s,L2,L1,f2,f1]
			# get scale 1
			copyn 2              # [L1,L2s,L2,L1,f2,f1]
			push 1               # [1,L1,L2s,L2,L1,f2,f1]
			add                  # [L1+1,L2s,L2,L1,f2,f1]
			retrieve             # [L1s,L2s,L2,L1,f2,f1]
			#subtract and decide what to do next
			sub                  # [scDiff,L2,L1,f2,f1]
			dup                  # [scDiff,scDiff,L2,L1,f2,f1]
			dup                  # [scDiff,scDiff,scDiff,L2,L1,f2,f1]
			jumpn :$AddFlscL1    # [scDiff,scDiff,L2,L1,f2,f1]
			jumpz :$AddFlscMatch # [scDiff,L2,L1,f2,f1]
			jump :$AddFlscL2     # [scDiff,L2,L1,f2,f1]

			:$AddFlscL2          # [scDiff,L2,L1,f2,f1]
			# increase value of L2
			copyn 1              # [L2,scDiff,L2,L1,f2,f1]
			copyn 1              # [scDiff,L2,scDiff,L2,L1,f2,f1]
			$mult10^n            # [scDiff,L2,L1,f2,f1]
			# decrease scale of L2
			copyn 1              # [L2,scDiff,L2,L1,f2,f1]
			push 1               # [1,L2,scDiff,L2,L1,f2,f1]
			add                  # [L2+1,scDiff,L2,L1,f2,f1]
			dup                  # [L2+1,L2+1,scDiff,L2,L1,f2,f1]
			retrieve             # [L2s,L2+1,scDiff,L2,L1,f2,f1]
			copyn 2              # [scDiff,L2s,L2+1,scDiff,L2,L1,f2,f1]
			sub                  # [L2sadj,L2+1,scDiff,L2,L1,f2,f1]
			store                # [scDiff,L2,L1,f2,f1]
			drop                 # [L2,L1,f2,f1]
			jump :$AddFlValues   # [L2,L1,f2,f1]

			:$AddFlscMatch       # [scDiff,L2,L1,f2,f1]
			drop                 # [L2,L1,f2,f1]
			jump :$AddFlValues

			:$AddFlscL1          # [scDiff,scDiff,L2,L1,f2,f1]
			drop                 # [scDiff,L2,L1,f2,f1]
			# increase value of L1
			copyn 2              # [L1,scDiff,L2,L1,f2,f1]
			copyn 1              # [scDiff,L1,scDiff,L2,L1,f2,f1]
			push -1              # [-1,scDiff,L1,scDiff,L2,L1,f2,f1]
			mult                 # [-scDiff,L1,scDiff,L2,L1,f2,f1]  # -scDiff is positive!
			$mult10^n            # [scDiff,L2,L1,f2,f1]
			# decrease scale of L1
			copyn 2              # [L1,scDiff,L2,L1,f2,f1]
			push 1               # [1,L1,scDiff,L2,L1,f2,f1]
			add                  # [L1+1,scDiff,L2,L1,f2,f1]
			dup                  # [L1+1,L1+1,scDiff,L2,L1,f2,f1]
			retrieve             # [L1s,L1+1,scDiff,L2,L1,f2,f1]
			copyn 2              # [scDiff,L1s,L1+1,scDiff,L2,L1,f2,f1]
			add                  # [L1sadj,L1+1,scDiff,L2,L1,f2,f1]
			store                # [scDiff,L2,L1,f2,f1]
			drop                 # [L2,L1,f2,f1]
			jump :$AddFlValues

			:$AddFlValues        # [L2,L1,f2,f1]
			# add the values
			dup                  # [L2,L2,L1,f2,f1]
			retrieve             # [v2,L2,L1,f2,f1]
			copyn 2              # [L1,v2,L2,L1,f2,f1]
			retrieve             # [v1,v2,L2,L1,f2,f1]
			add                  # [v1+v2,L2,L1,f2,f1]
			copyn 2              # [L1,v1+v2,L2,L1,f2,f1]
			swap                 # [v1+v2,L1,L2,L1,f2,f1]
			store                # [L2,L1,f2,f1]
			# tidy up
			$free                # [L1,f2,f1]
			slide 2              # [L1]
			return
			"""
		);
	}

	private String makeSubFl ( final String param, final int line )
			throws IOException {
		need ( "$subfl" );

		return getWhiteSpace ( "call", ":$SubFl", line );
	}

	private String getSubFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$SubFl   # [f2,f1]
			push 2    # [2,f2,f1]
			$malloc   # [T,f2,f1]
			dup       # [T,T,f2,f1]
			copyn 2   # [f2,T,T,f2,f1]
			retrieve  # [f2v,T,T,f2,f1]
			push -1   # [-1,f2v,T,T,f2,f1]
			mult      # [-f2v,T,T,f2,f1]
			store     # [T,f2,f1]
			dup       # [T,T,f2,f1]
			push 1    # [1,T,T,f2,f1]
			add       # [T+1,T,f2,f1]
			copyn 2   # [f2,T+1,T,f2,f1]
			push 1    # [1,f2,T+1,T,f2,f1]
			add       # [f2+1,T+1,T,f2,f1]
			retrieve  # [f2s,T+1,T,f2,f1]
			store     # [T,f2,f1]
			dup       # [T,T,f2,f1]
			copyn 3   # [f1,T,T,f2,f1]
			swap      # [T,f1,T,f2,f1]
			$addfl    # [r,T,f2,f1]
			swap      # [T,r,f2,f1]
			$free     # [r,f2,f1]
			slide 2   # [r]
			return    # [r]
			"""
		);
	}

	private String makeMultFl ( final String param, final int line )
			throws IOException {
		need ( "$multfl" );

		return getWhiteSpace ( "call", ":$MultFl", line );
	}

	private String getMultFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$MultFl          # [f2,f1]
			# make space for result
			push 2           # [2,f2,f1]
			$malloc          # [r,f2,f1]
			# multiply the values
			dup              # [r,r,f2,f1]
			copyn 3          # [f1,r,r,f2,f1]
			retrieve         # [f1v,r,r,f2,f1]
			copyn 3          # [f2,f1v,r,r,f2,f1]
			retrieve         # [f2v,f1v,r,r,f2,f1]
			mult             # [f2v*f1v,r,r,f2,f1]
			store            # [r,f2,f1]
			# add the scales
			dup              # [r,r,f2,f1]
			push 1           # [1,r,r,f2,f1]
			add              # [r+1,r,f2,f1]
			copyn 3          # [f1,r+1,r,f2,f1]
			push 1           # [1,f1,r+1,r,f2,f1]
			add              # [f1+1,r+1,r,f2,f1]
			retrieve         # [f1s,r+1,r,f2,f1]
			copyn 3          # [f2,f1s,r+1,r,f2,f1]
			push 1           # [1,f2,f1s,r+1,r,f2,f1]
			add              # [f2+1,f1s,r+1,r,f2,f1]
			retrieve         # [f2s,f1s,r+1,r,f2,f1]
			add              # [f2s+f1s,r+1,r,f2,f1]
			store            # [r,f2,f1]
			slide 2          # [r]
			return
			"""
		);
	}

	private String makeDivFl ( final String param, final int line )
			throws IOException {
		need ( "$divfl" );

		return getWhiteSpace ( "call", ":$DivFl", line );
	}

	private String getDivFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$DivFl            # [f2,f1]
			# make space for result
			push 2             # [2,f2,f1]
			$malloc            # [r,f2,f1]
			# get mag of f1v
			copyn 2            # [f1,r,f2,f1]
			$mag               # [f1vm,r,f2,f1]
			# get mag of f2v
			copyn 2            # [f2,f1vm,r,f2,f1]
			$mag               # [f2vm,f1vm,r,f2,f1]
			# do we need to adjust f1?
			sub                # [f1vm-f2vm,r,f2,f1]
			push 20            # [20,f1vm-f2vm,r,f2,f1]
			sub                # [f1vm-f2vm-20,r,f2,f1]
			dup                # [f1vm-f2vm-20,f1vm-f2vm-20,r,f2,f1]
			jumpn :$DivFlAdjF1 # [f1vm-f2vm-20,r,f2,f1]
			# no adjustment, just store f1 in result
			drop               # [r,f2,f1]
			copyn 2            # [f1,r,f2,f1]
			push 2             # [2,f1,r,f2,f1]
			$strncpy           # [r,f2,f1]
			jump :$DivFlDiv    # [r,f2,f1]
			:$DivFlAdjF1       # [diff-20,r,f2,f1]
			dup                # [diff-20,diff-20,r,f2,f1]
			# adjust f1 value & store in result
			push -1            # [-1,diff-20,diff-20,r,f2,f1]
			mult               # [-(diff-20),diff-20,r,f2,f1]
			copyn 2            # [r,-(diff-20),diff-20,r,f2,f1]
			copyn 5            # [f1,r,-(diff-20),diff-20,r,f2,f1]
			retrieve           # [f1v,r,-(diff-20),diff-20,r,f2,f1]
			store              # [-(diff-20),diff-20,r,f2,f1]
			copyn 2            # [r,-(diff-20),diff-20,r,f2,f1]
			swap               # [-(diff-20),r,diff-20,r,f2,f1]
			$mult10^n          # [diff-20,r,f2,f1]

			# adjust f1 scale & store in result
			copyn 1            # [r,diff-20,r,f2,f1]
			push 1             # [1,r,diff-20,r,f2,f1]
			add                # [r+1,diff-20,r,f2,f1]
			copyn 4            # [f1,r+1,diff-20,r,f2,f1]
			push 1             # [1,f1,r+1,diff-20,r,f2,f1]
			add                # [f1+1,r+1,diff-20,r,f2,f1]
			retrieve           # [f1s,r+1,diff-20,r,f2,f1]
			copyn 2            # [diff-20,f1s,r+1,diff-20,r,f2,f1]
			add                # [f1s+diff-20,r+1,diff-20,r,f2,f1]
			store              # [diff-20,r,f2,f1]
			drop               # [r,f2,f1]
			:$DivFlDiv         # [r,f2,f1]
			# divide value
			dup                # [r,r,f2,f1]
			dup                # [r,r,r,f2,f1]
			retrieve           # [rv,r,r,f2,f1]
			copyn 3            # [f2,rv,r,r,f2,f1]
			retrieve           # [f2v,rv,r,r,f2,f1]
			$truncdiv          # [rv/f2v,r,r,f2,f1]
			store              # [r,f2,f1]
			# subtract the scale
			dup                # [r,r,f2,f1]
			push 1             # [1,r,r,f2,f1]
			add                # [r+1,r,f2,f1]
			dup                # [r+1,r+1,r,f2,f1]
			retrieve           # [rs,r+1,r,f2,f1]
			copyn 3            # [f2,rs,r+1,r,f2,f1]
			push 1             # [1,f2,rs,r+1,r,f2,f1]
			add                # [f2+1,rs,r+1,r,f2,f1]
			retrieve           # [f2s,rs,r+1,r,f2,f1]
			sub                # [rs-f2s,r+1,r,f2,f1]
			store              # [r,f2,f1]
			slide 2            # [r]
			return
			"""
		);
	}

	private String makeModFl ( final String param, final int line )
			throws IOException {
		need ( "$modfl" );

		return getWhiteSpace ( "call", ":$ModFl", line );
	}

	private String getModFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$ModFl   # [denominator,numerator]

			# remainder = n - ( d * floorfl(divfl( n, d )) )
			# rpn:        n d n d divfl floorfl * -
			# Oops, I've been writing stacks the other way round 8~)

			copyn 1   # [n,d,n]
			copyn 1   # [d,n,d,n]
			$divfl    # [q,d,n]
			dup       # [q,q,d,n]
			$floorfl  # [qi,q,d,n]
			swap      # [q,qi,d,n]
			$free     # [qi,d,n]
			dup       # [qi,qi,d,n]
			copyn 2   # [d,qi,qi,d,n]
			$multfl   # [d*qi,qi,d,n]
			swap      # [qi,d*qi,d,n]
			$free     # [d*qi,d,n]
			dup       # [d*qi,d*qi,d,n]
			copyn 3   # [n,d*qi,d*qi,d,n]
			swap      # [d*qi,n,d*qi,d,n]
			$subfl    # [n-d*qi,d*qi,d,n]
			swap      # [d*qi,n-d*qi,d,n]
			$free     # [n-d*qi,d,n]
			slide 2   # [n-d*qi]
			return    # [n-d*qi]
			"""
		);
	}

	private String makeTruncModFl ( final String param, final int line )
			throws IOException {
		need ( "$truncmodfl" );

		return getWhiteSpace ( "call", ":$TruncModFl", line );
	}

	private String getTruncModFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$TruncModFl  # [denominator,numerator]

			# remainder = n - ( d * truncfl(divfl( n, d )) )
			# rpn:        n d n d divfl truncfl * -
			# Oops, I've been writing stacks the other way round 8~)

			copyn 1       # [n,d,n]
			copyn 1       # [d,n,d,n]
			$divfl        # [q,d,n]
			dup           # [q,q,d,n]
			$truncfl      # [qi,q,d,n]
			swap          # [q,qi,d,n]
			$free         # [qi,d,n]
			dup           # [qi,qi,d,n]
			copyn 2       # [d,qi,qi,d,n]
			$multfl       # [d*qi,qi,d,n]
			swap          # [qi,d*qi,d,n]
			$free         # [d*qi,d,n]
			dup           # [d*qi,d*qi,d,n]
			copyn 3       # [n,d*qi,d*qi,d,n]
			swap          # [d*qi,n,d*qi,d,n]
			$subfl        # [n-d*qi,d*qi,d,n]
			swap          # [d*qi,n-d*qi,d,n]
			$free         # [n-d*qi,d,n]
			slide 2       # [n-d*qi]
			return        # [n-d*qi]
			"""
		);
	}

	private String makeFloorFl ( final String param, final int line )
			throws IOException {
		need ( "$floorfl" );

		return getWhiteSpace ( "call", ":$FloorFl", line );
	}

	private String getFloorFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$FloorFl          # [f]
			# copy
			push 2             # [2,f]
			$malloc            # [c,f]
			copyn 1            # [f,c,f]
			push 2             # [2,f,c,f]
			$strncpy           # [c,f]
			# trunc
			dup                # [c,c,f]
			dup                # [c,c,c,f]
			push 1             # [1,c,c,c,f]
			add                # [c+1,c,c,f]
			retrieve           # [cs,c,c,f]
			$mult10^n          # [c,f]
			dup                # [c,c,f]
			push 1             # [1,c,c,f]
			add                # [c+1,c,f]
			push 0             # [0,c+1,c,f]
			store              # [c,f]
			# orig - copy
			copyn 1            # [f,c,f]
			copyn 1            # [c,f,c,f]
			$subfl             # [d,c,f]
			dup                # [d,d,c,f]
			retrieve           # [dv,d,c,f]
			jumpn :$FloorFlAdj # [d,c,f]
			$free              # [c,f]
			slide 1            # [c]
			return             # [c]

			# if -ve, subtract 1 from copy
			:$FloorFlAdj       # [d,c,f]
			$free              # [c,f]
			dup                # [c,c,f]
			$decr              # [c,f]
			slide 1            # [c]
			return             # [c]
			"""
		);
	}

	private String makeTruncFl ( final String param, final int line )
			throws IOException {
		need ( "$truncfl" );

		return getWhiteSpace ( "call", ":$TruncFl", line );
	}

	private String getTruncFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$TruncFl  # [f]
			# copy
			push 2     # [2,f]
			$malloc    # [c,f]
			copyn 1    # [f,c,f]
			push 2     # [2,f,c,f]
			$strncpy   # [c,f]
			# trunc
			dup        # [c,c,f]
			dup        # [c,c,c,f]
			push 1     # [1,c,c,c,f]
			add        # [c+1,c,c,f]
			retrieve   # [cs,c,c,f]
			$mult10^n  # [c,f]
			dup        # [c,c,f]
			push 1     # [1,c,c,f]
			add        # [c+1,c,f]
			push 0     # [0,c+1,c,f]
			store      # [c,f]
			slide 1    # [c]
			return     # [c]
			"""
		);
	}

	private String makeSqrtFl ( final String param, final int line )
			throws IOException {
		need ( "$sqrtfl" );

		return getWhiteSpace ( "call", ":$SqrtFl", line );
	}

	private String getSqrtFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$SqrtFl
			dup                  # [f,f]
			retrieve             # [fv,f]
			jumpn :$SqrtFlNeg    # [f]

			## Finding a starting value
			# heap storage for magnitude
			push 1               # [1,f]
			$malloc              # [ma,f]
			dup                  # [ma,ma,f]
			# get float value
			copyn 2              # [f,ma,ma,f]
			retrieve             # [fv,ma,ma,f]
			store                # [ma,f]
			# get float scale
			dup                  # [ma,ma,f]
			copyn 2              # [f,ma,ma,f]
			push 1               # [1,f,ma,ma,f]
			add                  # [f+1,ma,ma,f]
			retrieve             # [fs,ma,ma,f]
			# adjust value by scale
			$mult10^n            # [ma,f]
			# how big is it?
			dup                  # [ma,ma,f]
			$mag                 # [m,ma,f]
			# save magnitude for later
			push 1               # [1,m,ma,f]
			$malloc              # [ta,m,ma,f]
			dup                  # [ta,ta,m,ma,f]
			copyn 2              # [m,ta,ta,m,ma,f]
			store                # [ta,m,ma,f]
			swap                 # [m,ta,ma,f]
			# add 1, divide by 2, subtract 1 to get scale of starting value
			push 1               # [1,m,ta,ma,f]
			add                  # [m-1,ta,ma,f]
			push 2               # [2,m-1,ta,ma,f]
			div                  # [(m-1)/2,ta,ma,f]
			push 1               # [1,(m-1)/2,ta,ma,f]
			sub                  # [(m-1)/2+1,ta,ma,f]
			# raise 10 to that power
			copyn 2              # [ma,(m-1)/2+1,ta,ma,f]
			push 1               # [1,ma,(m-1)/2+1,ta,ma,f]
			store                # [(m-1)/2+1,ta,ma,f]
			copyn 2              # [ma,(m-1)/2+1,ta,ma,f]
			swap                 # [(m-1)/2+1,ma,ta,ma,f]
			$mult10^n            # [ta,ma,f]

			## now find biggest square less than first 1 or 2 digits
			## by first finding those first digits
			push 1               # [1,ta,ma,f]
			$malloc              # [da,ta,ma,f]
			dup                  # [da,da,ta,ma,f]
			copyn 4              # [f,da,da,ta,ma,f]
			retrieve             # [fv,da,da,ta,ma,f]
			store                # [da,ta,ma,f]
			dup                  # [da,da,ta,ma,f]
			copyn 4              # [f,da,da,ta,ma,f]
			push 1               # [1,f,da,da,ta,ma,f]
			add                  # [f+1,da,da,ta,ma,f]
			retrieve             # [fs,da,da,ta,ma,f]
			$mult10^n            # [da,ta,ma,f]
			dup                  # [da,da,ta,ma,f]
			dup                  # [da,da,da,ta,ma,f]
			$mag                 # [n,da,da,ta,ma,f]
			dup                  # [n,n,da,da,ta,ma,f]
			push 2               # [2,n,n,da,da,ta,ma,f]
			mod                  # [r,n,da,da,ta,ma,f]
			jumpz :$SqrtFlEven   # [n,da,da,ta,ma,f]
			# number of digits is odd, so need the first one
			push 1               # [1,n,da,da,ta,ma,f]
			jump :$SqrtFlScaleN

			:$SqrtFlEven
			# number of digits is even, so need the first two
			push 2               # [2,n,da,da,ta,ma,f]
			:$SqrtFlScaleN
			sub                  # [n-x,da,da,ta,ma,f]
			push -1              # [-1,n-x,da,da,ta,ma,f]
			mult                 # [-(n-x),da,da,ta,ma,f]
			$mult10^n            # [da,ta,ma,f]

			## found first digits - what's the biggest integer whose square that isn't bigger?
			dup                  # [da,da,ta,ma,f]
			retrieve             # [d,da,ta,ma,f]
			push 9               # [9,d,da,ta,ma,f]
			:$SqrtFlChkLoop
			dup                  # [9,9,d,da,ta,ma,f]
			copyn 2              # [d,9,9,d,da,ta,ma,f]
			swap                 # [9,d,9,d,da,ta,ma,f]
			call :$SqrtFlChk     # [r,9,d,da,ta,ma,f]
			dup                  # [r,r,9,d,da,ta,ma,f]
			jumpz :$SqrtFlNxSq   # [r,9,d,da,ta,ma,f]
			jump :$SqrtFlGotSq   # [r,d,da,ta,ma,f]

			:$SqrtFlNxSq         # [r,9,d,da,ta,ma,f]
			drop                 # [9,d,da,ta,ma,f]
			push 1               # [1,9,d,da,ta,ma,f]
			sub                  # [8,d,da,ta,ma,f]
			jump :$SqrtFlChkLoop

			:$SqrtFlGotSq        # [r,r,d,da,ta,ma,f]
			slide 2              # [r,da,ta,ma,f]
			# now we can find our starting guess
			copyn 3              # [ma,r,da,ta,ma,f]
			retrieve             # [m,r,da,ta,ma,f]
			mult                 # [m*r,da,ta,ma,f]

			## now apply the formula 6 times
			# first turn the guess into a float
			push 2               # [2,m*r,da,ta,ma,f]
			$malloc              # [sa,m*r,da,ta,ma,f]
			dup                  # [sa,sa,m*r,da,ta,ma,f]
			copyn 2              # [m*r,sa,sa,m*r,da,ta,ma,f]
			store                # [sa,m*r,da,ta,ma,f]
			slide 1              # [sa,da,ta,ma,f]
			# loop unrolling ;-)
			call :$SqrtFlFormula # [sa,da,ta,ma,f]
			call :$SqrtFlFormula # [sa,da,ta,ma,f]
			call :$SqrtFlFormula # [sa,da,ta,ma,f]
			call :$SqrtFlFormula # [sa,da,ta,ma,f]
			call :$SqrtFlFormula # [sa,da,ta,ma,f]
			call :$SqrtFlFormula # [sa,da,ta,ma,f]
			swap                 # [da,sa,ta,ma,f]
			$free                # [sa,ta,ma,f]
			swap                 # [ta,sa,ma,f]
			$free                # [sa,ma,f]
			swap                 # [ma,sa,f]
			$free                # [sa,f]
			slide 1              # [sa]
			push 0               # [0,sa]
			return

			:$SqrtFlNeg          # [f]
			return               # [f]

			:$SqrtFlChk          # [n,d]
			dup                  # [n,n,d]
			dup                  # [n,n,n,d]
			mult                 # [n*n,n,d]
			copyn 2              # [d,n*n,n,d]
			swap                 # [n*n,d,n,d]
			sub                  # [d-n*n,n,d]
			jumpn :$SqrtFlChkX   # [n,d]
			slide 1              # [n]
			return               # [n]

			:$SqrtFlChkX         # [n,d]
			drop                 # [d]
			drop                 # []
			push 0               # [0]
			return               # [0]

			:$SqrtFlFormula      # [sa,da,ta,ma,f]
			# divide original number by guess
			copyn 4              # [f,sa,da,ta,ma,f]
			copyn 1              # [sa,f,sa,da,ta,ma,f]
			$divfl               # [r1,sa,da,ta,ma,f]
			# add the guess
			dup                  # [r1,r1,sa,da,ta,ma,f]
			copyn 2              # [sa,r1,r1,sa,da,ta,ma,f]
			$addfl               # [r2,r1,sa,da,ta,ma,f]
			# divide by 2
			push 2               # [2,r2,r1,sa,da,ta,ma,f]
			$malloc              # [2a,r2,r1,sa,da,ta,ma,f]
			dup                  # [2a,2a,r2,r1,sa,da,ta,ma,f]
			dup                  # [2a,2a,2a,2a,r2,r1,sa,da,ta,ma,f]
			push 2               # [2,2a,2a,2a,r2,r1,sa,da,ta,ma,f]
			store                # [2a,2a,r2,r1,sa,da,ta,ma,f]
			copyn 2              # [r2,2a,2a,r2,r1,sa,da,ta,ma,f]
			swap                 # [2a,r2,2a,r2,r1,sa,da,ta,ma,f]
			$divfl               # [r3,2a,r2,r1,sa,da,ta,ma,f]
			# tidy up
			swap                 # [2a,r3,r2,r1,sa,da,ta,ma,f]
			$free                # [r3,r2,r1,sa,da,ta,ma,f]
			swap                 # [r2,r3,r1,sa,da,ta,ma,f]
			$free                # [r3,r1,sa,da,ta,ma,f]
			swap                 # [r1,r3,sa,da,ta,ma,f]
			$free                # [r3,sa,da,ta,ma,f]
			swap                 # [sa,r3,da,ta,ma,f]
			$free                # [r3,da,ta,ma,f]
			return               # [r3,da,ta,ma,f]
			"""
		);
	}

	private String makeExpFl ( final String param, final int line )
			throws IOException {
		need ( "$expfl" );

		return getWhiteSpace ( "call", ":$ExpFl", line );
	}

	private String getExpFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$ExpFl             # [e,b]
			# get integer part of exp
			push 2              # [2,e,b]
			$malloc             # [i,e,b]
			dup                 # [i,i,e,b]
			copyn 2             # [e,i,i,e,b]
			retrieve            # [ev,i,i,e,b]
			store               # [i,e,b]
			dup                 # [i,i,e,b]
			copyn 2             # [e,i,i,e,b]
			push 1              # [1,e,i,i,e,b]
			add                 # [e+1,i,i,e,b]
			retrieve            # [es,i,i,e,b]
			$mult10^n           # [i,e,b]
			# get fractional part of exp
			copyn 1             # [e,i,e,b]
			copyn 1             # [i,e,i,e,b]
			$subfl              # [f,i,e,b]
			# calculate the two parts
			copyn 3             # [b,f,i,e,b]
			copyn 2             # [i,b,f,i,e,b]
			call :$ExpFlInt     # [b^i,f,i,e,b]
			copyn 4             # [b,b^i,f,i,e,b]
			copyn 2             # [f,b,b^i,f,i,e,b]
			call :$ExpFlFra     # [b^f,b^i,f,i,e,b]
			# multiply
			copyn 1             # [b^i,b^f,b^i,f,i,e,b]
			copyn 1             # [b^f,b^i,b^f,b^i,f,i,e,b]
			$multfl             # [b^(i+f),b^f,b^i,f,i,e,b]
			# tidy up
			swap                # [b^f,b^(i+f),b^i,f,i,e,b]
			$free               # [b^(i+f),b^i,f,i,e,b]
			swap                # [b^i,b^(i+f),f,i,e,b]
			$free               # [b^(i+f),f,i,e,b]
			swap                # [f,b^(i+f),i,e,b]
			$free               # [b^(i+f),i,e,b]
			swap                # [i,b^(i+f),e,b]
			$free               # [b^(i+f),e,b]
			slide 2             # [b^(i+f)]
			return              # [b^(i+f)]

			# ---------------------------------

			:$ExpFlInt          # [pIntExp,pBase]
			## See https://en.wikipedia.org/wiki/Exponentiation_by_squaring
			# if ( exponent == 0 ) return 1;
			dup                 # [pIntExp,pIntExp,pBase]
			retrieve            # [int,pIntExp,pBase]
			jumpz :$ExpFlInt0   # [pIntExp,pBase]

			# runningTotal = base;
			push 2              # [2,pIntExp,pBase]
			$malloc             # [pRunningTotal,pIntExp,pBase]
			copyn 2             # [pBase,pRunningTotal,pIntExp,pBase]
			push 2              # [2,pBase,pRunningTotal,pIntExp,pBase]
			$strncpy            # [pRunningTotal,pIntExp,pBase]

			# countDown = abs ( exponent );
			push 1              # [1,pRunningTotal,pIntExp,pBase]
			$malloc             # [pCountDown,pRunningTotal,pIntExp,pBase]
			dup                 # [pCountDown,pCountDown,pRunningTotal,pIntExp,pBase]
			copyn 3             # [pIntExp,pCountDown,pCountDown,pRunningTotal,pIntExp,pBase]
			retrieve            # [intExp,pCountDown,pCountDown,pRunningTotal,pIntExp,pBase]
			$abs                # [intExp,pCountDown,pCountDown,pRunningTotal,pIntExp,pBase]
			store               # [pCountDown,pRunningTotal,pIntExp,pBase]

			# sideTotal    = 1;
			push 2              # [2,pCountDown,pRunningTotal,pIntExp,pBase]
			$malloc             # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			dup                 # [pSideTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			push 1              # [1,pSideTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			store               # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# while ( countDown > 1 ) {
			:$ExpFlIntLoop      # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			push 1              # [1,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			copyn 2             # [pCountDown,1,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			retrieve            # [countDown,1,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			sub                 # [1-countDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			jumpn :$ExpFlIntL_A # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			jump :$ExpFlIntL_Z  # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			:$ExpFlIntL_A       # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# 	if ( countDown % 2 == 0 ) {
			copyn 1             # [pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			retrieve            # [countDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			push 2              # [2,countDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			mod                 # [countDown%2,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			jumpz :$ExpFlIntL_B # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			jump :$ExpFlIntL_C  # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# 		// countdown is even - halve it
			# 		countDown = countDown / 2;
			:$ExpFlIntL_B       # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			copyn 1             # [pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			dup                 # [pCountDown,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			retrieve            # [countDown,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			push 2              # [2,countDown,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			div                 # [countDown/2,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			store               # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			jump :$ExpFlIntL_D  # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# 	} else {
			:$ExpFlIntL_C       # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# 		// countdown is odd, so adjust side total
			# 		sideTotal = runningTotal * sideTotal;
			dup                 # [pSideTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			copyn 3             # [pRunningTotal,pSideTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			$multfl             # [pTmp,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			swap                # [pSideTotal,pTmp,pCountDown,pRunningTotal,pIntExp,pBase]
			copyn 1             # [pTmp,pSideTotal,pTmp,pCountDown,pRunningTotal,pIntExp,pBase]
			push 2              # [2,pTmp,pSideTotal,pTmp,pCountDown,pRunningTotal,pIntExp,pBase]
			$strncpy            # [pSideTotal,pTmp,pCountDown,pRunningTotal,pIntExp,pBase]
			swap                # [pTmp,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			$free               # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# 		// make countdown even and then halve it
			# 		countDown = (countDown - 1) / 2;
			copyn 1             # [pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			dup                 # [pCountDown,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			retrieve            # [countDown,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			push 1              # [1,countDown,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			sub                 # [countDown-1,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			push 2              # [2,countDown-1,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			div                 # [(countDown-1)/2,pCountDown,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			store               # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# 	}
			:$ExpFlIntL_D       # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# 	// square the running total
			# 	runningTotal = runningTotal * runningTotal;
			copyn 2             # [pRunningTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			dup                 # [pRunningTotal,pRunningTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			dup                 # [pRunningTotal,pRunningTotal,pRunningTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			$multfl             # [pTmp,pRunningTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			swap                # [pRunningTotal,pTmp,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			copyn 1             # [pTmp,pRunningTotal,pTmp,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			push 2              # [2,pTmp,pRunningTotal,pTmp,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			$strncpy            # [pRunningTotal,pTmp,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			drop                # [pTmp,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			$free               # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# }
			jump :$ExpFlIntLoop # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			:$ExpFlIntL_Z       # [pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# // combine the totals
			# result = runningTotal * sideTotal;
			dup                 # [pSideTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			copyn 3             # [pRunningTotal,pSideTotal,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]
			$multfl             # [pResult,pSideTotal,pCountDown,pRunningTotal,pIntExp,pBase]

			# // adjust for negative exponent?
			# return exponent < 0
			# 	? 1.0 / result
			# 	: result;
			swap                # [pSideTotal,pResult,pCountDown,pRunningTotal,pIntExp,pBase]
			$free               # [pResult,pCountDown,pRunningTotal,pIntExp,pBase]
			swap                # [pCountDown,pResult,pRunningTotal,pIntExp,pBase]
			$free               # [pResult,pRunningTotal,pIntExp,pBase]
			swap                # [pRunningTotal,pResult,pIntExp,pBase]
			$free               # [pResult,pIntExp,pBase]
			swap                # [pIntExp,pResult,pBase]
			retrieve            # [intExp,pResult,pBase]
			jumpn :$ExpFlIntNeg # [pResult,pBase]
			slide 1             # [pResult]
			return              # [pResult]

			:$ExpFlIntNeg       # [pResult,pBase]
			dup                 # [pResult,pResult,pBase]
			push 2              # [2,pResult,pResult,pBase]
			$malloc             # [pOne,pResult,pResult,pBase]
			dup                 # [pOne,pOne,pResult,pResult,pBase]
			push 1              # [1,pOne,pOne,pResult,pResult,pBase]
			store               # [pOne,pResult,pResult,pBase]
			swap                # [pResult,pOne,pResult,pBase]
			$divfl              # [pResultRecip,pResult,pBase]
			swap                # [pResult,pResultRecip,pBase]
			$free               # [pResultRecip,pBase]
			slide 1             # [pResultRecip]
			return              # [pResultRecip]


			# n^0 is 1
			:$ExpFlInt0         # [pIntExp,pBase]
			drop                # [pBase]
			drop                # []
			push 2              # [2]
			$malloc             # [pResult]
			dup                 # [pResult,pResult]
			push 1              # [1,pResult,pResult]
			store               # [pResult]
			return              # [pResult]

			# ---------------------------------

			:$ExpFlFra          # [e,b]
			# Very little to do if exponent is zero
			dup                 # [e,e,b]
			retrieve            # [ev,e,b]
			jumpz :$ExpFlFra1   # [e,b]

			# work with positive exponent, and adjust at end
			push 2              # [2,e,b]
			$malloc             # [e+,e,b]
			copyn 1             # [e,e+,e,b]
			push 2              # [2,e,e+,e,b]
			$strncpy            # [e+,e,b]
			dup                 # [e+,e+,e,b]
			dup                 # [e+,e+,e+,e,b]
			retrieve            # [eV,e+,e+,e,b]
			$abs                # [e+V,e+,e+,e,b]
			store               # [e+,e,b]

			# Method from https://lamarotte2.blogspot.com/2011/04/hand-calculation-of-fractional-decimal.html
			# need 2 as a float
			push 2              # [2,e+,e,b]
			$malloc             # [t,e+,e,b]
			dup                 # [t,t,e+,e,b]
			push 2              # [2,t,t,e+,e,b]
			store               # [t,e+,e,b]

			# lowPower  = 0
			push 2              # [2,t,e+,e,b]
			$malloc             # [lp,t,e+,e,b]

			# lowResult = 1
			push 2              # [2,lp,t,e+,e,b]
			$malloc             # [lr,lp,t,e+,e,b]
			dup                 # [lr,lr,lp,t,e+,e,b]
			push 1              # [1,lr,lr,lp,t,e+,e,b]
			store               # [lr,lp,t,e+,e,b]

			# highPower = 1
			push 2              # [2,lr,lp,t,e+,e,b]
			$malloc             # [hp,lr,lp,t,e+,e,b]
			dup                 # [hp,hp,lr,lp,t,e+,e,b]
			push 1              # [1,hp,hp,lr,lp,t,e+,e,b]
			store               # [hp,lr,lp,t,e+,e,b]

			# highResult = base
			push 2              # [2,hp,lr,lp,t,e+,e,b]
			$malloc             # [hr,hp,lr,lp,t,e+,e,b]
			copyn 7             # [b,hr,hp,lr,lp,t,e+,e,b]
			push 2              # [2,b,hr,hp,lr,lp,t,e+,e,b]
			$strncpy            # [hr,hp,lr,lp,t,e+,e,b]

			# couter = 0
			push 1              # [1,hr,hp,lr,lp,t,e+,e,b]
			$malloc             # [c,hr,hp,lr,lp,t,e+,e,b]

			:$ExpFlFraLoop      # [c,hr,hp,lr,lp,t,e+,e,b]
			# power = ( lowPower + highPower ) / 2
			copyn 4             # [lp,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 3             # [hp,lp,c,hr,hp,lr,lp,t,e+,e,b]
			$addfl              # [p2,c,hr,hp,lr,lp,t,e+,e,b]
			dup                 # [p2,p2,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 7             # [t,p2,p2,c,hr,hp,lr,lp,t,e+,e,b]
			$divfl              # [p,p2,c,hr,hp,lr,lp,t,e+,e,b]
			swap                # [p2,p,c,hr,hp,lr,lp,t,e+,e,b]
			$free               # [p,c,hr,hp,lr,lp,t,e+,e,b]

			# result = sqrt( lowResult * highResullt )
			copyn 4             # [lr,p,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 3             # [hr,lr,p,c,hr,hp,lr,lp,t,e+,e,b]
			$multfl             # [r2,p,c,hr,hp,lr,lp,t,e+,e,b]
			dup                 # [r2,r2,p,c,hr,hp,lr,lp,t,e+,e,b]
			$sqrtfl             # [0,r,r2,p,c,hr,hp,lr,lp,t,e+,e,b]
			jumpz :$ExpFlFraSRO # [r,r2,p,c,hr,hp,lr,lp,t,e+,e,b]
			$print "Oops - tried to sqrt a -ve number:(\\n"
			end
			:$ExpFlFraSRO       # [r,r2,p,c,hr,hp,lr,lp,t,e+,e,b]
			swap                # [r2,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			$free               # [r,p,c,hr,hp,lr,lp,t,e+,e,b]

			# if power < exp
			copyn 1             # [p,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 9             # [e+,p,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			$subfl              # [d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			dup                 # [d,d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			retrieve            # [dv,d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			jumpn :$ExpFlFraLo  # [d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			jump :$ExpFlFraHi1  # [d,r,p,c,hr,hp,lr,lp,t,e+,e,b]

			:$ExpFlFraLo        # [d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			#	lowPower = power
			$free               # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 6             # [lp,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 2             # [p,lp,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			push 2              # [2,p,lp,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			$strncpy            # [lp,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			drop                # [r,p,c,hr,hp,lr,lp,t,e+,e,b]

			#	lowResult = result
			copyn 5             # [lr,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 1             # [r,lr,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			push 2              # [2,r,lr,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			$strncpy            # [lr,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			call :$ExpFlFraT    # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			jump :$ExpFlFraCtr  # [r,p,c,hr,hp,lr,lp,t,e+,e,b]

			:$ExpFlFraHi1       # [d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			# else if power > exp
			dup                 # [d,d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			retrieve            # [dv,d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			push -1             # [-1,dv,d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			mult                # [-dv,d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			jumpn :$ExpFlFraHi2 # [d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			jump :$ExpFlFraEls  # [d,r,p,c,hr,hp,lr,lp,t,e+,e,b]

			:$ExpFlFraHi2
			#	highPower = power
			$free               # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 4             # [hp,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 2             # [p,hp,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			push 2              # [2,p,hp,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			$strncpy            # [hp,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			drop                # [r,p,c,hr,hp,lr,lp,t,e+,e,b]

			#	highResult = result
			copyn 3             # [hr,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 1             # [r,hr,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			push 2              # [2,r,hr,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			$strncpy            # [hr,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			call :$ExpFlFraT    # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			jump :$ExpFlFraCtr  # [r,p,c,hr,hp,lr,lp,t,e+,e,b]

			:$ExpFlFraEls       # [d,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			# else
			# (tidy up)
			$free               # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			jump :$ExpFlFraEnd

			:$ExpFlFraCtr       # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			# counter++
			copyn 2             # [c,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			$incr               # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			copyn 2             # [c,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			retrieve            # [cv,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			# if counter < 99
			push 99             # [99,cv,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			sub                 # [cv-99,r,p,c,hr,hp,lr,lp,t,e+,e,b]
			jumpn :$ExpFlFraNxt # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			jump :$ExpFlFraEnd  # [r,p,c,hr,hp,lr,lp,t,e+,e,b]

			:$ExpFlFraNxt       # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			$free               # [p,c,hr,hp,lr,lp,t,e+,e,b]
			$free               # [c,hr,hp,lr,lp,t,e+,e,b]
			#	jump :loop
			jump :$ExpFlFraLoop # [c,hr,hp,lr,lp,t,e+,e,b]

			:$ExpFlFraEnd       # [r,p,c,hr,hp,lr,lp,t,e+,e,b]
			swap                # [p,r,c,hr,hp,lr,lp,t,e+,e,b]
			$free               # [r,c,hr,hp,lr,lp,t,e+,e,b]
			swap                # [c,r,hr,hp,lr,lp,t,e+,e,b]
			$free               # [r,hr,hp,lr,lp,t,e+,e,b]
			swap                # [hr,r,hp,lr,lp,t,e+,e,b]
			$free               # [r,hp,lr,lp,t,e+,e,b]
			swap                # [hp,r,lr,lp,t,e+,e,b]
			$free               # [r,lr,lp,t,e+,e,b]
			swap                # [lr,r,lp,t,e+,e,b]
			$free               # [r,lp,t,e+,e,b]
			swap                # [lp,r,t,e+,e,b]
			$free               # [r,t,e+,e,b]
			swap                # [t,r,e+,e,b]
			$free               # [r,e+,e,b]
			swap                # [e+,r,e,b]
			$free               # [r,e,b]
			# adjust if negative
			copyn 1             # [e,r,e,b]
			retrieve            # [eV,r,e,b]
			jumpn :$ExpFlFraNeg # [r,e,b]
			slide 2             # [r]
			#	return result
			return              # [r]

			:$ExpFlFraNeg       # [r,e,b]
			# reciprocal result
			push 2              # [2,r,e,b]
			$malloc             # [o,r,e,b]
			dup                 # [o,o,r,e,b]
			push 1              # [1,o,o,r,e,b]
			store               # [o,r,e,b]
			dup                 # [o,o,r,e,b]
			copyn 2             # [r,o,o,r,e,b]
			$divfl              # [rr,o,r,e,b]
			swap                # [o,rr,r,e,b]
			$free               # [rr,r,e,b]
			swap                # [r,rr,e,b]
			$free               # [rr,e,b]
			slide 2             # [rr]
			return              # [rr]

			:$ExpFlFra1         # [e,b]
			drop                # [b]
			drop                # []
			push 2              # [2]
			$malloc             # [r]
			dup                 # [r,r]
			push 1              # [1,r,r]
			store               # [r]
			return              # [r]

			:$ExpFlFraT         # [n]
			# is the scale more -ve than -20?
			dup                 # [n,n]
			push 1              # [1,n,n]
			add                 # [n+1,n]
			retrieve            # [ns,n]
			push 20             # [20,ns,n]
			add                 # [ns+20,n]
			jumpn :$ExpFlFraTX  # [n]
			drop                # []
			return

			:$ExpFlFraTX        # [n]
			# how much do we have to adjust scale by?
			dup                 # [n,n]
			push 1              # [1,n,n]
			add                 # [n+1,n]
			dup                 # [n+1,n+1,n]
			retrieve            # [ns,n+1,n]
			dup                 # [ns,ns,n+1,n]
			push -20            # [-20,ns,ns,n+1,n]
			sub                 # [ns+20,ns,n+1,n]

			# now adjust scale
			copyn 2             # [n+1,ns+20,ns,n+1,n]
			push -20            # [-20,n+1,ns+20,ns,n+1,n]
			store               # [ns+20,ns,n+1,n]

			# now adjust value
			copyn 3             # [n,ns+20,ns,n+1,n]
			swap                # [ns+20,n,ns,n+1,n]
			$mult10^n           # [ns,n+1,n]
			drop                # [n+1,n]
			drop                # [n]
			drop                # []
			return
			"""
		);
	}

	private String makePrintFl ( final String param, final int line )
			throws IOException {
		need ( "$printfl" );

		return getWhiteSpace ( "call", ":$PrintFl", line );
	}

	private String getPrintFlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$PrintFl              # [a]
			call :$PrFlParts       # [-2,314,a]
			dup                    # [-2,-2,314,a]
			jumpn :$PrFlGoDown     # [-2,314,a]

			# scale up             # [2,314,a]
			swap                   # [314,2,a]
			printn                 # [2,a]
			push -1                # [-1,2,a]
			mult                   # [-2,a]
			dup                    # [-2,-2,a]
			jumpn :$PrFlExtra0     # [-2,a]
			drop                   # [a]
			:$PrFlExtraBack        # [a]
			$print ".0"            # [a]
			drop                   # []
			return

			:$PrFlExtra0           # [-2,a]
			push 0                 # [0,-2,a]
			printn                 # [-2,a]
			push 1                 # [1,-2,a]
			add                    # [-1,a]
			dup                    # [-1,-1,a]
			jumpz :$PrFlExtraDone  # [-1,a]
			jump :$PrFlExtra0      # [-1,a]

			:$PrFlExtraDone        # [0,a]
			drop                   # [a]
			jump :$PrFlExtraBack   # [a]

			:$PrFlGoDown           # [-2,314,a]
			call :$PrFlDownSz      # [3,a]
			# have to check for "-0"
			dup                    # [3,3,a]
			jumpz :$PrFlGoDownZ    # [3,a]
			jump :$PrFlGoDownPl

			:$PrFlGoDownZ          # [0,a]
			copyn 1                # [a,0,a]
			retrieve               # [n,0,a]
			jumpn :$PrFlGoDownNg   # [0,a]
			jump :$PrFlGoDownPl

			:$PrFlGoDownNg         # [0,a]
			push '-'               # ['-',0,a]
			printc                 # [0,a]
			jump :$PrFlGoDownPl

			:$PrFlGoDownPl
			printn                 # [a]
			push '.'               # ['.',a]
			printc                 # [a]
			call :$PrFlParts       # [-2,314,a]
			call :$PrFlDecs        # [b,a]
			dup                    # [b,b,a]
			$printstr              # [b,a]
			$free                  # [a]
			drop                   # []
			return

			:$PrFlParts
			dup                    # [a,a]
			retrieve               # [314,a]
			copyn 1                # [a,314,a]
			push 1                 # [1,a,314,a]
			add                    # [a+1,314,a]
			retrieve               # [-2,314,a]
			return

			:$PrFlDownSz           # [-2,314,a]
			# scale down by 10
			copyn 1                # [314,-2,314,a]
			push 10                # [10,314,-2,314,a]
			$truncdiv              # [31,-2,314,a]
			copyn 1                # [-2,31,-2,314,a]
			push 1                 # [1,-2,31,-2,314,a]
			add                    # [-1,31,-2,314,a]
			dup                    # [-1,-1,31,-2,314,a]
			jumpz :$PrFlDownStop   # [-1,31,-2,314,a]
			call :$PrFlDownSz      # [3,-2,314,a]
			slide 2                # [3,a]
			return

			:$PrFlDownStop         # [-1,31,-2,314,a]
			drop                   # [31,-2,314,a]
			slide 2                # [31,a]
			return                 # [31,a]

			:$PrFlDecs             # [-2,314,a]
			# allocate enough space for decs as string
			dup                    # [-2,-2,314,a]
			push -1                # [-1,-2,-2,314,a]
			mult                   # [2,-2,314,a]
			push 1                 # [1,2,-2,314,a]
			add                    # [3,-2,314,a]
			$malloc                # [b,-2,314,a]
			# fill string in reverse order
			dup                    # [b,b,-2,314,a]
			copyn 2                # [-2,b,b,-2,314,a]
			sub                    # [b+2,b,-2,314,a]
			copyn 3                # [314,b+2,b,-2,314,a]
			dup                    # [314,314,b+2,b,-2,314,a]
			jumpn :$PrFlDecsFlip   # [314,b+2,b,-2,314,a]
			jump :$PrFlDecsNoFlip  # [314,b+2,b,-2,314,a]

			:$PrFlDecsFlip         # [-314,b+2,b,-2,314,a]
			push -1                # [-1,-314,b+2,b,-2,314,a]
			mult                   # [314,b+2,b,-2,314,a]
			:$PrFlDecsNoFlip       # [314,b+2,b,-2,314,a]
			call :$PrFlWrDecs      # [b,-2,314,a]
			slide 2                # [b,a]
			return

			:$PrFlWrDecs           # [314,b+2,b,-2,314,a]
			# put last digit in right place
			copyn 1                # [b+2,314,b+2,b,-2,314,a]
			push 1                 # [1,b+2,314,b+2,b,-2,314,a]
			sub                    # [b+1,314,b+2,b,-2,314,a]
			copyn 1                # [314,b+1,314,b+2,b,-2,314,a]
			push 10                # [10,314,b+1,314,b+2,b,-2,314,a]
			mod                    # [4,b+1,314,b+2,b,-2,314,a]
			push '0'               # ['0',4,b+1,314,b+2,b,-2,314,a]
			add                    # ['4',b+1,314,b+2,b,-2,314,a]
			store                  # [314,b+2,b,-2,314,a]

			# are we done?
			copyn 2                # [b,314,b+2,b,-2,314,a]
			copyn 2                # [b+2,b,314,b+2,b,-2,314,a]
			sub                    # [-2,314,b+2,b,-2,314,a]
			push 1                 # [1,-2,314,b+2,b,-2,314,a]
			add                    # [-1,314,b+2,b,-2,314,a]
			jumpz :$PrFlWrDecsDone # [314,b+2,b,-2,314,a]

			#set up for next digit
			push 10                # [10,314,b+2,b,-2,314,a]
			$truncdiv              # [31,b+2,b,-2,314,a]
			swap                   # [b+2,31,b,-2,314,a]
			push 1                 # [1,b+2,31,b,-2,314,a]
			sub                    # [b+1,31,b,-2,314,a]
			swap                   # [31,b+1,b,-2,314,a]
			jump :$PrFlWrDecs

			:$PrFlWrDecsDone       # [3,b,b,-2,314,a]
			drop                   # [b,b,-2,314,a]
			drop                   # [b,-2,314,a]
			return
			"""
		);
	}

	private String makeInt2Fl ( final String param, final int line )
			throws IOException {
		need ( "$int2fl" );

		return getWhiteSpace ( "call", ":$Int2Fl", line );
	}

	private String getInt2FlRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Int2Fl  # [i]
			push 2    # [2,i]
			$malloc   # [f,i]
			dup       # [f,f,i]
			copyn 2   # [i,f,f,i]
			store     # [f,i]
			slide 1   # [f]
			return    # [f]
			"""
		);
	}

	private String makeFl2Int ( final String param, final int line )
			throws IOException {
		need ( "$fl2int" );

		return getWhiteSpace ( "call", ":$Fl2Int", line );
	}

	private String getFl2IntRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$Fl2Int                # [f]
			dup                     # [f,f]
			push 1                  # [1,f,f]
			add                     # [f+1,f]
			retrieve                # [fs,f]
			dup                     # [fs,fs,f]
			jumpn :$Fl2IntDownScale # [fs,f]
			# Need to upscale.
			push 1                  # [1,fs,f]
			$malloc                 # [t,fs,f]
			dup                     # [t,t,fs,f]
			copyn 3                 # [f,t,t,fs,f]
			retrieve                # [fv,t,t,fs,f]
			store                   # [t,fs,f]
			dup                     # [t,t,fs,f]
			dup                     # [t,t,t,fs,f]
			copyn 3                 # [fs,t,t,t,fs,f]
			$mult10^n               # [t,t,fs,f]
			retrieve                # [r,t,fs,f]
			swap                    # [t,r,fs,f]
			$free                   # [r,fs,f]
			slide 2                 # [r]
			return                  # [r]

			:$Fl2IntDownScale       # [fs,f]
			push 2                  # [2,fs,f]
			$malloc                 # [t,fs,f]
			copyn 2                 # [f,t,fs,f]
			push 2                  # [2,f,t,fs,f]
			$strncpy                # [t,fs,f]
			swap                    # [fs,t,f]
			:$Fl2IntDSLoop          # [fs,t,f]
			dup                     # [fs,fs,t,f]
			jumpz :$Fl2IntDSDone    # [fs,t,f]
			copyn 1                 # [t,fs,t,f]
			retrieve                # [tv,fs,t,f]
			push 10                 # [10,tv,fs,t,f]
			$truncdiv               # [tv/10,fs,t,f]
			copyn 2                 # [t,tv/10,fs,t,f]
			swap                    # [tv/10,t,fs,t,f]
			store                   # [fs,t,f]
			push 1                  # [1,fs,t,f]
			add                     # [fs+1,t,f]
			jump :$Fl2IntDSLoop     # [fs+1,t,f]

			:$Fl2IntDSDone          # [fs,t,f]
			drop                    # [t,f]
			dup                     # [t,t,f]
			retrieve                # [r,t,f]
			swap                    # [t,r,f]
			$free                   # [r,f]
			slide 1                 # [r]
			return                  # [r]
			"""
		);
	}

	private String makeXArgs ( final String param, final int line )
			throws IOException {
		need ( "$x-args" );

		return getWhiteSpace ( "call", ":$X-Args", line );
	}

	private String getXArgsRoutine ()
			throws IOException {
		return getWhiteSpace (
			"""
			:$X-Args                   # []
			x-args                     # [argc]
			dup                        # [argc,argc]
			# allocate space for args array
			push 1                     # [1,argc,argc]
			add                        # [argc+1,argc]
			$malloc                    # [args,argc]
			copyn 1                    # [argc,args,argc]
			jumpz :$X-ArgsNone         # [args,argc]
			push 0                     # [argi,args,argc]
			:$X-ArgsLoop               # [argi,args,argc]
			push 0                     # [0,argi,args,argc]
			push 1                     # [1,0,argi,args,argc]
			$malloc                    # [arglen,0,argi,args,argc]

			# read an arg to the stack
			:$X-ArgsReadLoop           # [arglen,0,argi,args,argc]
			x-args                     # [argn[0],arglen,0,argi,args,argc]
			dup                        # [argn[0],argn[0],arglen,0,argi,args,argc]
			jumpz :$X-ArgsCArgStore    # [argn[0],arglen,0,argi,args,argc]
			swap                       # [arglen,argn[0],0,argi,args,argc]
			dup                        # [arglen,arglen,argn[0],0,argi,args,argc]
			$incr                      # [arglen,argn[0],0,argi,args,argc]
			jump :$X-ArgsReadLoop      # [arglen,argn[0],0,argi,args,argc]

			:$X-ArgsCArgStore          # [argn[0],arglen,0,argi,args,argc]
			call :$X-ArgsArgStore      # [argi,args,argc]
			dup                        # [argi,argi,args,argc]
			copyn 3                    # [argc,argi,argi,args,argc]
			sub                        # [argc-argi,argi,args,argc]
			jumpz :$X-ArgsDone         # [argi,args,argc]
			jump :$X-ArgsLoop          # [argi,args,argc]

			:$X-ArgsDone               # [argi,args,argc]
			drop                       # [args,argc]
			slide 1                    # [args]
			return                     # [args]

			:$X-ArgsNone               # [args,argc]
			slide 1                    # [args]
			return                     # [args]

			# store an arg to the array
			:$X-ArgsArgStore           # [0,arglen,argn[1],argn[0],0,argi,args,argc]
			drop                       # [arglen,argn[1],argn[0],0,argi,args,argc]
			dup                        # [arglen,arglen,argn[1],argn[0],0,argi,args,argc]
			retrieve                   # [len,arglen,argn[1],argn[0],0,argi,args,argc]
			push 1                     # [1,len,arglen,argn[1],argn[0],0,argi,args,argc]
			add                        # [len+1,arglen,argn[1],argn[0],0,argi,args,argc]
			$malloc                    # [arga,arglen,argn[1],argn[0],0,argi,args,argc]
			copyn 1                    # [arglen,arga,arglen,argn[1],argn[0],0,argi,args,argc]
			retrieve                   # [len,arga,arglen,argn[1],argn[0],0,argi,args,argc]
			add                        # [argEnd,arglen,argn[1],argn[0],0,argi,args,argc]
			swap                       # [arglen,argEnd,argn[1],argn[0],0,argi,args,argc]
			$free                      # [argEnd,argn[1],argn[0],0,argi,args,argc]

			:$X-ArgsArgStrLoop         # [argEnd,argn[1],argn[0],0,argi,args,argc]
			push 1                     # [1,argEnd,argn[1],argn[0],0,argi,args,argc]
			sub                        # [argEnd,argn[1],argn[0],0,argi,args,argc]
			dup                        # [argEnd,argEnd,argn[1],argn[0],0,argi,args,argc]
			copyn 2                    # [argn[1],argEnd,argEnd,argn[1],argn[0],0,argi,args,argc]
			dup                        # [argn[1],argn[1],argEnd,argEnd,argn[1],argn[0],0,argi,args,argc]
			jumpz :$X-ArgsArgStrEnd    # [argn[1],argEnd,argEnd,argn[1],argn[0],0,argi,args,argc]
			store                      # [argEnd,argn[1],argn[0],0,argi,args,argc]
			slide 1                    # [argEnd,argn[0],0,argi,args,argc]
			jump :$X-ArgsArgStrLoop    # [argEnd,argn[0],0,argi,args,argc]

			:$X-ArgsArgStrEnd          # [0,argEnd,argEnd,0,argi,args,argc]
			drop                       # [argEnd,argEnd,0,argi,args,argc]
			drop                       # [argEnd,0,argi,args,argc]
			copyn 3                    # [args,argEnd,0,argi,args,argc]
			copyn 3                    # [argi,args,argEnd,0,argi,args,argc]
			add                        # [args,argEnd,0,argi,args,argc]
			swap                       # [argEnd,args,0,argi,args,argc]
			push 1                     # [1,argEnd,args,0,argi,args,argc]
			add                        # [arga,args,0,argi,args,argc]
			store                      # [0,argi,args,argc]
			drop                       # [argi,args,argc]
			push 1                     # [1,argi,args,argc]
			add                        # [argi+1,args,argc]
			return                     # [argi+!,args,argc]
			"""
		);
	}
}
