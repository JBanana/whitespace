package org.codeberg.jbanana.whitespace;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Executes Whitespace programs.
 * <h2>Example usage</h2>
 * <pre>java Whitespace myprogram.ws</pre>
 * <h2>Syntax</h2>
 * <pre>java Whitespace [options] myprogram.ws [arg...]</pre>
 * where options are
 * <dl>
 *	<dt>-a</dt><dd>Annotate a Whitespace program instead of executing it. This will dis-assemble Whitespace code to the sort of assembler used by {@link WsAsm}.</dd>
 *	<dt>-v</dt><dd>Verbose output about the program execution. With -a, gives a more verbose annotation.</dd>
 *	<dt>-V</dt><dd>Same as -v, but more verbose.</dd>
 *	<dt>-x</dt><dd>Enable extensions.</dd>
 * </dl>
 */
public class Whitespace {
	private static final BigInteger MINUS_ONE     = BigInteger.valueOf (  -1L );
	private static final BigInteger MINUS_TWO     = BigInteger.valueOf (  -2L );
	private static final BigInteger MINUS_THREE   = BigInteger.valueOf (  -3L );
	private static final BigInteger TWO           = BigInteger.valueOf (   2L );
	private static final BigInteger THIRTY_ONE    = BigInteger.valueOf (  31L );
	private static final BigInteger TWO_FIVE_SIX  = BigInteger.valueOf ( 256L );
	private static final BigInteger CHAR_MAX      = BigInteger.valueOf ( Character.MAX_VALUE );

	private static boolean verbose;
	private static boolean veryVerbose;
	private static boolean annotate;

	/**
	 * Entry point.
	 *
	 * @param args
	 * <dl>
	 *	<dt>-a</dt><dd>Optional - annotates the program as WsAsm assembler instead of executing it.</dd>
	 *	<dt>-v</dt><dd>Optional - verbose output.</dd>
	 *	<dt>-V</dt><dd>Optional - very verbose output.</dd>
	 *	<dt>-x</dt><dd>Optional - enables extensions.</dd>
	 *	<dt>&lt;somefile.ws&gt;</dt><dd>Required - a file contining a whitespace program to execute.</dd>
	 *	<dt>[args]</dt><dd>Optional- arguments to the program.</dd>
	 * </dl>
	 * @throws IOException if there's a problem reading the Whitespace program
	 */
	@SuppressWarnings("fallthrough")
	public static void main ( final String [] args ) throws IOException {
		/*v*/ var fileName   = ( String ) null;
		final var progArgs   = new ArrayList < String > ();
		/*v*/ var compatible = true;

		for ( var index = 0; index < args.length; index++ ) {
			final var arg = args [ index ];
			if ( null == fileName )
				switch ( arg ) {
					case "-a":
						annotate = true;
						break;
					case "-V":
						veryVerbose = true;
						// fallthrough
					case "-v":
						verbose = true;
						break;
					case "-x":
						compatible = false;
						break;
					default:
						fileName = arg;
				}
			else
				progArgs.add ( arg );
		}

		new Whitespace ( fileName, compatible, progArgs ).run ();
	}

	private final String                                 fileName;
	private final LinkedList < BigInteger >              stack      = new LinkedList <> ();
	private final Map < BigInteger, BigInteger >         heap       = new TreeMap <> ();
	private final List < Instruction >                   program    = new ArrayList <> ();
	private /*v*/ int                                    pointer    = 0;
	private final Map < String, Integer >                labels     = new HashMap <> ();
	private final Deque < Integer >                      callStack  = new LinkedList <> ();
	private final BufferedInputStream                    input;
	private final Debug                                  debug;
	private final List < Integer >                       argSource  = new ArrayList <> ();
	private /*v*/ int                                    argIndex   = 0;
	private final Map < BigInteger, InputStreamReader >  readFiles  = new HashMap <> ();
	private final Map < BigInteger, OutputStreamWriter > writeFiles = new HashMap <> ();
	private /*v*/ int                                    fileHandle = -3;
	private final boolean                                compatible;

	private Whitespace ( final String fileName, final boolean compatible, final List < String > args ) {
		this ( fileName, compatible, args, null );
	}

	/*package*/ Whitespace ( final String fileName, final boolean compatible, final List < String > args, final Debug debug ) {
		this.fileName = fileName;
		this.compatible = compatible;
		this.debug = debug;
		if ( args.isEmpty () )
			argSource.add ( 0 );
		else {
			final var len = args.size ();

			argSource.add ( len );
			for ( var i = 0; i < len; i++ ) {
				final var arg = args.get ( i );

				for ( int c = arg.length (), j = 0; j < c; j++ )
					argSource.add ( ( int ) arg.charAt ( j ) );
				argSource.add ( 0 );
			}
		}
		input = new BufferedInputStream (
			null == debug
				? System.in
				: debug.getStdInputStream ()
		);
	}

	/*package*/ void run ()
			throws IOException {
		final var picker = new OperationPicker ();

		try ( final var fis = new FileInputStream ( fileName ) ) {
			for ( var b = 0; -1 != ( b = fis.read () );  ) {
				switch ( b ) {
					case ' ':
						if ( verbose ) System.out.print ( "[Space]" );
						picker.add ( 's' );
						break;
					case '\t':
						if ( verbose ) System.out.print ( "[Tab]" );
						picker.add ( 't' );
						break;
					case '\n':
						if ( verbose ) System.out.print ( "[LF]" );
						picker.add ( 'f' );
						break;
					default:
						continue;
				}
				picker.toProgram ();
			}
		}

		if ( verbose ) {
			System.out.println ();
			System.out.println ( "Labels: " + labels );
			if ( ! annotate ) System.out.println ( "== Executing ==" );
			System.out.println ();
		}

		if ( annotate ) return;

		if ( null != debug ) {
			debug.setStack ( stack );
			debug.setHeap ( heap );
			debug.setProgram ( program );
			debug.setCallStack ( callStack );
		}

		for ( var programSize = program.size();;) {
			final var instruction = program.get ( pointer );

			if ( verbose ) {
				System.out.print ( pointer );
				System.out.print ( ": " );
				System.out.println ( instruction );
			}
			if ( null != debug )
				debug.nextInstruction ( pointer, instruction );
			try {
				instruction.exec ();
			} catch ( final RuntimeException x ) {
				throw new Barf ( "Failed at " + pointer + ": " + instruction + stackDump (), x );
			}
			pointer++;
			if ( veryVerbose ) {
				System.out.println ( "Stack: " + stack );
				System.out.println ( "Heap: " + heap );
				System.out.println ();
			}
			// "Programs must be ended by means of [LF][LF][LF] so that the interpreter can exit cleanly."
			// Some people didn't read the spec, so exit if off end of program
			if ( pointer >= programSize ) System.exit ( 1 );
		}
	}

	private class Barf
			extends RuntimeException {
		private Barf ( final String msg, final RuntimeException x ) {
			super ( msg, x );
		}
		private Barf ( final String msg ) {
			super ( msg );
		}
	}

	private String stackDump () {
		final var result = new StringBuilder ();

		for ( final var i = callStack.iterator (); i.hasNext ();  )
			result.append ( "\n from " )
				.append ( program.get ( i.next () ) );

		return result.toString ();
	}

	/*package*/ class Instruction {
		private final Operation  m_op;
		private final BigInteger m_param;
		private final String     m_label;

		private Instruction ( final Operation op, final BigInteger param, final String label ) {
			m_op    = op;
			m_param = param;
			m_label = label;
		}

		void exec () {
			if ( ParameterType.LABEL == m_op.getParameterType () ) m_op.exec ( m_label );
			else                                                   m_op.exec ( m_param );
		}

		@Override public String toString () {
			final var buf = new StringBuilder ( m_op.toString () );

			if ( ! m_op.arityZero () )
				buf.append ( verbose ? " - " : "" )
					.append ( m_op.isLabel() ? "" : " " )
					.append ( ParameterType.LABEL == m_op.getParameterType () ? ":" + decodeLabel () : m_param );

			return buf.toString ();
		}

		private String decodeLabel () {
			return Whitespace.decodeLabel ( m_label );
		}

		/*package*/ Operation getOp () {
			return m_op;
		}

		/*package*/ BigInteger getParam () {
			return m_param;
		}

		/*package*/ String getLabel () {
			return m_label;
		}
	}

	/*package*/ static String decodeLabel ( final String label ) {
		if ( null == label || label.isEmpty () )
			throw new IllegalArgumentException ( "Label cannot be null or empty." );

		final var result = new StringBuilder ();

		result.append ( label )
			.append ( " - " )
			.append ( decodeBin2Asc ( label ) )
		;

		return result.toString ();
	}

	/*package*/ static String decodeBin2Asc ( final String source ) {
		final var padded = prePadZeros ( source.replaceAll ( "s", "0" ).replaceAll ( "t", "1" ) );
		final var result = new StringBuilder ();

		for ( int index = 0, len = padded.length (); index < len; index += 8 ) {
			final var c = Integer.parseInt ( padded.substring ( index, index + 8 ), 2 );

			if ( c <= 32 ) result.append ( "[" + c + ']' );
			else           result.append ( ( char ) c );
		}

		return result.toString ();
	}

	/*package*/ static String prePadZeros ( final String source ) {
		final var lPadded = "0000000" + source;

		return lPadded.substring ( lPadded.length () % 8 );
	}

	private enum ParameterType {
		NONE,
		NUMBER,
		LABEL
	}

	/*package*/ static abstract class Operation {
		private final ParameterType m_parameterType;

		Operation ( final ParameterType parameterType ) {
			m_parameterType = parameterType;
		}

		void exec ( final BigInteger parameter ) {}
		void exec ( final String label ) {}

		ParameterType getParameterType () {
			return m_parameterType;
		}

		boolean isLabel () {
			return false;
		}

		boolean arityZero () {
			return m_parameterType == ParameterType.NONE;
		}
	}

	// Stack manipulation
	private class Push extends Operation {
		private Push () { super ( ParameterType.NUMBER ); }
		@Override public void exec ( final BigInteger parameter ) {
			stack.push ( parameter );
		}
		@Override public String toString () {
			return veryVerbose
				? "Push the number onto the stack"
				: "push";
		}
	}
	private class Dup extends Operation {
		private Dup () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			stack.push ( stack.peek () );
		}
		@Override public String toString () {
			return veryVerbose
				? "Duplicate the top item on the stack"
				: "dup";
		}
	}
	private class CopyN extends Operation {
		private CopyN () { super ( ParameterType.NUMBER ); }
		@Override public void exec ( final BigInteger parameter ) {
			stack.push ( stack.get ( parameter.intValueExact () ) );
		}
		@Override public String toString () {
			return veryVerbose
				? "Copy the nth item on the stack (given by the argument) onto the top of the stack"
				: "copyn";
		}
	}
	private class Swap extends Operation {
		private Swap () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var a = stack.pop ();
			final var b = stack.pop ();

			stack.push ( a );
			stack.push ( b );
		}
		@Override public String toString () {
			return veryVerbose
				? "Swap the top two items on the stack"
				: "swap";
		}
	}
	private class Drop extends Operation {
		private Drop () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			stack.pop ();
		}
		@Override public String toString () {
			return veryVerbose
				? "Discard the top item on the stack"
				: "drop";
		}
	}
	private class Slide extends Operation {
		private Slide () { super ( ParameterType.NUMBER ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var a = stack.pop ();

			for ( var i = BigInteger.ZERO; i.compareTo ( parameter ) < 0; i = i.add ( BigInteger.ONE ) )
				stack.pop ();
			stack.push ( a );
		}
		@Override public String toString () {
			return veryVerbose
				? "Slide n items off the stack, keeping the top item"
				: "slide";
		}
	}

	// Arithmetic
	private class Add extends Operation {
		private Add () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var r = stack.pop ();
			final var l = stack.pop ();

			stack.push ( l.add ( r ) );
		}
		@Override public String toString () {
			return veryVerbose
				? "Addition"
				: "add";
		}
	}
	private class Sub extends Operation {
		private Sub () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var r = stack.pop ();
			final var l = stack.pop ();

			stack.push ( l.subtract ( r ) );
		}
		@Override public String toString () {
			return veryVerbose
				? "Subtraction"
				: "sub";
		}
	}
	private class Mult extends Operation {
		private Mult () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var r = stack.pop ();
			final var l = stack.pop ();

			stack.push ( l.multiply ( r ) );
		}
		@Override public String toString () {
			return veryVerbose
				? "Multiplication"
				: "mult";
		}
	}
	private class Div extends Operation {
		private Div () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var r = stack.pop ();
			final var l = stack.pop ();

			stack.push ( floorDiv ( l, r ) );
		}
		@Override public String toString () {
			return veryVerbose
				? "Integer Division"
				: "div";
		}
	}

	/*package*/ static BigInteger floorDiv ( final BigInteger left, final BigInteger right ) {
		final var leftNeg  = left.signum () < 0;
		final var rightNeg = right.signum () < 0;
		final var result   = left.divide ( right );

		return ( leftNeg != rightNeg && ( ! result.multiply ( right ).equals ( left ) ) )
			? result.subtract ( BigInteger.ONE )
			: result;
	}

	private class Mod extends Operation {
		private Mod () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var right = stack.pop ();
			final var left  = stack.pop ();

			stack.push ( left.subtract ( floorDiv ( left, right ).multiply ( right ) ) );
		}
		@Override public String toString () {
			return veryVerbose
				? "Modulo"
				: "mod";
		}
	}

	// Heap access
	private class Store extends Operation {
		private Store () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var v = stack.pop ();
			final var a = stack.pop ();

			if ( BigInteger.ZERO.compareTo ( a ) > 0 )
				throw new IllegalStateException ( "Store to heap address " + a + " - less than zero!" );

			heap.put
			(
				a,
				BigInteger.ZERO != parameter && BigInteger.ZERO.equals ( parameter )
					? BigInteger.ZERO
					: v
			);
		}
		@Override public String toString () {
			return veryVerbose
				? "Store"
				: "store";
		}
	}
	private class Retrieve extends Operation {
		private Retrieve () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var a = stack.pop ();

			if ( BigInteger.ZERO.compareTo ( a ) > 0 )
				throw new IllegalStateException ( "Retrieve from heap address " + a + " - less than zero!" );
			if ( heap.containsKey ( a ) )
				stack.push ( heap.get ( a ) );
			else {
				final var maxKey = heap.keySet ()
					.stream ()
					.max ( BigInteger::compareTo )
					.orElseThrow ( () -> new IllegalStateException ( "Retrieve from heap address " + a + " - heap empty." ) );
				if ( maxKey.compareTo ( a ) > 0 )
					stack.push ( BigInteger.ZERO );
				else
					throw new IllegalStateException ( "Retrieve from heap address " + a + " - address too big." );
			}
		}
		@Override public String toString () {
			return veryVerbose
				? "Retrieve"
				: "retrieve";
		}
	}
	private class XDump extends Operation {
		private XDump () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			if ( compatible ) throw new Barf ( "Extensions not enabled." );
			System.out.println ( "Pointer: " + pointer );
			System.out.println ( "Stack: " + stack );
			System.out.println ( "Heap: " + heap );
			System.out.println ( analyseHeap ( heap ) );
			System.out.println ();
		}
		@Override public String toString () {
			return veryVerbose
				? "XDump (non-standard debug operation)"
				: "x-dump";
		}
	}

	// Flow
	private class Label extends Operation {
		private Label () { super ( ParameterType.LABEL ); }
		@Override public void exec ( final String label ) {
			if ( veryVerbose ) {
				System.out.println ( "Stack: " + stack );
				System.out.println ( "Heap: " + heap );
			}
		}
		@Override public String toString () {
			return veryVerbose
				? "Mark a location in the program"
				: "";
		}
		@Override public boolean isLabel () {
			return true;
		}
	}
	/*package*/ class Call extends Operation {
		private Call () { super ( ParameterType.LABEL ); }
		@Override public void exec ( final String label ) {
			if ( ! labels.containsKey ( label ) ) throw new IllegalArgumentException ( "Label not found: " + decodeLabel ( label ) );
			callStack.push ( pointer );
			pointer = labels.get ( label );
		}
		@Override public String toString () {
			return veryVerbose
				? "Call a subroutine"
				: "call";
		}
	}
	private class Jump extends Operation {
		private Jump () { super ( ParameterType.LABEL ); }
		@Override public void exec ( final String label ) {
			if ( ! labels.containsKey ( label ) ) throw new IllegalArgumentException ( "Label not found: " + decodeLabel ( label ) );
			pointer = labels.get ( label );
		}
		@Override public String toString () {
			return veryVerbose
				? "Jump unconditionally to a label"
				: "jump";
		}
	}
	private class JumpZ extends Operation {
		private JumpZ () { super ( ParameterType.LABEL ); }
		@Override public void exec ( final String label ) {
			if ( BigInteger.ZERO.equals ( stack.pop () ) ) {
				if ( ! labels.containsKey ( label ) ) throw new IllegalArgumentException ( "Label not found: " + decodeLabel ( label ) );
				pointer = labels.get ( label );
			}
		}
		@Override public String toString () {
			return veryVerbose
				? "Jump to a label if the top of the stack is zero"
				: "jumpz";
		}
	}
	private class JumpN extends Operation {
		private JumpN () { super ( ParameterType.LABEL ); }
		@Override public void exec ( final String label ) {
			if ( BigInteger.ZERO.compareTo ( stack.pop () ) > 0 ) {
				if ( ! labels.containsKey ( label ) ) throw new IllegalArgumentException ( "Label not found: " + decodeLabel ( label ) );
				pointer = labels.get ( label );
			}
		}
		@Override public String toString () {
			return veryVerbose
				? "Jump to a label if the top of the stack is negative"
				: "jumpn";
		}
	}
	private class Return extends Operation {
		private Return () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			pointer = callStack.pop ();
		}
		@Override public String toString () {
			return veryVerbose
				? "End a subroutine and transfer control back to the caller"
				: "return";
		}
	}
	private class End extends Operation {
		private End () { super ( compatible ? ParameterType.NONE : ParameterType.NUMBER ); }
		@Override public void exec ( final BigInteger parameter ) {
			System.exit ( compatible ? 0 : parameter.intValueExact () );
		}
		@Override public String toString () {
			return veryVerbose
				? "End the program"
				: "end";
		}
	}

	// I/O
	private class PrintC extends Operation {
		private PrintC () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			System.out.print ( ( char ) stack.pop ().intValue () );
		}
		@Override public String toString () {
			return veryVerbose
				? "Output the character at the top of the stack"
				: "printc";
		}
	}
	private class PrintN extends Operation {
		private PrintN () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			System.out.print ( stack.pop () );
		}
		@Override public String toString () {
			return veryVerbose
				? "Output the number at the top of the stack"
				: "printn";
		}
	}
	private class ReadC extends Operation {
		private ReadC () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			try {
				final var b = input.read ();

				if ( b == -1 && compatible ) throw new RuntimeException ( "End of file" );

				heap.put ( stack.pop (), BigInteger.valueOf ( b ) );
			} catch ( final IOException x ) {
				throw new RuntimeException ( "Could not read character input: " + x, x );
			}
		}
		@Override public String toString () {
			return veryVerbose
				? "Read a character and place it in the location given by the top of the stack"
				: "readc";
		}
	}
	private class ReadN extends Operation {
		private ReadN () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var buf = new StringBuilder ();

			for ( int b; -1 != ( b = readByte () );  ) {
				if ( b >= '0' && b <= '9' ) {
					buf.append ( ( char ) b );
				}
			}

			heap.put (
				stack.pop (),
				new BigInteger ( buf.toString ().trim () )
			);
		}
		private int readByte () {
			try {
				final int b = input.read ();

				if ( 0x0d != b && 0x0a != b )
					return b;
				if ( 0x0a == b )
					return -1;
				input.mark ( 1 );
				if ( 0x0a != input.read () )
					input.reset ();
				return -1;
			} catch ( final IOException x ) {
				throw new IllegalStateException ( "Failed to read inout: " + x, x );
			}
		}
		@Override public String toString () {
			return veryVerbose
				? "Read a number and place it in the location given by the top of the stack"
				: "readn";
		}
	}
	private class XArgs extends Operation {
		private XArgs () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			if ( compatible ) throw new Barf ( "To use x-args please enable extensions." );
			stack.push ( BigInteger.valueOf ( argSource.get ( argIndex++ ) ) );
		}
		@Override public String toString () {
			return veryVerbose
				? "Read from the program arguments"
				: "x-args";
		}
	}
	private class XReadFile extends Operation {
		private XReadFile () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var arg = stack.pop ();

			if ( arg.signum () < 0 ) {
				// Negative: existing file handle to read
				if ( ! readFiles.containsKey ( arg ) ) {
					stack.push ( MINUS_TWO );
					return;
				}
				try {
					stack.push ( BigInteger.valueOf ( readFiles.get ( arg ).read () ) );
				} catch ( final IOException x ) {
					x.printStackTrace ();
					stack.push ( MINUS_THREE );
					return;
				}
			} else {
				// Non-negative: pointer to name of new file to open
				final StringBuilder fileName = new StringBuilder ();

				for ( var ptr = arg;  ; ptr = ptr.add ( BigInteger.ONE ) ) {
					final var num = heap.get ( ptr );

					if ( num.compareTo ( CHAR_MAX ) > 0 ) {
						stack.push ( BigInteger.ZERO );
						return;
					}

					if ( num.compareTo ( BigInteger.ZERO ) == 0 )
						break;

					fileName.append ( ( char ) num.intValueExact () );
				}

				final var result = BigInteger.valueOf ( fileHandle-- );

				try {
					readFiles.put ( result, new InputStreamReader ( new FileInputStream ( fileName.toString () ), UTF_8 ) );
					stack.push ( result );
				} catch ( final IOException x ) {
					x.printStackTrace ();
					stack.push ( BigInteger.ZERO );
				}
			}
		}
		@Override public String toString () {
			return veryVerbose
				? "Read from a file"
				: "x-readfile";
		}
	}
	private class XWriteFile extends Operation {
		private XWriteFile () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger parameter ) {
			final var arg = stack.pop ();

			if ( arg.signum () < 0 ) {
				// Negative: existing file handle to write
				if ( ! writeFiles.containsKey ( arg ) ) {
					stack.push ( MINUS_TWO );
					return;
				}
				try {
					final var num = stack.pop ();

					if ( num.compareTo ( CHAR_MAX ) > 0 || num.compareTo ( BigInteger.ZERO ) < 0 ) {
						stack.push ( MINUS_ONE );
						return;
					}
					var writer = writeFiles.get ( arg );
					writer.write ( ( char ) num.intValueExact () );
					writer.flush ();
					stack.push ( BigInteger.ZERO );
				} catch ( final IOException x ) {
					x.printStackTrace ();
					stack.push ( MINUS_THREE );
					return;
				}
			} else {
				// Non-negative: pointer to name of file to open
				final StringBuilder fileName = new StringBuilder ();

				for ( var ptr = arg;  ; ptr = ptr.add ( BigInteger.ONE ) ) {
					final var num = heap.get ( ptr );

					if ( num.compareTo ( CHAR_MAX ) > 0 ) {
						stack.push ( BigInteger.ZERO );
						return;
					}

					if ( num.compareTo ( BigInteger.ZERO ) == 0 )
						break;

					fileName.append ( ( char ) num.intValueExact () );
				}

				final var result = BigInteger.valueOf ( fileHandle-- );

				try {
					writeFiles.put ( result, new OutputStreamWriter ( new FileOutputStream ( fileName.toString () ), UTF_8 ) );
					stack.push ( result );
				} catch ( final IOException x ) {
					x.printStackTrace ();
					stack.push ( BigInteger.ZERO );
				}
			}
		}
		@Override public String toString () {
			return veryVerbose
				? "Write to a file"
				: "x-writefile";
		}
	}
	private class XCloseFile extends Operation {
		private XCloseFile () { super ( ParameterType.NONE ); }
		@Override public void exec ( final BigInteger __ ) {
			final var arg = stack.pop ();

			try {
				if ( readFiles.containsKey ( arg ) )
					readFiles.get ( arg ).close ();
				else if ( writeFiles.containsKey ( arg ) )
					writeFiles.get ( arg ).close ();
				else
					System.err.println ( "Tried to close file handle " + arg + " which was not open." );
			} catch ( final IOException x ) {
				System.err.println ( "Could not close file handle " + arg + ": " + x );
				x.printStackTrace ();
			}
		}
		@Override public String toString () {
			return veryVerbose
				? "Close a file"
				: "x-closefile";
		}
	}

	private class OperationPicker {
		private final Map < String, Operation > operations = new HashMap <> ();

		{
			// Stack manipulation
			operations.put ( "ss",  new Push  () );
			operations.put ( "sfs", new Dup   () );
			operations.put ( "sts", new CopyN () );
			operations.put ( "sft", new Swap  () );
			operations.put ( "sff", new Drop  () );
			operations.put ( "stf", new Slide () );

			// Arithmetic
			operations.put ( "tsss", new Add  () );
			operations.put ( "tsst", new Sub  () );
			operations.put ( "tssf", new Mult () );
			operations.put ( "tsts", new Div  () );
			operations.put ( "tstt", new Mod  () );

			// Heap access
			operations.put ( "tts", new Store    () );
			operations.put ( "ttt", new Retrieve () );
			operations.put ( "ttf", new XDump    () );

			// Flow
			operations.put ( "fss", new Label  () );
			operations.put ( "fst", new Call   () );
			operations.put ( "fsf", new Jump   () );
			operations.put ( "fts", new JumpZ  () );
			operations.put ( "ftt", new JumpN  () );
			operations.put ( "ftf", new Return () );
			operations.put ( "fff", new End    () );

			// I/O
			operations.put ( "tfss",  new PrintC     () );
			operations.put ( "tfst",  new PrintN     () );
			operations.put ( "tfts",  new ReadC      () );
			operations.put ( "tftt",  new ReadN      () );
			operations.put ( "tfsf",  new XArgs      () );
			operations.put ( "tftfs", new XReadFile  () );
			operations.put ( "tftff", new XWriteFile () );
			operations.put ( "tftft", new XCloseFile () );
		}

		private /*v*/ String    key       = "";
		private /*v*/ String    parameter = "";
		private /*v*/ Operation operation;

		private void add ( final char c ) {
			if ( null == operation ) key += c;
			else                     parameter += c;
		}

		private void toProgram () {
			if ( null == operation ) {
				if ( operations.containsKey ( key ) ) {
					operation = operations.get ( key );
					if ( operation.arityZero () )
						addInstruction ();
				}
			} else
				if ( parameter.endsWith ( "f" ) )
					addInstruction ();
		}

		private void addInstruction () {
			final var usesLabel   = ParameterType.LABEL == operation.getParameterType ();
			final var intParam    = usesLabel ? BigInteger.ZERO : makeInt ( parameter );
			final var labelParam  = usesLabel ? parameter.substring ( 0, parameter.length () - 1 ) : null;
			final var instruction = new Instruction ( operation, intParam, labelParam );

			if ( verbose ) {
				System.out.println ();
				System.out.print ( program.size () );
				System.out.print ( ": " );
			}
			if ( verbose || annotate )
				System.out.println ( instruction );

			if ( operation.isLabel () )
				labels.put ( labelParam, program.size () );
			program.add ( instruction );
			operation = null;
			key = "";
			parameter = "";
		}
	}

	private static BigInteger makeInt ( final String p ) {
		if ( p.isEmpty () ) return BigInteger.ZERO;

		final var buf  = new StringBuilder ();
		final var sign = p.charAt ( 0 );

		for ( int i = 1, c = p.length (); i < c; i++ )
			switch ( p.charAt ( i ) ) {
				case 's':
					buf.append ( '0' );
					break;
				case 't':
					buf.append ( '1' );
					break;
			}

		if ( 0 == buf.length () ) return BigInteger.ZERO;

		final var result = new BigInteger ( buf.toString (), 2 );

		return 's' == sign
			? result
			: result.negate ();
	}

	/*package*/ interface Debug {
		void setStack        ( LinkedList < BigInteger >      stack                             );
		void setHeap         ( Map < BigInteger, BigInteger > heap                              );
		void setProgram      ( List < Instruction >           program                           );
		void setCallStack    ( Deque < Integer >              callStack                         );
		void nextInstruction ( int                            pointer,  Instruction instruction );

		InputStream getStdInputStream ();
	}

	/*package*/ static String analyseHeap ( final Map < BigInteger, BigInteger > heap ) {
		return analyseHeap ( heap, false );
	}

	/*package*/ static String analyseHeap ( final Map < BigInteger, BigInteger > heap, final boolean full ) {
		if ( null == heap.get ( TWO_FIVE_SIX ) )
			return "No malloc setup.";

		if ( hasValue ( heap, TWO_FIVE_SIX, 0 ) )
			return "Nothing malloced.";

		final var result = new StringBuilder ();

		for ( var a = TWO_FIVE_SIX; ! hasValue ( heap, a, 0 );  ) {
			final var block     = a;
			final var nextBlock = heap.get ( a );
			final var size      = nextBlock.subtract ( block ).subtract ( TWO );

			a = a.add ( BigInteger.ONE );

			final var allocated = ! heap.get ( a ).equals ( BigInteger.ZERO );

			a = a.add ( BigInteger.ONE );

			if ( allocated || full ) {
				result.append ( "At " )
					.append ( a )
					.append ( ": " )
					.append ( allocated ? "" : "un" )
					.append ( "allocated block of " )
					.append ( size )
					.append ( allocated ? ": " : "" );
				if ( allocated )
					for ( var i = BigInteger.ZERO; i.compareTo ( size ) < 0; i = i.add ( BigInteger.ONE  ) ) {
						final var value = heap.get ( a.add ( i ) );

						if ( value.compareTo ( THIRTY_ONE ) > 0 && value.compareTo ( TWO_FIVE_SIX ) < 0 )
							result.append ( ( char ) value.intValue () );
						else
							result.append ( "[" + value + ']' );
					}
				result.append ( '\n' );
			}
			a = nextBlock;
		}

		return result.toString ().trim ();
	}

	/*package*/ static boolean hasValue ( final Map < BigInteger, BigInteger > heap, final BigInteger address, final int value ) {
		return BigInteger.valueOf ( value ).equals ( heap.get ( address ) );
	}
}
