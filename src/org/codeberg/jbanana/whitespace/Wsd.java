package org.codeberg.jbanana.whitespace;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import static java.util.stream.Collectors.toList;

/**
 * Debugger for a Whitespace program. Takes a <code>.wsa</code> assembler file,
 * assembles it, and lets you step through the code, set breakpoints, etc.
 */
public class Wsd
		implements Whitespace.Debug {
	/**
	 * Entry point.
	 *
	 * @param args a source (<code>.wsa</code>) file name and a file to
	 *             use as input to the Whitespace program.
	 * @throws IOException if there's a problem reading the assembler code or
	 *                     writing the Whitespace program.
	 */
	public static void main ( final String [] args )
			throws IOException {
		var argIndex   = 0;
		var compatible = true;

		if ( "-x".equals ( args [ argIndex ] ) ) {
			compatible = false;
			argIndex++;
		}

		final var wsaFileName = args [ argIndex++ ];
		final var argList     = new ArrayList < String > ();
		
		for (  ; argIndex < args.length; argIndex++ )
			argList.add ( args [ argIndex ] );

		new Wsd (
			wsaFileName,
			argList,
			new WsAsm (
				new File ( wsaFileName ),
				null,
				compatible
			).assemble (),
			compatible
		).debug ();
	}

	private final Whitespace                      ws;
	private final InputStream                     stdinProxy;
	private /*v*/ LinkedList < BigInteger >       stack;
	private /*v*/ Map < BigInteger, BigInteger >  heap;
	private /*v*/ List < Whitespace.Instruction > program;
	private /*v*/ Deque < Integer >               callStack;
	private /*v*/ int                             stackTargetValue;
	private final Map < Integer, Integer >        lineMap;
	private final Set < Integer >                 breakpoints       = new TreeSet <> ();
	private /*v*/ boolean                         running;
	private /*v*/ boolean                         steppingOut;
	private final List < String >                 source            = new ArrayList <> ();
	private final Scanner                         rcLines;
	private final List < String >                 monitors          = new ArrayList <> ( List.of ( "heap", "list", "stack", "analyse" ) );
	private final boolean                         compatible;

	private Wsd ( final String wsaFileName, final List < String > args, final Map < Integer, Integer > lineMap, final boolean compatible )
			throws IOException {
		final File wsaFile   = new File ( wsaFileName );
		final File rcFile    = new File ( wsaFile.getParent (), wsaFile.getName () + ".wsdrc" );
		final File stdinFile = new File ( wsaFile.getParent (), wsaFile.getName () + ".wsdin" );
		final File wsFile    = WsAsm.getWsFile ( wsaFile );

		this.compatible = compatible;

		try ( final Scanner s = new Scanner ( wsaFile, "UTF-8" ) ) {
			while ( s.hasNextLine () )
				source.add ( s.nextLine () );
		}

		stdinProxy = stdinFile.exists () ? new FileInputStream ( stdinFile ) : new ByteArrayInputStream ( new byte [ 0 ] );
		this.lineMap = new java.util.TreeMap <> ( lineMap );
		rcLines = rcFile.exists () ? new Scanner ( rcFile, "UTF-8" ) : null;
		ws = new Whitespace ( wsFile.getAbsolutePath (), compatible, args, this );
	}

	private void debug ()
			throws IOException {
		ws.run ();
	}

	@Override
	public void setStack ( final LinkedList < BigInteger > stack ) {
		this.stack = stack;
	}

	@Override
	public void setHeap ( final Map < BigInteger, BigInteger > heap ) {
		this.heap = heap;
	}

	@Override
	public void setProgram ( final List < Whitespace.Instruction > program ) {
		this.program = program;
	}

	@Override
	public void setCallStack ( final Deque < Integer > callStack ) {
		this.callStack = callStack;
	}

	@Override
	public void nextInstruction ( final int pointer, final Whitespace.Instruction instruction ) {
		if ( 0 == pointer ) {
			if ( null != rcLines && rcLines.hasNextLine () )
				while ( null != rcLines && rcLines.hasNextLine () )
					processCommand ( rcLines.nextLine (), pointer );
		}

		final int callStackSize = callStack.size ();

		if ( stackTargetValue > callStackSize )
			stackTargetValue = callStackSize;

		//var tmpLineNo = lineMap.get ( pointer );
		//System.out.println
		//(
		//	"stackTargetValue: " + stackTargetValue
		//	+ ", callStackSize: " + callStackSize
		//	+ ", pointer: " + pointer
		//	+ (
		//		null == tmpLineNo
		//			? ""
		//			: ", line: " + tmpLineNo
		//				+ ' ' + source.get ( tmpLineNo - 1 )
		//	)
		//);

		// stopFlag = hit a breakpoint?
		//	or (not running and not ( stack size bigger than stackTargetValue ) )
		final boolean stopFlag = isBreakpoint ( pointer )
			|| ( ! running && callStackSize <= stackTargetValue && lineMap.containsKey ( pointer ) );

		if ( stopFlag ) {
			running = false;
			steppingOut = false;
			System.out.println ( "Ptr: " + pointer + ", maxdpth (" + stackTargetValue + ") calls " + callStack.size () + ' ' + instruction );
			for ( final var command : monitors )
				processCommand ( command, pointer );
			handleUserInput ( pointer );
			System.out.println ();
		}

		if ( instruction.getOp () instanceof Whitespace.Call ) {
			if ( Whitespace.decodeBin2Asc ( instruction.getLabel () ).startsWith ( "$" ) ) {
				// If about to call a $ macro and stopFlag is not set, stackTargetValue = stack size
				if ( ! stopFlag && ! steppingOut )
					stackTargetValue = callStackSize;
			} else if ( ! running && ! steppingOut ) {
				// It's not a $ call, so increment stackTargetValue
				stackTargetValue++;
			}
		}
	}

	private boolean isBreakpoint ( final int instruction ) {
		return
			(
				0 == instruction
				&& ! running
			)
			||
			(
				lineMap.containsKey ( instruction )
				&& breakpoints.contains ( lineMap.get ( instruction ) )
			);
	}

	private void handleUserInput ( final int pointer ) {
		for (;;) {
			System.out.print ( "? " );
			if ( processCommand ( new Scanner ( System.in ).nextLine (), pointer ) )
				return;
		}
	}

	@SuppressWarnings("fallthrough")
	private boolean processCommand ( final String command, final int pointer ) {
		switch ( command ) {
			case "clear":
			case "stop":
			case "s":
			case "cl":
				System.out.println ( breakpoints );
				break;
			case "stack":
				System.out.println ( "Stack: " + stack );
				break;
			case "heap":
				System.out.println ( "Heap: " + heap );
				break;
			case "analyse":
			case "a":
				System.out.println ( Whitespace.analyseHeap ( heap ) );
				break;
			case "analysefull":
			case "af":
				System.out.println ( Whitespace.analyseHeap ( heap, true ) );
				break;
			case "calls":
			case "ca":
				System.out.println (
					"Call stack:\n"
						+ String.join (
							"\n",
							callStack.stream ()
								.map ( lineMap::get )
								.map ( line -> String.format ( "%4d  %s", line, source.get ( line - 1 ) ) )
								.collect ( toList () )
							)
				);
				break;
			case "run":
			case "cont":
			case "r":
			case "co":
				running = true;
				return true;
			case "next":
			case "n":
			case "step":
			case "ste":
			case "step in":
				running = false;
				return true;
			case "out":
			case "o":
			case "step out":
			case "step up":
				running = false;
				steppingOut = true;
				stackTargetValue = callStack.size () - 1;
				return true;
			case "l":
			case "list":
				listLines
				(
					lineMap.containsKey ( pointer )
						? lineMap.get ( pointer )
						: -1
				);
				break;
			case "unmonitor":
			case "u":
			case "monitor":
			case "m":
				monitors.forEach ( System.out::println );
				break;
			case "q":
			case "e":
			case "x":
			case "quit":
			case "exit":
				System.exit ( 2 );
				// Compiler thinks this is a fallthrough.
				// It isn't.
			case "help":
			case "h":
			case "?":
				System.out.println (
					"""
					Commands:
					---------
					stop n
					   - sets a breakpoint at source line n
					   synonyms: "s n", "stop at n"
					clear n
					   - removes a breakpoint at source line n
					   synonyms: "cl n", "clear at n"
					clear
					   - list breakpoints
					   synonyms: "stop", "s", "cl"
					stack
					   - print the stack
					setstack i v
					   - set the value of stack element i to v - zero is the top of the stack
					   synonyms: "ss i v"
					heap
					   - print the heap
					sethead a v
					   - set the value of heap address a to v
					   synonyms: "sh a v"
					analyse
					   - print heap analysis
					   synonyms: "a"
					analysefull
					   - print heap analysis, including unallocated blocks
					   synonyms: "af"
					calls
					   - print the call stack
					   synonyms: "ca"
					run
					   - run the program
					   synonyms: "r", "cont", "co"
					next
					   - step to the next source line
					   synonyms: "n", "step", "step in"
					out
					   - step out of the current call
					   synonyms: "o", "step out"
					list
					   - show wsa source at current line
					   synonyms: "l"
					list n
					   - show wsa source at given line
					   synonyms: "l n"
					   Hint: to see the next chunk of code, add 10 to to the current line number.
					monitor foo
					   - run the command foo after every breakpoint is reached
					   synonyms: "m foo"
					   Note: you can add as many monitors as you like. The initial defaults are
					   "heap", "list", "stack", "analyse"
					unmonitor
					   - list monitors
					   synonyms: "u", "m", "monitor"
					unmonitor foo
					   - stop monitoring foo
					quit
					   - abort the program and the debugger immediately
					   synonyms: "q", "exit", "e", "x"
					help
					   - show this list of commands
					   synonyms: "h", "?"
					"""
				);
				break;
			default:
				if ( command.startsWith ( "stop " ) || command.startsWith ( "s " ) ) {
					final int bp = parseLineNo ( command, command.lastIndexOf ( " " ) + 1 );

					if ( bp > 0 ) {
						breakpoints.add ( bp );
						System.out.println ( breakpoints );
					}
				} else if ( command.startsWith ( "clear " ) || command.startsWith ( "cl " ) ) {
					final int bp = parseLineNo ( command, command.lastIndexOf ( " " ) + 1 );
					if ( bp > 0 ) {
						breakpoints.remove ( bp );
						System.out.println ( breakpoints );
					}
				} else if ( command.startsWith ( "list " ) || command.startsWith ( "l " ) ) {
					final int lineNum = parseLineNo ( command, command.lastIndexOf ( " " ) + 1 );
					if ( lineNum > 0 )
						listLines ( lineNum );
				} else if ( command.startsWith ( "setstack " ) || command.startsWith ( "ss " ) ) {
					var parts = command.split ( " +" );
					if ( 3 == parts.length )
					{
						try {
							var index = Integer.parseInt ( parts [ 1 ] );
							var value = new BigInteger ( parts [ 2 ] );
							stack.set ( index, value );
						} catch ( final NumberFormatException x ) {
							System.out.println ( "Didn't understand [" + command + "]: " + x.getMessage () );
						}
					} else {
						System.out.println ( "Didn't understand [" + command + ']' );
					}
				} else if ( command.startsWith ( "setheap " ) || command.startsWith ( "sh " ) ) {
					var parts = command.split ( " +" );
					if ( 3 == parts.length )
					{
						try {
							var addr = new BigInteger ( parts [ 1 ] );
							var value = new BigInteger ( parts [ 2 ] );
							heap.put ( addr, value );
						} catch ( final NumberFormatException x ) {
							System.out.println ( "Didn't understand [" + command + "]: " + x.getMessage () );
						}
					} else {
						System.out.println ( "Didn't understand [" + command + ']' );
					}
				} else if ( command.startsWith ( "monitor " ) || command.startsWith ( "m " ) ) {
					monitors.add ( command.substring ( command.indexOf ( ' ' ) + 1 ) );
				} else if ( command.startsWith ( "unmonitor " ) || command.startsWith ( "u " ) ) {
					monitors.remove ( command.substring ( command.indexOf ( ' ' ) + 1 ) );
				} else {
					System.out.println ( "Didn't understand [" + command + ']' );
				}
				break;
		}
		return false;
	}

	private void listLines ( final int lineNumber ) {
		if ( lineNumber <= 0 ) return;

		final int start = Math.max ( 1,              lineNumber - 4 );
		final int end   = Math.min ( source.size (), lineNumber + 5 );

		for ( int i = start - 1; i < end; i++ ) {
			final int thisLine = i + 1;
			System.out.println (
				String.format (
					"%4d %s %s",
					thisLine,
					( thisLine == lineNumber ? "=> " : "   " ),
					source.get ( i )
				)
			);
		}
	}

	private static int parseLineNo ( final String line, final int from ) {
		try {
			return Integer.parseInt ( line.substring ( from ) );
		} catch ( final NumberFormatException x ) {
			System.out.println ( "Oops: " + x.getMessage () );
			return -1;
		}
	}

	@Override
	public InputStream getStdInputStream () {
		return stdinProxy;
	}
}

/*
Thoughts of live editing in the debugger

Write foo.wsa
Debug with wsad
Notice missing op
Insert op as next instr
Continue

Whitespace would have to allow runtime manipulation
	Could tweak internal model and export back to file
		But this would lose comments
		And macros!
	Could tweak source and reassemble
		But can we guarantee it's safe to run the new code with the live data?
		We were at instruction n with stack/heap/callstack x/y/z
		So all the code we executed should stay the same to ensure that the new code is used correctly
		Whitespace would have to record the executed instructions
		We could diff that against the new code, and fail to continue if not same
*/
