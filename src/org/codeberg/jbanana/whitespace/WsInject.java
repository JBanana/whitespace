package org.codeberg.jbanana.whitespace;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Injects a Whitespace program into some other file, replacing its white space
 * with the Whitespace code. The original files are unchanged. The injection
 * result is written to stdout. Works best for files where white space is not
 * significant, for example, some HTML files, or code in curly bracket
 * languages. However, it's still likely to fail in many cases. HTML "pre"
 * blocks will get messed up. Code that relies on line breaks (e.g. //
 * comments) will get messed up
 */
public class WsInject {
	/**
	 * Entry point.
	 *
	 * @param args a source (Whitespace) file name and a target (other)
	 *             file name.
	 * @throws IOException if there's a problem reading the Whitespace program
	 *                     or writing the injected version to stdout.
	 */
	public static void main ( final String [] args )
			throws IOException {
		if ( args.length < 2 ) {
			help ();
			return;
		}

		final File source = new File ( args [ 0 ] );
		final File target = new File ( args [ 1 ] );

		if ( ! source.exists () || ! target.exists () ) {
			help ();
			return;
		}

		final List < Character > sourceTokens = cleanse ( source );
		final List < String >    targetTokens = tokenise ( target );
		final int                sourceCount  = sourceTokens.size ();
		final int                targetCount  = targetTokens.size ();
		final int                multiplier   = getMultiplier ( sourceCount, targetCount );

		for ( int i = 0, c = Math.max ( sourceCount, targetCount ); i < c; i++ ) {
			if ( i < targetCount )
				System.out.print ( targetTokens.get ( i ) );
			if ( i < sourceCount )
				for ( int j = 0; j < multiplier; j++ ) {
					final int k = i * multiplier + j;
					if ( k < sourceCount )
						System.out.print ( sourceTokens.get ( k ) );
					else
						break;
				}
			else
				System.out.print ( ' ' );
		}
	}

	private static void help () {
		System.out.println ( "WsInject help" );
		System.out.println ( "-------------" );
		System.out.println ( "Required parameters: myprog.ws mydoc.html" );
		System.out.println ( "We assume that the first file is valid Whitespace code" );
		System.out.println ( "and the second will survive the replacement of its white space." );
		System.out.println ( "Neither file is updated." );
		System.out.println ( "You probably want to redirect the output to a third file." );
	}

	/*package*/ static int getMultiplier ( final int sourceCount, final int targetCount ) {
		if ( targetCount >= sourceCount )
			return 1;
		for ( int i = 3;  ; i++ ) {
			if ( targetCount * i > sourceCount ) {
				return i - 2;
			}
		}
	}

	/*package*/ static List < Character > cleanse ( final File wsFile )
			throws IOException {
		final var result = new ArrayList < Character > ();

		try ( final var fis = new FileInputStream ( wsFile ) ) {
			for ( int b; -1 != ( b = fis.read () );  ) {
				switch ( ( b ) ) {
					case ' ':
					case '\t':
					case '\n':
						result.add ( ( char ) b );
				}
			}
		}

		return result;
	}

	/*package*/ static List < String > tokenise ( final File targetFile )
			throws IOException {
		final var result = new ArrayList < String > ();

		try ( final Scanner s = new Scanner ( targetFile ) ) {
			while ( s.hasNext () )
				result.add ( s.next () );
		}

		return result;
	}
}
