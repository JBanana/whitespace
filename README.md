# Whitespace
An implementation of the Whitespace language together with some tools to help in writing Whitespace programs

Also included here is a copy of the original Haskell implementation, with minor tweaks to make it compile in ghc-9.2.2

## How to build
To build this, you need
 1. a clone of this repo
 2. Java 17 (or higher - try `java -version`)
 3. Ant 1.10 (or higher - `ant -version`)
Then:
```
cd whitespace
ant
```

## How to run
The bin directory has scripts. For example,
```
cd whitespace
./bin/ws examples/rpncalc.ws
```

## How to write Whitespace code
You can hand-code Whitespace, but that's hard. It's easier to write assembler code and generate the Whitespace with WsAsm.

See the [documentation](https://jbanana.codeberg.page/whitespace/) for more details on the assembler, the debugger, and the injection tool.
