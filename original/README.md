# Whitespace - original
The original implementation of the Whitespace language extracted from the Wayback Machine and tweaked to work with ghc-9.2.2

## How to build
To build this, you need
 1. a clone of this repo
 2. the Glasgow Haskell Compiler, 9.2.2 (or higher, I guess)
Then:
```
cd whitespace
cd original
make
```

## Everything else
See the original tutorial, included (with minor tweaks) here.
