package org.codeberg.jbanana.whitespace;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class WhitespaceTest
		extends Test {
	void testAnalyseNoMallocSetupHeap () {
		final var heap = new HashMap < BigInteger, BigInteger > ();

		assertEquals ( "No malloc setup.", Whitespace.analyseHeap ( heap ) );
	}

	void testAnalyseUnmallocedHeap () {
		final var heap = new HashMap < BigInteger, BigInteger > ();

		load ( heap, 256, 0 );
		load ( heap, 257, 0 );

		assertEquals ( "Nothing malloced.", Whitespace.analyseHeap ( heap ) );
	}

	void testAnalyseMalloc3Heap () {
		final var heap = new HashMap < BigInteger, BigInteger > ();

		load ( heap, 256, 261 );
		load ( heap, 257, 1   );
		load ( heap, 258, 0   );
		load ( heap, 259, 0   );
		load ( heap, 260, 0   );
		load ( heap, 261, 0   );
		load ( heap, 262, 0   );

		assertEquals ( "At 258: allocated block of 3: [0][0][0]", Whitespace.analyseHeap ( heap ) );
	}

	void testAnalyseMallocCatOnHeap () {
		final var heap = new HashMap < BigInteger, BigInteger > ();

		load ( heap, 256, 262 );
		load ( heap, 257, 1   );
		load ( heap, 258, 67  );
		load ( heap, 259, 97  );
		load ( heap, 260, 116 );
		load ( heap, 261, 0   );
		load ( heap, 262, 0   );
		load ( heap, 263, 0   );

		assertEquals ( "At 258: allocated block of 4: Cat[0]", Whitespace.analyseHeap ( heap ) );
	}

	void testAnalyseMallocCatDogOnHeap () {
		final var heap = new HashMap < BigInteger, BigInteger > ();

		load ( heap, 256, 262 );
		load ( heap, 257, 1   );
		load ( heap, 258, 67  );
		load ( heap, 259, 97  );
		load ( heap, 260, 116 );
		load ( heap, 261, 0   );
		load ( heap, 262, 268 );
		load ( heap, 263, 1   );
		load ( heap, 264, 68  );
		load ( heap, 265, 111 );
		load ( heap, 266, 103 );
		load ( heap, 267, 0   );
		load ( heap, 268, 0   );
		load ( heap, 269, 0   );

		assertEquals ( "At 258: allocated block of 4: Cat[0]\nAt 264: allocated block of 4: Dog[0]", Whitespace.analyseHeap ( heap ) );
	}

	void testAnalyseMallocCatDogFreeCatOnHeap () {
		final var heap = new HashMap < BigInteger, BigInteger > ();

		load ( heap, 256, 262 );
		load ( heap, 257, 0   );
		load ( heap, 258, 67  );
		load ( heap, 259, 97  );
		load ( heap, 260, 116 );
		load ( heap, 261, 0   );
		load ( heap, 262, 268 );
		load ( heap, 263, 1   );
		load ( heap, 264, 68  );
		load ( heap, 265, 111 );
		load ( heap, 266, 103 );
		load ( heap, 267, 0   );
		load ( heap, 268, 0   );
		load ( heap, 269, 0   );

		assertEquals ( "At 264: allocated block of 4: Dog[0]", Whitespace.analyseHeap ( heap ) );
	}

	void testAnalyseFullMallocCatDogFreeCatOnHeap () {
		final var heap = new HashMap < BigInteger, BigInteger > ();

		load ( heap, 256, 262 );
		load ( heap, 257, 0   );
		load ( heap, 258, 67  );
		load ( heap, 259, 97  );
		load ( heap, 260, 116 );
		load ( heap, 261, 0   );
		load ( heap, 262, 268 );
		load ( heap, 263, 1   );
		load ( heap, 264, 68  );
		load ( heap, 265, 111 );
		load ( heap, 266, 103 );
		load ( heap, 267, 0   );
		load ( heap, 268, 0   );
		load ( heap, 269, 0   );

		assertEquals ( "At 258: unallocated block of 4\nAt 264: allocated block of 4: Dog[0]", Whitespace.analyseHeap ( heap, true ) );
	}

	private static void load (
		final Map < BigInteger, BigInteger > target,
		final int                            key,
		final int                            value
	) {
		target.put (
			BigInteger.valueOf ( key ),
			BigInteger.valueOf ( value )
		);
	}

	void testDecodeLabel () {
		assertEquals ( "s - [0]",                  Whitespace.decodeLabel ( "s"                ) );
		assertEquals ( "t - [1]",                  Whitespace.decodeLabel ( "t"                ) );
		assertEquals ( "st - [1]",                 Whitespace.decodeLabel ( "st"               ) );
		assertEquals ( "ts - [2]",                 Whitespace.decodeLabel ( "ts"               ) );
		assertEquals ( "sssststs - [10]",          Whitespace.decodeLabel ( "sssststs"         ) );
		assertEquals ( "sttsssst - a",             Whitespace.decodeLabel ( "sttsssst"         ) );
		assertEquals ( "stttttts - ~",             Whitespace.decodeLabel ( "stttttts"         ) );
		assertEquals ( "ststtsttststttst - []",    Whitespace.decodeLabel ( "ststtsttststttst" ) );
		try {
			Whitespace.decodeLabel ( "" );
			fail ( "Unexpected lack of exception decoding empty string." );
		} catch ( final IllegalArgumentException x ) {
			// expected
		}
		try {
			Whitespace.decodeLabel ( " " );
			fail ( "Unexpected lack of exception decoding single space." );
		} catch ( final IllegalArgumentException x ) {
			// expected
		}
		try {
			Whitespace.decodeLabel ( "cat" );
			fail ( "Unexpected lack of exception decoding 'cat'." );
		} catch ( final IllegalArgumentException x ) {
			// expected
		}
	}

	void testDecodeBin2Asc () {
		assertEquals ( "a", Whitespace.decodeBin2Asc ( "sttsssst" ) );
		assertEquals ( "~", Whitespace.decodeBin2Asc ( "stttttts" ) );
		assertEquals ( "Cat", Whitespace.decodeBin2Asc ( "stssssttsttsssststttstss" ) );
	}

	void testFloorDiv () {
		assertEquals ( BigInteger.valueOf (  3L ), Whitespace.floorDiv ( BigInteger.valueOf (  22L ), BigInteger.valueOf (  7L ) ) );
		assertEquals ( BigInteger.valueOf ( -4L ), Whitespace.floorDiv ( BigInteger.valueOf ( -22L ), BigInteger.valueOf (  7L ) ) );
		assertEquals ( BigInteger.valueOf ( -4L ), Whitespace.floorDiv ( BigInteger.valueOf (  22L ), BigInteger.valueOf ( -7L ) ) );
		assertEquals ( BigInteger.valueOf (  3L ), Whitespace.floorDiv ( BigInteger.valueOf ( -22L ), BigInteger.valueOf ( -7L ) ) );
	}

	void testHasValue () {
		assertFalse (
			Whitespace.hasValue (
				new HashMap < BigInteger, BigInteger > (),
				BigInteger.valueOf ( 264L ),
				57
			)
		);
		assertTrue (
			Whitespace.hasValue (
				Map.of (
					BigInteger.valueOf ( 264L ), BigInteger.valueOf (  57L )
				),
				BigInteger.valueOf ( 264L ),
				57
			)
		);
		assertFalse (
			Whitespace.hasValue (
				Map.of (
					BigInteger.valueOf ( 264L ), BigInteger.valueOf (  57L ),
					// No 265 mappping
					BigInteger.valueOf ( 266L ), BigInteger.valueOf (  59L )
				),
				BigInteger.valueOf ( 265L ),
				57
			)
		);
	}

	void testPrePadZeros () {
		assertEquals ( "",                 Whitespace.prePadZeros ( ""          ) );
		assertEquals ( "0000000a",         Whitespace.prePadZeros ( "a"         ) );
		assertEquals ( "000000ab",         Whitespace.prePadZeros ( "ab"        ) );
		assertEquals ( "abcdefgh",         Whitespace.prePadZeros ( "abcdefgh"  ) );
		assertEquals ( "0000000abcdefghi", Whitespace.prePadZeros ( "abcdefghi" ) );
	}
}
