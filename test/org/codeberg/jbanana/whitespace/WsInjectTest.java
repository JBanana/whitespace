package org.codeberg.jbanana.whitespace;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class WsInjectTest
		extends Test {
	void testGetMultiplier () {
		assertEquals (  1, WsInject.getMultiplier (    0,  0 ) );
		assertEquals (  1, WsInject.getMultiplier (    1,  2 ) );
		assertEquals (  1, WsInject.getMultiplier (    2,  1 ) );
		assertEquals (  2, WsInject.getMultiplier (    3,  1 ) );
		assertEquals (  3, WsInject.getMultiplier (    4,  1 ) );
		assertEquals (  4, WsInject.getMultiplier (    5,  1 ) );
		assertEquals ( 21, WsInject.getMultiplier ( 1234, 56 ) );
	}
	void testCleanse () throws IOException {
		assertEquals ( "[ , 	, \n, 	,  ]", WsInject.cleanse ( new File ( "test/resources/forTestingWsInject.txt" ) ).toString () );
	}
	void testTokenise () throws IOException {
		assertEquals ( "[a, bb, ccc, dddd, eeeee, ffffff]", WsInject.tokenise ( new File ( "test/resources/forTestingWsInject.txt" ) ).toString () );
	}
}
