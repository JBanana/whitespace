package org.codeberg.jbanana.whitespace;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Finds and runs unit test methods.
 */
public class TestRunner {
	private static final String FILE_SEP      = System.getProperty ( "file.separator" );
	private static final String SUFFIX        = "Test.class";
	private static final int    SUFFIX_LENGTH = SUFFIX.length ();

	public static void main ( final String [] args ) {
		if ( args.length != 1 )
			throw new Test.Fail ( "Expected 1 arg but got " + args.length );

		final var dir    = new File ( args [ 0 ] );
		final var counts = new int [] { 0, 0 };

		System.out.println ( "Looking in " + dir );
		if ( ! dir.isDirectory () )
			throw new Test.Fail ( "Not a dir: " + dir );

		findUnder (
			dir,
			f -> {
				final String name = f.getName ();
				return name.endsWith ( SUFFIX ) && name.length() > SUFFIX_LENGTH;
			}
		).forEach (
			f -> {
				try {
					final var clarse     = fileToClass ( f, dir );
					final var testObject = clarse.getConstructor ().newInstance ();

					for ( final var method : getTestMethods ( clarse ) )
						try {
							System.out.println ( "Trying " + method );
							method.invoke ( testObject );
							counts [ 0 ]++;
						} catch ( final Exception x ) {
							System.out.println ( "Oops: " + x );
							x.printStackTrace ();
							counts [ 1 ]++;
						}
				} catch ( final Exception x ) {
					throw new RuntimeException ( x.getMessage (), x );
				}
			}
		);

		System.out.println ( "Tests complete. Passes " + counts [ 0 ] + ", failures " + counts [ 1 ] + '.' );
		System.exit ( counts [ 1 ] );
	}

	private static List < File > findUnder ( final File dir, final FileFilter filter ) {
		final var result = new ArrayList < File > ();

		for ( final var f : dir.listFiles () )
			if ( f.isDirectory () )
				result.addAll ( findUnder ( f, filter ) );
			else if ( filter.accept ( f ) )
				result.add ( f );

		return result;
	}

	private static Class < ? > fileToClass ( final File classFile, final File dir )
			throws FileNotFoundException, ClassNotFoundException {
		final String dirName       = dir.getAbsolutePath ();
		final String classFileName = classFile.getAbsolutePath ();

		if ( ! classFileName.startsWith ( dirName ) )
			throw new Test.Fail ( "File " + classFileName + " isn't in " + dirName );

		final String className = classFileName
				.substring ( dirName.length () + 1 )
				.replaceAll ( Pattern.quote ( FILE_SEP ), "." )
				.replaceAll ( "\\.class$", "" );

		System.out.println ( className );

		return Class.forName ( className );
	}

	private static List < Method > getTestMethods ( final Class < ? > testClass ) {
		final var result = new ArrayList < Method > ();

		for ( final var method : testClass.getDeclaredMethods () ) {
			final var name = method.getName ();

			if ( name.startsWith ( "test" ) && 4 < name.length () && 0 == method.getParameterCount() )
				result.add ( method );
			else
				System.out.println ( "Ignoring " + method );
		}

		return result;
	}
}
