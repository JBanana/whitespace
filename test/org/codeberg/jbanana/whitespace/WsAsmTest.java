package org.codeberg.jbanana.whitespace;

import java.io.File;

public class WsAsmTest
		extends Test {
	void testMakeNum () {
		assertEquals ( "ssf",                 showWs ( WsAsm.makeNum (    "0"  ) ) );
		assertEquals ( "stf",                 showWs ( WsAsm.makeNum (    "1"  ) ) );
		assertEquals ( "ttf",                 showWs ( WsAsm.makeNum (   "-1"  ) ) );
		assertEquals ( "stssssssf",           showWs ( WsAsm.makeNum (   "'@'" ) ) );
		assertEquals ( "ststsf",              showWs ( WsAsm.makeNum ( "'\\n'" ) ) );
		assertEquals ( "sttstf",              showWs ( WsAsm.makeNum ( "'\\r'" ) ) );
		assertEquals ( "ststsssttf",          showWs ( WsAsm.makeNum (   "'£'" ) ) );
		assertEquals ( "stssssstststtssf",    showWs ( WsAsm.makeNum (   "'€'" ) ) );
		assertEquals ( "stssssssttstsstsssf", showWs ( WsAsm.makeNum (   "'𐍈'" ) ) );
	}

	void testMakeLabel () {
		assertEquals ( "ssttsstsf",         showWs ( WsAsm.makeLabel ( ":2"  ) ) );
		assertEquals ( "sttsssstf",         showWs ( WsAsm.makeLabel ( ":a"  ) ) );
		assertEquals ( "stssssstf",         showWs ( WsAsm.makeLabel ( ":A"  ) ) );
		assertEquals ( "sttsssststtssstsf", showWs ( WsAsm.makeLabel ( ":ab" ) ) );
		assertThrows ( () -> WsAsm.makeLabel ( null    ), IllegalArgumentException.class );
		assertThrows ( () -> WsAsm.makeLabel ( ""      ), IllegalArgumentException.class );
		assertThrows ( () -> WsAsm.makeLabel ( "aa"    ), IllegalArgumentException.class );
		assertThrows ( () -> WsAsm.makeLabel ( ":"     ), IllegalArgumentException.class );
		assertThrows ( () -> WsAsm.makeLabel ( ": "    ), IllegalArgumentException.class );
		assertThrows ( () -> WsAsm.makeLabel ( ":a b"  ), IllegalArgumentException.class );
		assertThrows ( () -> WsAsm.makeLabel ( ":£"    ), IllegalArgumentException.class );
		assertThrows ( () -> WsAsm.makeLabel ( ":€"    ), IllegalArgumentException.class );
		assertThrows ( () -> WsAsm.makeLabel ( ":𐍈"    ), IllegalArgumentException.class );
	}

	private static String showWs ( final String source ) {
		return source.replace ( " ", "s" ).replace ( "\t", "t" ).replace ( "\n", "f" );
	}

	void testExtendableSet () {
		// Given
		final var set = new WsAsm.ExtendableSet < String > ();

		set.add ( "aa" );
		set.add ( "bb" );
		assertEquals ( 2, set.size () );

		final var i = set.iterator ();
		assertEquals ( "aa", i.next () );

		// When
		set.add ( "cc" );
		set.add ( "aa" );

		// Then
		assertEquals ( "bb", i.next () );
		assertEquals ( "cc", i.next () );
		assertFalse ( i.hasNext () );
	}

	void testGetWsFile () {
		assertEquals ( "a.ws", WsAsm.getWsFile ( new File ( "a.wsa" ) ).getName () );
		assertEquals ( "a.txt.ws", WsAsm.getWsFile ( new File ( "a.txt" ) ).getName () );
	}
}
