package org.codeberg.jbanana.whitespace;

public class Test {
	/*package */ static class Fail extends RuntimeException {
		/*package */ Fail ( final String msg ) {
			super ( msg );
		}
		/*package */ Fail ( final String msg, final Throwable t ) {
			super ( msg, t );
		}
	}
	protected <T> void assertEquals ( final T expected, final T actual ) {
		if ( ( null == expected && null != actual )
				|| ( null != expected && null == actual )
				|| ! expected.equals ( actual ) )
			throw new Fail ( "Expected [" + expected + "] but was [" + actual + "]." );
	}
	protected void assertTrue ( final boolean b ) {
		if ( ! b )
			throw new Fail ( "Expected TRUE but was FALSE." );
	}
	protected void assertFalse ( final boolean b ) {
		if ( b )
			throw new Fail ( "Expected FALSE but was TRUE." );
	}
	protected void assertThrows ( final Runnable r, final Class < ? extends Throwable > clarse ) {
		try {
			r.run ();
			fail ( "Did not throw " + clarse );
		} catch ( final Fail f ) {
			throw f;
		} catch ( final Throwable t ) {
			if ( ! clarse.isAssignableFrom ( t.getClass () ) )
				fail ( "Threw " + t + ", not " + clarse );
		}
	}
	protected void fail ( final String msg ) {
		throw new Fail ( msg );
	}
}
