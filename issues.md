# Issues List

## Whitespace Interpreter Issues
* I'm not aware of any differences between this implementation and the original.
* I'm not aware of any issues with the extensions.

## Assembler and Macro Issues
### Accuracy of $sqrtfl and $expfl
* The output has changed since I documented it - why??
* The number of decimals for $expfl is definitely more than can be accurately claimed
### Map issues
* Is it possible to write a generally applicable $freemap? We don't know the type of the mapped values, so how can we free them? Hmm. This also applies to lists, hence $freelst and $freelstlite. It would be nice to have a polymorphic $free. But there are no function pointers, lambdas, or similar. Is it possible/worthwhile making a data type wrapper (0: in; 1: string; 2: list; etc)???
* Add a mapping, and the key is used in the map, but replace a mapping and the key isn't used (because the map already has it). For a new mapping, you don't want to free the key, but for a repleacment maping, you do. Hmm. Maybe the mapping function should free the key in the replacement case?
* $mapget returns a negative number for "mapping not found" but if the map values are ints (not addresses) then a negative result is quite likely.

## Whitespace Injection Issues
* Is it possible to automatically handle sections where white space is significant - HTML PRE, etc.?
